//
//  SOMessagesTableViewController.h
//  snapOpen
//
//  Created by Pantera Engineering on 25/11/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CFNetwork/CFNetwork.h>

@interface SOMessagesTableViewController : UITableViewController <NSStreamDelegate>

@end
