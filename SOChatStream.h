//
//  SOChatStream.h
//  snapOpen
//
//  Created by Pantera Engineering on 21/01/2015.
//  Copyright (c) 2015 Pantera Engineering. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CFNetwork/CFNetwork.h>

@interface SOChatStream : NSObject  <NSStreamDelegate>
@property (nonatomic,strong) id delegate;
- (void)initNetworkCommunication;
-(void)sendMessage:(NSString *)msg toUser:(long)userId;
-(void)startConnectionIfNotAlreadyOpen;
+(id)currentStream;
@end
