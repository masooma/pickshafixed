//
//  SOChatTableViewController.m
//  snapOpen
//
//  Created by Pantera Engineering on 25/11/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "SOChatTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "SOChatStream.h"

@interface SOChatTableViewController ()

@end

@implementation SOChatTableViewController{
    UITextView *theTextView;
    NSMutableArray *messagesArray;
   // user *myUser;
    
    UIView *bottomView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @ "Jullian James" ;
    UIImageView *tempImageView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"background.png"]];
    [tempImageView setFrame:self.tableView.frame];
    self.tableView.backgroundView = tempImageView;
    
    [self setUpView];
    messagesArray = [[NSMutableArray alloc] init];
    [self getConversation];
   // myUser = [user currentUser];
    UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, 66, 0);
    self.tableView.contentInset = inset;
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardFrameDidChange:) name:UIKeyboardWillChangeFrameNotification object:nil];
    [[SOChatStream currentStream] startConnectionIfNotAlreadyOpen];
}
-(void)viewWillDisappear:(BOOL)animated{
    [self.navigationController.view endEditing:YES];
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    [bottomView removeFromSuperview];
}
-(void)getConversation{
  /*  [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    NSLog(@"convo_id: %li",_theUser.conversationId);
    [[SOWebServices sharedServices] getDetailsOfConversationWithId:_theUser.conversationId Handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        
    }];
   */
}
/*
-(void)connectionGotData:(NSData *)data{
    
    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    if ([responseDictionary[@"code"] integerValue]==100) {
        messagesArray = [responseDictionary[@"message"] mutableCopy];

        [self.tableView reloadData];
        
        [self scrollTableViewToBottom];
    }
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}
-(void)addMessage:(NSDictionary *)message{
    [messagesArray addObject:message];
    [self.tableView reloadData];
    
    [self scrollTableViewToBottom];
    
}
-(void)messageReceived:(NSDictionary *)theMessage{
    NSLog(@"received: %@",theMessage);
  //  if ([theMessage[@"sender_id"] longValue]==_theUser.userId) {
 //       [self addMessage:theMessage];
 //   }
 
}
*/
-(void)setUpView{
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 150, 33)];
    UIImageView *thumb = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 33, 33)];
  //  [thumb setImageWithURL:_theUser.thumbURL];
    [thumb setContentMode:UIViewContentModeScaleAspectFill];
    //thumb.image = [UIImage imageNamed:@"sample_author"];
    thumb.clipsToBounds = YES;
    thumb.layer.cornerRadius = 16.5;
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 110, 33)];
  //  label.text = [NSString stringWithFormat:@"%@ %@",_theUser.firstName, _theUser.lastName];
     label.text = [NSString stringWithFormat:@"Jullian James"];
    label.textColor = [UIColor whiteColor];
    [titleView addSubview:thumb];
    [titleView addSubview:label];
    
    [self.navigationItem setTitleView:titleView];
    
    [self setUpBottomBar];
}
-(void)setUpBottomBar{
  /*  NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"messageBox" owner:self options:nil];
    
    bottomView = [[UIView alloc] init];
    
    bottomView = (UIView *)[nib objectAtIndex:0];
    UIButton *sendButton = (UIButton *)[bottomView viewWithTag:2];
    [sendButton addTarget:self action:@selector(sendMessage:) forControlEvents:UIControlEventTouchUpInside];
    
    theTextView = (UITextView *)[bottomView viewWithTag:1];
    [theTextView setDelegate:self];
    
    [bottomView setFrame:CGRectMake(0, self.view.frame.size.height-110.0, self.view.frame.size.width, 110.0)];
    [self.navigationController.view insertSubview:bottomView belowSubview:self.navigationController.navigationBar];
   */
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)scrollTableViewToBottom{
    
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:[self.tableView numberOfRowsInSection:0]-1 inSection:0] atScrollPosition:UITableViewScrollPositionBottom animated:YES];
    //[self.tableView scrollRectToVisible:CGRectMake(0, self.tableView.contentSize.height-1, 1, 10) animated:YES];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    SOChatStream *stream = [SOChatStream currentStream];
    stream.delegate = self;
}
-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
    SOChatStream *stream = [SOChatStream currentStream];
    stream.delegate = nil;
}
- (void)setTabBarVisible:(BOOL)visible animated:(BOOL)animated {
    
    // bail if the current state matches the desired state
    if ([self tabBarIsVisible] == visible) return;
    
    // get a frame calculation ready
    CGRect frame = self.tabBarController.tabBar.frame;
    CGFloat height = frame.size.height;
    CGFloat offsetY = (visible)? -height : height;
    
    // zero duration means no animation
    CGFloat duration = (animated)? 0.3 : 0.0;
    
    [UIView animateWithDuration:duration animations:^{
        self.tabBarController.tabBar.frame = CGRectOffset(frame, 0, offsetY);
    }];
}
// know the current state
- (BOOL)tabBarIsVisible {
    return self.tabBarController.tabBar.frame.origin.y < CGRectGetMaxY(self.view.frame);
}
-(void)closeChat{
    [self dismissViewControllerAnimated:YES completion:nil];
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    //return messagesArray.count;
    return 20;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSString *reuseId;
    BOOL user;
    if (indexPath.row % 2 == 0) {
        reuseId = @"otherCell";
        user = NO;
    }else{
        reuseId = @"userCell";
        user = YES;
    }
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    UIView *textWalaView = (UIView *)[cell viewWithTag:3];
    textWalaView.layer.cornerRadius = 20.0;
    [self configureBasicCell:cell atIndexPath:indexPath];
    return cell;
}
-(void)configureBasicCell:(UITableViewCell *)sizingCell atIndexPath:(NSIndexPath *)indexPath{
    UILabel *messageLabel = (UILabel *)[sizingCell viewWithTag:10];
    messageLabel.text = @"HELLO " ;//messagesArray[indexPath.row][@"content"];
}

-(CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return 88.0;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [self heightForBasicCellAtIndexPath:indexPath];
}

- (CGFloat)heightForBasicCellAtIndexPath:(NSIndexPath *)indexPath {
    static UITableViewCell *sizingCell = nil;
    static UITableViewCell *otherSizingCell = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"userCell"];
        otherSizingCell = [self.tableView dequeueReusableCellWithIdentifier:@"otherCell"];
    });
   /* if ([messagesArray[indexPath.row][@"sender_Id"] longValue]==_theUser.userId) {
        [self configureBasicCell:otherSizingCell atIndexPath:indexPath];
        float height = [self calculateHeightForConfiguredSizingCell:otherSizingCell];
        return height;//(height>60.0)? height : 60;
        
    }else{*/
        [self configureBasicCell:sizingCell atIndexPath:indexPath];
        float height = [self calculateHeightForConfiguredSizingCell:sizingCell];
        
       return height;//(height>40.0)? height : 40;
   // } */
  
}

- (CGFloat)calculateHeightForConfiguredSizingCell:(UITableViewCell *)sizingCell {
    
    sizingCell.bounds = CGRectMake(0.0f, 0.0f, CGRectGetWidth(self.tableView.frame), CGRectGetHeight(sizingCell.bounds));
    
    [sizingCell setNeedsLayout];
    [sizingCell layoutIfNeeded];
    
    CGSize size = [sizingCell.contentView systemLayoutSizeFittingSize:UILayoutFittingCompressedSize];
    return size.height + 0.0f; // Add 1.0f for the cell separator height
}
/*
-(UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    NSArray *nib = [[NSBundle mainBundle] loadNibNamed:@"messageBox" owner:self options:nil];
    
    UIView *view = [[UIView alloc] init];
    
    view = (UIView *)[nib objectAtIndex:0];
    UIButton *sendButton = (UIButton *)[view viewWithTag:2];
    [sendButton addTarget:self action:@selector(sendMessage:) forControlEvents:UIControlEventTouchUpInside];
    
    theTextView = (UITextView *)[view viewWithTag:1];
    [theTextView setDelegate:self];
    return view;
}
-(CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 110.0;
}*/
/*
// Override to support conditional editing of the table view.
- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the specified item to be editable.
    return YES;
}
*/

/*
// Override to support editing the table view.
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        // Delete the row from the data source
        [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    } else if (editingStyle == UITableViewCellEditingStyleInsert) {
        // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
    }   
}
*/

/*
// Override to support rearranging the table view.
- (void)tableView:(UITableView *)tableView moveRowAtIndexPath:(NSIndexPath *)fromIndexPath toIndexPath:(NSIndexPath *)toIndexPath {
}
*/

/*
// Override to support conditional rearranging of the table view.
- (BOOL)tableView:(UITableView *)tableView canMoveRowAtIndexPath:(NSIndexPath *)indexPath {
    // Return NO if you do not want the item to be re-orderable.
    return YES;
}
*/

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
-(void)sendMessage:(id)sender{
  /*  if (theTextView.text.length!=0) {
        //NSLog(@"message: %@",[aStr stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"]);
       [[SOChatStream currentStream] sendMessage:[theTextView.text stringByReplacingOccurrencesOfString:@"\n" withString:@"\\n"] toUser:_theUser.userId];
        NSDictionary *message = @{@"sender_id":[NSNumber numberWithLong:myUser.userId], @"receiver_id":[NSNumber numberWithLong:_theUser.userId], @"content":theTextView.text};
        [self addMessage:message];
        [theTextView setText:@""];
    } */
      
}


#pragma mark - Text View Delegate
-(void)textViewDidChange:(UITextView *)textView{

    NSLayoutConstraint *heightConstraint = textView.constraints[0];

    if (textView.contentSize.height<88.0) {
        //textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, textView.contentSize.height) ;
        heightConstraint.constant = textView.contentSize.height+6.0;
    }else{
        //textView.frame = CGRectMake(textView.frame.origin.x, textView.frame.origin.y, textView.frame.size.width, 94.0) ;
        heightConstraint.constant = 94.0;
    }
    [textView.superview layoutIfNeeded];
}
- (void)keyboardFrameDidChange:(NSNotification *)notification
{
    CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    CGRect keyboardBeginFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    UIViewAnimationCurve animationCurve = [[[notification userInfo] objectForKey:UIKeyboardAnimationCurveUserInfoKey] integerValue];
    NSTimeInterval animationDuration = [[[notification userInfo] objectForKey:UIKeyboardAnimationDurationUserInfoKey] integerValue];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:animationDuration];
    [UIView setAnimationCurve:animationCurve];
    
    CGRect newFrame = self.navigationController.view.frame;
    CGRect keyboardFrameEnd = [self.view convertRect:keyboardEndFrame toView:nil];
    CGRect keyboardFrameBegin = [self.view convertRect:keyboardBeginFrame toView:nil];
    
    newFrame.origin.y -= (keyboardFrameBegin.origin.y - keyboardFrameEnd.origin.y);
    self.navigationController.view.frame = newFrame;
    
    [UIView commitAnimations];
   /* CGRect keyboardEndFrame = [[[notification userInfo] objectForKey:UIKeyboardFrameEndUserInfoKey] CGRectValue];
    UIEdgeInsets inset = UIEdgeInsetsMake(0, 0, keyboardEndFrame.size.height+110, 0);
    self.tableView.contentInset = inset;
    
    
    [bottomView setFrame:CGRectMake(0, self.view.frame.size.height-110.0 - keyboardEndFrame.size.height, self.view.frame.size.width, 110.0)];
    */
}

@end
