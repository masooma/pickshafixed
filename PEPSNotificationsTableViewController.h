//
//  PEPSNotificationsTableViewController.h
//  snapOpen
//
//  Created by Pantera Engineering on 02/12/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"
@interface PEPSNotificationsTableViewController : UITableViewController <MWPhotoBrowserDelegate>
@property (nonatomic, strong) NSMutableArray *photos;

@end
