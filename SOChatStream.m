//
//  SOChatStream.m
//  snapOpen
//
//  Created by Pantera Engineering on 21/01/2015.
//  Copyright (c) 2015 Pantera Engineering. All rights reserved.
//

#import "SOChatStream.h"
//#import "user.h"
#import "SOChatTableViewController.h"
static SOChatStream *theStream = nil;
@implementation SOChatStream{
    NSInputStream *inputStream;
    NSOutputStream *outputStream;
    BOOL abc;
}
+(id)currentStream{
    static dispatch_once_t token;
    dispatch_once(&token, ^{
        theStream = [[self alloc] init];
        [theStream initNetworkCommunication];
    });
    return theStream;
}

- (void)initNetworkCommunication {
    NSLog(@"attemting connection");
    abc = NO;
    CFReadStreamRef readStream;
    CFWriteStreamRef writeStream;
    CFStreamCreatePairWithSocketToHost(NULL, (CFStringRef)@"tsunami.pk", 3100, &readStream, &writeStream);
    inputStream = (__bridge NSInputStream *)readStream;
    outputStream = (__bridge NSOutputStream *)writeStream;
    [inputStream setDelegate:self];
    [outputStream setDelegate:self];
    [inputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [outputStream scheduleInRunLoop:[NSRunLoop currentRunLoop] forMode:NSDefaultRunLoopMode];
    [inputStream open];
    [outputStream open];
}
-(void)sendUserId{
    if (!abc) {
      //  user *currentUser = [user currentUser];
   //     NSString *response  = [NSString stringWithFormat:@"%li", currentUser.userId];
    //    NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
     //   [outputStream write:[data bytes] maxLength:[data length]];
        abc = YES;
    }
}
-(void)startConnectionIfNotAlreadyOpen{
    if (outputStream.streamStatus == NSStreamStatusClosed || outputStream.streamStatus == NSStreamStatusNotOpen || outputStream.streamStatus == NSStreamStatusError) {
        NSLog(@"stream not open");
        [self initNetworkCommunication];
    }
}
-(void)sendMessage:(NSString *)msg toUser:(long)userId{
    if (outputStream.streamStatus == NSStreamStatusClosed || outputStream.streamStatus == NSStreamStatusNotOpen || outputStream.streamStatus == NSStreamStatusError) {
        NSLog(@"stream not open");
        [self initNetworkCommunication];
    }else{
        NSString *response  = [NSString stringWithFormat:@"{\"receiver_id\":%li, \"content\":\"%@\"}", userId, msg];
        NSData *data = [[NSData alloc] initWithData:[response dataUsingEncoding:NSASCIIStringEncoding]];
        [outputStream write:[data bytes] maxLength:[data length]];
    }
    
}
- (void)stream:(NSStream *)theStream handleEvent:(NSStreamEvent)streamEvent {
    //NSLog(@"Stream: %@ Event Code: %i",theStream,streamEvent);
    switch (streamEvent) {
            
        case NSStreamEventOpenCompleted:
            NSLog(@"Stream opened");
            break;
            
        case NSStreamEventHasBytesAvailable:
            if (theStream == inputStream) {
                
                uint8_t buffer[1024];
                int len;
                
                while ([inputStream hasBytesAvailable]) {
                    len = [inputStream read:buffer maxLength:sizeof(buffer)];
                    if (len > 0) {
                        NSData *output = [[NSData alloc] initWithBytes:buffer length:len];
                        
                        if (nil != output) {
                            [self parseServerResponse:output];
                        }
                    }
                }
            }
            break;
            
        case NSStreamEventErrorOccurred:
            NSLog(@"Can not connect to the host!");
            break;
            
        case NSStreamEventEndEncountered:
            break;
            
        case NSStreamEventHasSpaceAvailable:
            //NSLog(@"space available");
            //[self sendUserId];
            break;
            
        default:
            NSLog(@"Unknown event");
    }
    
}
-(void)parseServerResponse:(NSData *)response{
    //NSString *output = [[NSString alloc] initWithData:response encoding:NSASCIIStringEncoding];

    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:response options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"server said: %@ \nunformatted string: %@", responseDictionary,[[NSString alloc] initWithData:response encoding:NSUTF8StringEncoding]);
    if ([responseDictionary[@"code"] integerValue]==120) {
        [self sendUserId];
    }else if ([responseDictionary[@"code"] integerValue]==127){
        if (_delegate!=nil) {
          //  SOChatTableViewController *chatControl = _delegate;
           // [chatControl messageReceived:responseDictionary[@"message"]];
        }
    }
}
@end
