//
//  SOChatTableViewController.h
//  snapOpen
//
//  Created by Pantera Engineering on 25/11/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SOChatTableViewController : UITableViewController<UITextViewDelegate>

//@property (nonatomic, strong) user *theUser;
-(void)messageReceived:(NSDictionary *)theMessage;
-(void)closeChat;
@end
