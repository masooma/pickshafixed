//
//  PEPSNotificationsTableViewController.m
//  snapOpen
//
//  Created by Pantera Engineering on 02/12/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "PEPSNotificationsTableViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIImageView+WebCache.h"
#import "PEPSViewAlbumViewController.h"
#import "PEPSProfileViewController.h"

#import "MZFormSheetController.h"
#import "commentBoxViewController.h"
#include "PSTAlertController.h"
#import "AsyncImageView.h"



@interface PEPSNotificationsTableViewController ()

@end

@implementation PEPSNotificationsTableViewController{
    NSArray *returnedDataArray ;
    NSString *imageIdToSend;
    BOOL toggleIsOn;
    NSString *isLike;
    NSString *likeId;
    NSString * myId;
    NSString *imageIdToForward;

    

}

- (void)viewDidLoad {
    [super viewDidLoad];
    myId =[[webServices sharedServices] myUser][@"id"];
    NSLog(@"myID : %@" , myId);
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleNotificationChange:)
                                                 name:@"notificationCountChanged"
                                               object:nil];

    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];

    self.navigationController.navigationBar.barTintColor = aColor;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    
    UIColor *blueColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
    [self.tabBarController.tabBar setBackgroundImage:[UIImage new]];
    self.tabBarController.tabBar.backgroundColor = blueColor;
    [self getNotifications];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];

}
-(void)viewDidAppear:(BOOL)animated{

    [super viewDidAppear:animated];
   // self.navigationItem.title = @"Notifications" ;
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    
    self.navigationController.navigationBar.barTintColor = aColor;
    
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];

    
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController setNavigationBarHidden:NO];

}

- (void)handleNotificationChange:(NSNotification *)note {
    NSLog(@"Getting notifications in listener!");
    [self getNotifications];
    
    /*NSDictionary *theData = [note userInfo];
    if (theData != nil) {
        NSNumber *n = [theData objectForKey:@"isReachable"];
        BOOL isReachable = [n boolValue];
        NSLog(@"reachable: %d", isReachable);
    }
     */
}
#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    // Return the number of rows in the section.
    return returnedDataArray.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"notificationCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    NSString *text = [returnedDataArray objectAtIndex:indexPath.row][@"Text"];
    NSString *imageURL =[returnedDataArray objectAtIndex:indexPath.row][@"profileImage"];
    UILabel *notiflabel =(UILabel *)[cell viewWithTag:1];
    notiflabel.text = text;
    AsyncImageView *thumbImage = (AsyncImageView *)[cell viewWithTag:10];
   // thumbImage.layer.cornerRadius = 17.5;
    thumbImage.layer.cornerRadius = thumbImage.frame.size.height/2;
    thumbImage.layer.borderColor = [UIColor blackColor].CGColor;
    thumbImage.layer.borderWidth = 1.5;
    thumbImage.clipsToBounds = YES;
    
    NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
    [thumbImage sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    UILabel *timeLabel = (UILabel *)[cell viewWithTag:2];
    NSString *time2Show = [self timeToShowForDate:[self convert:returnedDataArray[indexPath.row][@"created_time"]]];
    timeLabel.text = time2Show;
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
     NSDictionary *galleryDetails = returnedDataArray[indexPath.row];
    
    if ([returnedDataArray[indexPath.row][@"type"] isEqualToString:@"Gallery Upload"])
    {
        if ([galleryDetails[@"album_name"] isKindOfClass:[NSNull class]]) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"The item you are trying to access no longer exists!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
        }
        else{
            PEPSViewAlbumViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewAlbumScene"];
            vc.type = 1;
            vc.albumName = galleryDetails[@"album_name"];
            vc.isPublic = 0;
            vc.albumID = galleryDetails[@"object_id"];
            vc.invitesCount = galleryDetails[@"invite_count"];
            vc.coverPhotoURL = galleryDetails[@"coverUrl"];
            vc.userName = galleryDetails[@"username"];
            vc.likesCount = galleryDetails[@"likeCount"];
            vc.photosCount = galleryDetails[@"photosCount"];
            vc.albumDescription = galleryDetails[@"des"];
            [self.navigationController pushViewController:vc animated:YES];
        }
    }
    
    
    else if ([returnedDataArray[indexPath.row][@"type"] isEqualToString:@"Friend request accepted "]) {
        PEPSProfileViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileScene"];
        vc.type=1;
        vc.otherUserUsername=galleryDetails[@"username"];
        vc.otherUserUserId=galleryDetails[@"sender_id"];
        [self.navigationController pushViewController:vc animated:YES];
    }

    else if ([returnedDataArray[indexPath.row][@"type"] isEqualToString:@"Image Like"] || [returnedDataArray[indexPath.row][@"type"] isEqualToString:@"Comment"] || [returnedDataArray[indexPath.row][@"type"] isEqualToString:@"Image Upload"] ) {[[webServices sharedServices]getImageDetailWithUserId:myId objectId:galleryDetails[@"object_id"] handler:^(NSData *data){       [self imageDetailsConnectionGotData:data imageId:galleryDetails[@"object_id"]];
        }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }
 ];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];}
    
}

- (NSString *)relativeDateStringForDate:(NSDate *)date
{
    NSCalendarUnit units = NSCalendarUnitDay | NSCalendarUnitWeekOfYear |
    NSCalendarUnitMonth | NSCalendarUnitYear;
    
    // if `date` is before "now" (i.e. in the past) then the components will be positive
    NSDateComponents *components = [[NSCalendar currentCalendar] components:units
                                                                   fromDate:date
                                                                     toDate:[NSDate date]
                                                                    options:0];
    
    if (components.year > 0) {
        return [NSString stringWithFormat:@"%ld years ago", (long)components.year];
    } else if (components.month > 0) {
        return [NSString stringWithFormat:@"%ld months ago", (long)components.month];
    } else if (components.weekOfYear > 0) {
        return [NSString stringWithFormat:@"%ld weeks ago", (long)components.weekOfYear];
    } else if (components.day > 0) {
        if (components.day > 1) {
            return [NSString stringWithFormat:@"%ld days ago", (long)components.day];
        } else {
            return @"Yesterday";
        }
    } else {
        return @"Today";
    }
}


- (NSDate *)convert:(NSString *)strdate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:strdate];
    
    return date;
    
}

- (NSString *)timeToShowForDate:(NSDate *)date{
    NSDate *currentDate = [NSDate date];
    NSString *dateFormat;
    
    //NSTimeInterval timePassed = [currentDate timeIntervalSinceDate:date];
    //NSInteger hoursPassed = timePassed/3600;
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:date
                                                          toDate:currentDate
                                                         options:0];
    NSInteger daysPassed = [components day];
    if (daysPassed<1) {
        dateFormat = @"HH:mm a";
    }else if(daysPassed<2){
        return @"Yesterday";
    }else if (daysPassed<5){
        dateFormat = @"EEE";
    }else{
        dateFormat = @"MMM dd";
    }
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:dateFormat];
    NSString * depResult = [df stringFromDate:date];
    
    return depResult;
}


-(void)showPhotoBrowserFromIndex:(NSInteger )startingIndex{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    //browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
    
    // Optionally set the current visible photo before displaying
    // [browser setCurrentPhotoIndex:1];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:startingIndex];
    
}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}



- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    
    NSLog(@"photo to send: %@",self.photos[index]);
    if (index < self.photos.count){
        
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

-(void)commentButtonPressedForPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    NSLog(@"show a comment box inside the view for notifications :%@",photoBrowser);

    commentBoxViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"commentBoxScene"];
    vc.imageId =imageIdToForward;

    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
    formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-30, self.view.frame.size.height-100);
    formSheet.shadowRadius = 5.0;
    formSheet.cornerRadius = 0.0;
    formSheet.shadowOpacity = 0.3;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    [[MZFormSheetController sharedBackgroundWindow] setBackgroundBlurEffect:YES];
    [[MZFormSheetController sharedBackgroundWindow] setBlurRadius:5.0];
    [[MZFormSheetController sharedBackgroundWindow] setBackgroundColor:[UIColor clearColor]];
    [formSheet setMovementWhenKeyboardAppears:MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard];
    
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromBottom;
    
    [photoBrowser mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
    }];
}


#pragma mark - CONNECTION

-(void)getNotifications{
    [[webServices sharedServices] getNotificationsWithHandler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}


-(void)connectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSArray *returnedArray = returnedDict[@"Data"];
    if (returnedArray !=nil) {
        
    NSLog(@"returnedArray getNotifications: %@" , returnedArray);
    returnedDataArray = returnedArray;
    //NSInteger code = [returnedDict[@"code"] integerValue];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    
    [self.tableView reloadData];
        
    }
}

-(void)imageDetailsConnectionGotData:(NSData *)data imageId : (NSString *) imageId{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"image LIKE COUNT Details returned : in notf : %@" , returnedDict[@"imageLikeCount"]);
    if ([returnedDict[@"imageLikeCount"] isKindOfClass:[NSNull class]]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"The item you are trying to access no longer exists!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        
    }
    else{

    
    imageIdToForward = imageId;
   // NSArray *returnedArray = returnedDict[@"Data"];
   //if (returnedArray !=nil) {

    NSString *imageURL = returnedDict[@"imageUrl"];
    
    self.photos = [NSMutableArray array];
  //  [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]]
    
    MWPhoto *photo =[MWPhoto photoWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]]];
    photo.imageId=[imageId integerValue];
    photo.isLiked =[returnedDict[@"like"][0][@"like_id"]boolValue];
    photo.likeId= [returnedDict[@"like"][0][@"like"] integerValue];
    [_photos addObject:photo];
    [self showPhotoBrowserFromIndex:0];
    
    }


              
   // }
}


-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}



@end
