//
//  SOMessagesTableViewController.m
//  snapOpen
//
//  Created by Pantera Engineering on 25/11/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "SOMessagesTableViewController.h"
#import <QuartzCore/QuartzCore.h>
//#import "user.h"
#import "SOChatStream.h"
#import "SOChatTableViewController.h"
#include "SOConversationViewController.h"
#import "UIImageView+WebCache.h"

@interface SOMessagesTableViewController ()

@end

@implementation SOMessagesTableViewController{
    NSArray *conversations;
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    
    
    NSMutableArray *allUsersDataArray;
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMessageChange:)
                                                 name:@"messageCountChanged"
                                               object:nil];
    
    //     [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(getMessagesList) userInfo:nil repeats:YES];
    
    allUsersDataArray = [[NSMutableArray alloc] init];
    
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    
    NSLog(@"my user is: %@",[[webServices sharedServices] myUser]);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    NSLog(@"myUserName : %@ " , myUserName);
    NSLog(@"myUserId : %@ " , myId);
    
}
-(void)getMessagesList{
    
    [[webServices sharedServices]getMessagesListWithHandler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        //[self connectionFailedWithError:error];
    }];
    [self.tableView reloadData ];
    
    
}
- (void)handleMessageChange:(NSNotification *)note {
    NSLog(@"Getting messages in listener!");
    [self getMessagesList];
    
    [NSTimer scheduledTimerWithTimeInterval:5.0f target:self selector:@selector(getMessagesList) userInfo:nil repeats:NO];
    
    
    
    /*NSDictionary *theData = [note userInfo];
     if (theData != nil) {
     NSNumber *n = [theData objectForKey:@"isReachable"];
     BOOL isReachable = [n boolValue];
     NSLog(@"reachable: %d", isReachable);
     }
     */
}

-(void)viewDidAppear:(BOOL)animated{
    [[webServices sharedServices]getMessagesListWithHandler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    self.tabBarController.tabBar.hidden = NO;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancel:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return conversations.count;
    
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"messageCell" forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    
    
    NSDictionary *conversation = conversations[indexPath.row];
    
    
    // NSDictionary *conversation = conversations[arrayRow];
    
    UIImageView *imageView = (UIImageView *) [cell viewWithTag:1];
    
    
    //[imageView setImageWithURL:[NSURL URLWithString:conversation[@"profileimage"]]];
    //imageView.layer.cornerRadius = 25.0;
    imageView.layer.cornerRadius = imageView.frame.size.height/2;
    imageView.layer.borderColor = [UIColor blackColor].CGColor;
    imageView.layer.borderWidth = 1.5;
    imageView.clipsToBounds = YES;
    
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",conversation[@"profileimage"]]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    
    UILabel *firstname = (UILabel *)[cell viewWithTag:2];
    firstname.text = conversation[@"username"];
    UILabel *lastMessage = (UILabel *)[cell viewWithTag:4];
    lastMessage.text = conversation[@"message"];
    UILabel *timeLabel = (UILabel *)[cell viewWithTag:5];
    NSString *time2Show = [self timeToShowForDate:[self convert:conversation[@"msg_send_time"]]];
    timeLabel.text = time2Show;
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *conversation = conversations[indexPath.row];
    
    SOConversationViewController *chatTableView = [self.storyboard instantiateViewControllerWithIdentifier:@"conversationScene"];
    
    chatTableView.messageFromUserId  =  conversation[@"otherparty"];
    chatTableView.messageFromUserName  =   conversation[@"username"];
    chatTableView.messageFromUserProfilePicture = conversation[@"profileimage"];
    
    
    chatTableView.myUserId = myId;
    
    [self.navigationController pushViewController:chatTableView animated:YES];
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        //add code here for when you hit delete
        NSLog(@"tapped on delete");
        NSDictionary *conversation = conversations[indexPath.row];
        
        
        [[webServices sharedServices]deleteConversationWithUserId:conversation[@"otherparty"] handler:^(NSData *data){
            [self deleteConversationConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}

- (NSDate *)convert:(NSString *)strdate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:strdate];
    
    return date;
    
}

- (NSString *)timeToShowForDate:(NSDate *)date{
    NSDate *currentDate = [NSDate date];
    NSString *dateFormat;
    
    //NSTimeInterval timePassed = [currentDate timeIntervalSinceDate:date];
    //NSInteger hoursPassed = timePassed/3600;
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:date
                                                          toDate:currentDate
                                                         options:0];
    NSInteger daysPassed = [components day];
    if (daysPassed<1) {
        dateFormat = @"HH:mm a";
    }else if(daysPassed<2){
        return @"Yesterday";
    }else if (daysPassed<5){
        dateFormat = @"EEE";
    }else{
        dateFormat = @"MMM dd";
    }
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:dateFormat];
    NSString * depResult = [df stringFromDate:date];
    
    return depResult;
}

#pragma -mark Connection

-(void)deleteConversationConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDict= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:returnedDict[@"message"] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil];
    [alert show];
    [self getMessagesList];
}

-(void)connectionGotData:(NSData *)data{
    NSLog(@"connection got data for messages : %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSDictionary *returnedDict= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSArray *entries =returnedDict[@"Data"];
    
    NSArray *reversedEntries = [[entries reverseObjectEnumerator] allObjects];
    // conversations = returnedDict[@"Data"];
    conversations = reversedEntries;
    NSString *chatWithUserId;
    
    // NSLog(@"returnedDict for PUBLIC n Album View: : %@" , returnedDict);
    
    NSLog(@"returnedDataArray in connectionGotData , conversations : : %@" , conversations);
    
    for (int i = 0 ; i<conversations.count; i++) {
        
        
        if (![myId isEqualToString: conversations[i][@"user_to"]] ) {
            
            chatWithUserId =conversations[i][@"user_to"];
        }
        else{
            chatWithUserId =conversations[i][@"user_from"];
            
        }
        
        
        
    }
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [self.tableView reloadData];
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

@end
