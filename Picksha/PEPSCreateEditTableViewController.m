//
//  PEPSCreateEditTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 22/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSCreateEditTableViewController.h"
#import <AssetsLibrary/AssetsLibrary.h>
#import "PSTAlertController.h"



@interface PEPSCreateEditTableViewController ()

@end

@implementation PEPSCreateEditTableViewController{
    UIImagePickerController *ipc;
    UIPopoverController *popover;
    IBOutlet UICollectionView *selectedImagesCollectionView;
    UITapGestureRecognizer *tapRecognizer;
    IBOutlet UITextField *albumNameTextfield;
    IBOutlet UITextField *albumDescriptionTextfield;
    NSArray *selectedImagesFromGallery;
    UIImage *imageToBeSentToServer;
    
    NSMutableArray *allTemporaryImagesArrayyToDisplay;
 /*   NSInteger seeAlbumSelectedRow;
    NSInteger uploadPhotosSelectedRow;
    NSInteger inviteOthersSelectedRow;
   */
    
    NSString *seeAlbumText;
    NSString *uploadPhotosText;
    NSString *inviteOthersText;
    
    NSString *previousInviteOthersText;
    NSString *previousUploadPhotosText;
    NSString *previousSeeAlbumText;
    NSString *previousAlbumName;
    NSString *previousAlbumDescription;
    NSString *myUserName;
    NSString *myId;
    
    IBOutlet UIButton *albumSettingsButton;

    
    NSMutableArray *selectedRows;
    
    NSInteger count;
    NSInteger failed;
    
    
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setUpView];
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    selectedImagesFromGallery=[[NSMutableArray alloc]init];
    allTemporaryImagesArrayyToDisplay=[[NSMutableArray alloc]init];
    
    if (_type == 1) {
        _seeAlbumSelectedRow =5;
        
        _uploadPhotosSelectedRow=5;
        _inviteOthersSelectedRow=5;
    }
    
}
-(void)setUpView{
    self.tableView.scrollEnabled = NO;
   // [self roundBtn];
    if (_is_me == 1) {
        albumNameTextfield.enabled=NO;
        albumDescriptionTextfield.enabled=NO;
    }
    [self.tableView setSeparatorStyle:UITableViewCellSeparatorStyleNone];
    self.tableView.backgroundColor = [UIColor colorWithRed:45.0/255.0 green:45.0/255.0 blue:45.0/255.0 alpha:1];
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
   // UIBarButtonItem *myBarButtonItem = [[UIBarButtonItem alloc] init];
   // myBarButtonItem.title = @" "; // or whatever text you want
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    switch (_type) {
        case 0:{
            self.navigationItem.title = @ "Create Album" ;
            albumSettingsButton.hidden=NO;
            albumSettingsButton.enabled=YES;
             if (_is_me == 0) {
            UIBarButtonItem *newCreateButton = [[UIBarButtonItem alloc] initWithTitle:@"Create" style:UIBarButtonItemStylePlain target:self action:@selector(create)];
            self.navigationItem.rightBarButtonItem=newCreateButton;
             }
            
            break;
        }
        case 1:{

            
            
            self.navigationItem.title = @ "Edit Album" ;
            if (_is_me == 0) {
                UIBarButtonItem *newCreateButton = [[UIBarButtonItem alloc] initWithTitle:@"Save" style:UIBarButtonItemStylePlain target:self action:@selector(create)];
                self.navigationItem.rightBarButtonItem=newCreateButton;
                albumSettingsButton.hidden=NO;
                albumSettingsButton.enabled=YES;
            }
            else{
                albumSettingsButton.hidden=YES;
                albumSettingsButton.enabled=NO;
            }
            [[webServices sharedServices] getAlbumSettingsWithAlbumId:_albumIdToEdit handler:^(NSData *data){
                [self gallerySettingsConnectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            break;
        }
        default:
            break;
    }

    [self hideKeyboard];
}

-(void)hideKeyboard{
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];

}
- (void)roundBtn{
    albumSettingsButton.layer.cornerRadius = 8;
    [albumSettingsButton.layer setBorderWidth:1.0];
    [albumSettingsButton.layer setBorderColor:[UIColor whiteColor].CGColor];
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [selectedImagesCollectionView reloadSections:[NSIndexSet indexSetWithIndex:0]];
    [selectedImagesCollectionView reloadData];
    [self.tableView reloadData];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if (_type == 0) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES  scrollPosition:UITableViewScrollPositionNone];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] animated:YES  scrollPosition:UITableViewScrollPositionNone];
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] animated:YES  scrollPosition:UITableViewScrollPositionNone];
        [self.tableView reloadData];
    }
}


-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [albumNameTextfield resignFirstResponder];
    [albumDescriptionTextfield resignFirstResponder];

    
}
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)uploadPhotosButtonOnTap:(id)sender {
    
    PSTAlertController *gotoPageController = [PSTAlertController alertWithTitle:nil message:nil];
    
    [gotoPageController addCancelActionWithHandler:NULL];
    [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Capture Photo" handler:^(PSTAlertAction *action) {
     
        [self selectImagesByCapturing];
    }]];
    [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Upload Photos" handler:^(PSTAlertAction *action) {

        [self selectImagesfromGallery];
    }]];
    [gotoPageController showWithSender:nil controller:self animated:YES completion:NULL];
    
}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    
    switch (actionSheet.tag ) {
        case 1001:
            if (buttonIndex == 0)
            {
                //NSLog(@"NO");
               // NSLog(@"case 1001 for NOT deleting album");
                
            }
            else
            {
               // NSLog(@"YES");
               // NSLog(@"case 1001 for deleting album with album id : %@" , _albumIdToEdit);
             
                [[webServices sharedServices] deleteGalleryWithAlbumId:_albumIdToEdit handler:^(NSData *data){
                    [self deleteGalleryConnectionGotData:data];
                }errorHandler:^(NSError *error){
                    [self connectionFailedWithError:error];
                }];
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
               
            }
            break;
        case 1002:
            if (buttonIndex == 0)
            {
               // NSLog(@"NO");
               // NSLog(@"case 2001 for NOT adding");
            }
            else
            {   if(_type == 0)
                {
                
                //NSLog(@"YES");
               // NSLog(@"case 1002 for uploading album");
                //  NSLog(@"aading user with user id : %@" , _otherUserUserId);
                
                    
                if ([self setAlbumSettings])
                    {
                    
                   // NSLog(@"CREATING AN ALBUM with seeAlbumText,uploadPhotosText,inviteOthersText%@ , %@ , %@" ,seeAlbumText,uploadPhotosText,inviteOthersText );
                    
                    [[webServices sharedServices] createGalleryWithAlbumname:albumNameTextfield.text description:albumDescriptionTextfield.text seeAlbum:seeAlbumText uploadPhotos:uploadPhotosText inviteOthers:inviteOthersText handler:^(NSData *data){
                        [self createAlbumConnectionGotData:data];
                    }errorHandler:^(NSError *error){
                        [self connectionFailedWithError:error];
                    }];
                    
                    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    }
                }
                else if(_type == 1)
                {
                    
                  //  NSLog(@"YES");
                   // NSLog(@"case 1002 for UPDATING album");
                   //  NSLog(@"UPDATING album with  id : %@" , _albumIdToEdit);
                    
                    if ([self setAlbumSettings])
                    {
                       /* NSString *messageToShow = [NSString stringWithFormat:@"Album updated successfully.\n%ld files uploaded, %ld files failed" , (long)count , (long)failed];
                        if (count+failed == allTemporaryImagesArrayyToDisplay.count) {
                            UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"" message:messageToShow delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                            
                            [Alert show];
                            // [self.navigationController popViewControllerAnimated:YES];
                       
                        }*/

                        
                        NSLog(@"UPDATE GALLERY SETTINGS with : seeAlbumText,uploadPhotosText,inviteOthersText%@ , %@ , %@" ,seeAlbumText,uploadPhotosText,inviteOthersText );
                        
                        [self updateGalleryWithAlbumId:_albumIdToEdit AlbumName:albumNameTextfield.text AlbumDescription:albumDescriptionTextfield.text seeAlbum:seeAlbumText uploadPhotos:uploadPhotosText inviteOthers:inviteOthersText];
                    }
                }
            }
            break;
        default:
            break;
    }
}

-(void)updateGalleryWithAlbumId: (NSString *)albumId AlbumName: (NSString *)albumName AlbumDescription: (NSString *)albumDescription seeAlbum: (NSString *)seeAlbum uploadPhotos: (NSString *)uploadPhotos inviteOthers: (NSString *)inviteOthers{
    
    [[webServices sharedServices] updateGalleryWithAlbumId:albumId name:albumName description:albumDescription seeAlbum:seeAlbum uploadPhotos:uploadPhotos inviteOthers:inviteOthers handler:^(NSData *data){
        [self updateAlbumConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 3;
}

-(void)selectImagesfromGallery {
    QBImagePickerController *imagePickerController = [QBImagePickerController new];
    
   imagePickerController.assetCollectionSubtypes = @[
                                                      @(PHAssetCollectionSubtypeSmartAlbumUserLibrary), // Camera Roll
                                                      @(PHAssetCollectionSubtypeAlbumMyPhotoStream), // My Photo Stream
                                                      @(PHAssetCollectionSubtypeAny),
                                                      @(PHAssetCollectionSubtypeSmartAlbumPanoramas),
                                                    // Panoramas
                                                      
                    
                                                      
                                                     @(PHAssetCollectionSubtypeSmartAlbumBursts) // Bursts
                                                      ];
    
 
    imagePickerController.delegate = self;
  
    imagePickerController.mediaType = QBImagePickerMediaTypeImage;
    imagePickerController.allowsMultipleSelection = YES;
    imagePickerController.showsNumberOfSelectedAssets = YES;
    imagePickerController.maximumNumberOfSelection = 9;
    [self presentViewController:imagePickerController animated:YES completion:NULL];
}

#pragma mark - QBImagePickerControllerDelegate

- (void)qb_imagePickerController:(QBImagePickerController *)imagePickerController didFinishPickingAssets:(NSArray *)assets
{
    
   // NSLog(@"Selected assets:%@ /n assets.count : %lu", assets,(unsigned long)assets.count);
    selectedImagesFromGallery = assets;
    NSMutableArray *temporaryImagesArray;
    NSMutableArray *temporaryImagesNamesArray;
    temporaryImagesNamesArray = [[NSMutableArray alloc]init];
    temporaryImagesArray= [[NSMutableArray alloc]init];

    
    for (int i = 0; i < assets.count; i++) {
        
       // NSLog(@"i : %d " , i);
        
        [[PHImageManager defaultManager] requestImageForAsset:[assets objectAtIndex:i] targetSize:PHImageManagerMaximumSize contentMode:PHImageContentModeDefault options:nil resultHandler:^(UIImage *resultImage, NSDictionary *info){
         
            
           if ([info objectForKey:PHImageErrorKey] == nil) {
               
               // NSLog(@"resultImage : %@" , resultImage);
                UIImage *imageToBeSent = resultImage;
                float width,height;
                width = imageToBeSent.size.width;
                height = imageToBeSent.size.height;
                if (width>1024.0f && height>1024.0f) {
                    if (width<height) {
                        height = height/width*1024.0f;
                        width=1024.0f;
                    }else{
                        width = width/height*1024.0f;
                        height = 1024.0f;
                    }
                }
                CGSize newSize = CGSizeMake(width, height);
                UIGraphicsBeginImageContext(newSize);
                
                [imageToBeSent drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
                imageToBeSent = UIGraphicsGetImageFromCurrentImageContext();
                UIGraphicsEndImageContext();
               
                
                [temporaryImagesArray addObject:imageToBeSent];
                [allTemporaryImagesArrayyToDisplay addObject:imageToBeSent];
               
                
                if ([info objectForKey:@"PHImageFileURLKey"]) {
                    
                    NSURL *path = [info objectForKey:@"PHImageFileURLKey"];
                    NSString* theFileName = [[path lastPathComponent] stringByDeletingPathExtension];
                    [temporaryImagesNamesArray addObject:theFileName];
                  //  NSLog(@"temporaryImagesNamesArray : %@" , temporaryImagesNamesArray);
                    

                }
               [selectedImagesCollectionView reloadData];
               if (_type == 0) {
                   MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];

                [[webServices sharedServices] uploadImagesTemporarilywithImage:imageToBeSent withImageName:@"" aboutPhoto:@"My First Photo" cover:@"0" handler:^(NSData *data){
                    [self connectionGotData:data];
                    [hud hide:YES];
                }errorHandler:^(NSError *error){
                    [self tempConnectionFailedWithError:error];
                    [hud hide:YES];
                }];
                //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
               }
               
               else if(_type==1){
                   MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    NSString *uploadingUsername = [_otherUserName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];

                   [[webServices sharedServices] editGallerywithImage:imageToBeSent userUploadingUsername:uploadingUsername withGalleryId:_albumIdToEdit cover:@"0" handler:^(NSData *data){
                       [self editGallerywithImageConnectionGotData:data];
                       [hud hide:YES];
                   }errorHandler:^(NSError *error){
                       [self tempConnectionFailedWithError:error];
                       [hud hide:YES];
                   }];
                   //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
                   
               }

               
              [selectedImagesCollectionView reloadData];
            }
        }];
        //[selectedImagesCollectionView reloadData];

    }
            [self dismissViewControllerAnimated:YES completion:NULL];
}

- (void)qb_imagePickerControllerDidCancel:(QBImagePickerController *)imagePickerController{

   // NSLog(@"cancelled");
    [self dismissViewControllerAnimated:YES completion:NULL];

}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath{

    NSLog(@"selected picture : %ld " , (long)indexPath.row);
    
}

#pragma mark - Collection View Delegate Methods

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section{
   // NSLog(@"allTemporaryImagesArrayyToDisplay.count in rows collection view : %lu" , (unsigned long)allTemporaryImagesArrayyToDisplay.count);
    return allTemporaryImagesArrayyToDisplay.count;
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
   
    UIImageView *selectedImage = (UIImageView *) [cell viewWithTag:11];
    selectedImage.layer.cornerRadius = selectedImage.frame.size.height/6;
    selectedImage.layer.borderColor = [UIColor blackColor].CGColor;
    selectedImage.layer.borderWidth = 1.5;
    selectedImage.clipsToBounds = YES;
    
    [selectedImage setImage:allTemporaryImagesArrayyToDisplay[indexPath.row]];
    
  
    
    return cell;

}
-(void)connectionGotData:(NSData *)data{
    count++;
    NSLog(@"allTemporaryImagesArrayyToDisplay.count  :%lu" , (unsigned long)allTemporaryImagesArrayyToDisplay.count);
    NSLog(@"selectedImagesFromGallery.count : %lu" , (unsigned long)selectedImagesFromGallery.count);
    NSLog(@"count : %ld" , (long)count);
    
  NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@"returnedDictionary for temp  : %@" , returnedDictionary);
}
-(void)deleteGalleryConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
   // NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
   // NSLog(@"returnedDictionary for deleteGalleryConnectionGotData : %@" , returnedDictionary);
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
    
}
-(void)editGallerywithImageConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
   // NSLog(@"returnedDictionary for editGallerywithImageConnectionGotData : %@" , returnedDictionary);
    
    if ([returnedDictionary[@"code"] isEqualToString:@"200"]) {
        count++;
        /*
    NSString *messageToShow = [NSString stringWithFormat:@"Album updated successfully.\n%ld files uploaded, %ld files failed" , (long)count , (long)failed];
        if (count+failed == allTemporaryImagesArrayyToDisplay.count) {
            UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"" message:messageToShow delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            
            [Alert show];
           // [self.navigationController popViewControllerAnimated:YES];
        }
         */
    }
    else if ([returnedDictionary[@"code"] isEqualToString:@"400"]) {
        
        UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Images could not be uploaded!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [self.navigationController popViewControllerAnimated:YES];

        [Alert show];
    }
}

-(void)connectionFailedWithError:(NSError *)error{
        [self hideAllHUDs];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
}

-(void)tempConnectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    failed++;
    /*
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
     */
}

/*-(void)hideAllcollectionHUDs{
        [MBProgressHUD hideAllHUDsForView:selectedImagesCollectionView animated:YES];
}
 */
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(BOOL)setAlbumSettings{


    switch (_seeAlbumSelectedRow) {
        case 0:
            seeAlbumText = @"Friends";
            break;
        case 1:
            seeAlbumText = @"Friends of Friends";
            break;
        case 2:
            seeAlbumText = @"Only Me";
            break;
        case 3:
            seeAlbumText = @"Everyone";
            break;
        case 4:
            seeAlbumText = @"Just Invited Friends";
            break;
        default:
            break;
    }
    
    switch (_uploadPhotosSelectedRow) {
        case 0:
            uploadPhotosText = @"Friends";
            break;
        case 1:
            uploadPhotosText = @"Friends of Friends";
            break;
        case 2:
            uploadPhotosText = @"Only Me";
            break;
        case 3:
            uploadPhotosText = @"Everyone";
            break;
        case 4:
            uploadPhotosText = @"Just Invited Friends";
            break;
        default:
            break;
    }
    switch (_inviteOthersSelectedRow) {
        case 0:
            inviteOthersText = @"Friends";
            break;
        case 1:
            inviteOthersText = @"Friends of Friends";
            break;
        case 2:
            inviteOthersText = @"Only Me";
            break;
        case 3:
            inviteOthersText = @"Everyone";
            break;
        default:
            break;
    }
    
    return YES;
}
- (IBAction)albumSettingsBtnOnTap:(id)sender {
   
    
    ///////// ===========/////
    PEPSHelpTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"helpScene"];
    vc.type = 1;
    vc.isEdit=_type;
    vc.seeAlbumSelectedRow = _seeAlbumSelectedRow;
    vc.uploadPhotosSelectedRow  = _uploadPhotosSelectedRow;
    vc.inviteOthersSelectedRow  = _inviteOthersSelectedRow;
    vc.delegate  =self;
    [self.navigationController pushViewController:vc animated:YES];
    
}

-(void)create{
    
   // if ([albumNameTextfield.text isEqualToString:@""] || [albumDescriptionTextfield.text isEqualToString:@""] ) {
    if ([albumNameTextfield.text isEqualToString:@""] ) {
        
      /*  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Album name and description textfields can not be empty!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       */
        albumNameTextfield.text = @"Untitled Album";
    }
    
   // else{
    
    if (_type == 0) {
        
        UIAlertView *uploadAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Upload Album?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        uploadAlert.tag = 1002;
        [uploadAlert show];
    }
    
    else if(_type ==1){
        UIAlertView *uploadAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Save changes to the album?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        uploadAlert.tag = 1002;
        [uploadAlert show];
    }
    
 //   }

    
  }
-(void)createAlbumConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
   // NSLog(@"createAlbum: returnedDictionary : %@" , returnedDictionary);
    NSString *message =returnedDictionary[@"message"];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];

    if ([returnedDictionary[@"code"] isEqualToString:@"200"]) {
        NSString *messageToShow = [NSString stringWithFormat:@"Album uploaded successfully.\n%ld files uploaded, %ld files failed" , (long)count , (long)failed];
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:messageToShow delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else if ([returnedDictionary[@"code"] isEqualToString:@"400"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Album could not be uploaded.\nPlease try again!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];

    }

    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}
-(void)updateAlbumConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"updateAlbum: returnedDictionary : %@" , returnedDictionary);
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSString *message = returnedDictionary[@"message"];
    if ([returnedDictionary[@"code"] isEqualToString:@"200"] || [returnedDictionary[@"code"] isEqualToString:@"400"]) {
        NSString *messageToShow = [NSString stringWithFormat:@"Album updated successfully.\n%ld files uploaded, %ld files failed" , (long)count , (long)failed];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:messageToShow delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshAlbum" object:self];
        [self.navigationController popViewControllerAnimated:YES];
    }
   /*else if ([returnedDictionary[@"code"] isEqualToString:@"300"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter an album name and description!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
   }
    */
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}

-(void)gallerySettingsConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"gallerySettingsConnectionGotData in CREATE: returnedDictionary : %@" , returnedDictionary);
    NSArray * dataArray = returnedDictionary[@"Data"];
    
    previousAlbumName= dataArray[0][@"name"];
    previousAlbumDescription= dataArray[0][@"description"];
    previousUploadPhotosText= dataArray[0][@"gallery_access"];
    previousInviteOthersText= dataArray[0][@"invited_gallery"];
    previousSeeAlbumText= dataArray[0][@"see_gallery"];
    
    [albumNameTextfield performSelectorOnMainThread:@selector(setText:) withObject:previousAlbumName waitUntilDone:NO];
    [albumDescriptionTextfield performSelectorOnMainThread:@selector(setText:) withObject:previousAlbumDescription waitUntilDone:NO];
    
   
    
    if ([previousSeeAlbumText isEqualToString:@"Friends"] ) {
            [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _seeAlbumSelectedRow = 0;
    }
    if ([previousSeeAlbumText isEqualToString:@"Friends of Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];_seeAlbumSelectedRow = 1;
    }
    if ([previousSeeAlbumText isEqualToString:@"Only Me"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _seeAlbumSelectedRow = 2;
    }
    if ([previousSeeAlbumText isEqualToString:@"Everyone"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _seeAlbumSelectedRow = 3;
    }
    if ([previousSeeAlbumText isEqualToString:@"Just Invited Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _seeAlbumSelectedRow = 4;
    }
    
    
    
    
    
    if ([previousUploadPhotosText isEqualToString:@"Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:2] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _uploadPhotosSelectedRow = 0;
    }
    if ([previousUploadPhotosText isEqualToString:@"Friends of Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:2] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _uploadPhotosSelectedRow = 1;
    }
    if ([previousUploadPhotosText isEqualToString:@"Only Me"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:2] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _uploadPhotosSelectedRow = 2;
    }
 
    if ([previousUploadPhotosText isEqualToString:@"Everyone"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:2] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _uploadPhotosSelectedRow = 3;
    }
    if ([previousUploadPhotosText isEqualToString:@"Just Invited Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:4 inSection:2] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _uploadPhotosSelectedRow = 4;
    }

    
    
    
    if ([previousInviteOthersText isEqualToString:@"Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:3] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _inviteOthersSelectedRow=0;
    }
    if ([previousInviteOthersText isEqualToString:@"Friends of Friends"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:1 inSection:3] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _inviteOthersSelectedRow=1;
    }
    if ([previousInviteOthersText isEqualToString:@"Only Me"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:2 inSection:3] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _inviteOthersSelectedRow=2;
    }
    
    if ([previousInviteOthersText isEqualToString:@"Everyone"] ) {
        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:3 inSection:3] animated:YES scrollPosition:UITableViewScrollPositionNone];
        _inviteOthersSelectedRow=3;
    }

    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];

}

-(void)selectImagesByCapturing
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    [ipc setAllowsEditing:NO];
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
}

#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{

    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }

    
    imageToBeSentToServer = [info objectForKey:UIImagePickerControllerOriginalImage];

    float width,height;
    width = imageToBeSentToServer.size.width;
    height = imageToBeSentToServer.size.height;
    if (width>1024.0f && height>1024.0f) {
        if (width<height) {
            height = height/width*1024.0f;
            width=1024.0f;
        }else{
            width = width/height*1024.0f;
            height = 1024.0f;
        }
    }
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    
    [imageToBeSentToServer drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    imageToBeSentToServer = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    //NSLog(@"imageToBeSentToSe rver : %@" , imageToBeSentToServer);
    [allTemporaryImagesArrayyToDisplay addObject:imageToBeSentToServer];
    
    //NSLog(@"allTemporaryImagesArrayyToDisplay.count : %lu" , (unsigned long)allTemporaryImagesArrayyToDisplay.count);
    //NSLog(@"allTemporaryImagesArrayyToDisplay : %@" , allTemporaryImagesArrayyToDisplay);

 [picker dismissViewControllerAnimated:YES completion:nil];
    
    if (_type ==0) {
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[webServices sharedServices] uploadImagesTemporarilywithImage:imageToBeSentToServer withImageName:@"" aboutPhoto:@"My First Photo" cover:@"0" handler:^(NSData *data){
            [self connectionGotData:data];
            [hud hide:YES];
        }errorHandler:^(NSError *error){
            [self tempConnectionFailedWithError:error];
            [hud hide:YES];
        }];
    }
    else if(_type==1){
        
        NSString *uploadingUsername = [_otherUserName stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
        
        MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        [[webServices sharedServices] editGallerywithImage:imageToBeSentToServer userUploadingUsername:uploadingUsername withGalleryId:_albumIdToEdit cover:@"0" handler:^(NSData *data){
            [self connectionGotData:data];
            [hud hide:YES];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
            [hud hide:YES];
        }];
        
      
    }
    [selectedImagesCollectionView reloadData];

}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}
-(void)settingsSeePhotos:(NSInteger)seePhotos uploadPhotos:(NSInteger)uploadPhotos invite:(NSInteger)invite{
    _seeAlbumSelectedRow = seePhotos;
    _uploadPhotosSelectedRow= uploadPhotos;
    _inviteOthersSelectedRow = invite;
    
    NSLog(@" 3 values in create received : %ld , %ld , %ld" , (long)_seeAlbumSelectedRow , (long)_uploadPhotosSelectedRow, (long)_inviteOthersSelectedRow);

}

@end
