//
//  accordionTableViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 21/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PEMenuSectionHeaderView.h"

@interface accordionTableViewController : UITableViewController<SectionHeaderViewDelegate>

@end
