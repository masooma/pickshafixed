//
//  PEPSTabBarViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 21/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSTabBarViewController.h"

@interface PEPSTabBarViewController ()

@end

@implementation PEPSTabBarViewController{
    UITabBarItem *notificationItem;
    UITabBarItem *friendItem;
    UITabBarItem *messageItem;
    
    NSInteger notificationCount;
    NSInteger friendRequestCount;
    NSInteger messagesCount;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [NSTimer scheduledTimerWithTimeInterval:3.0f target:self selector:@selector(fetchCounts) userInfo:nil repeats:YES];
    
    UIColor *blueColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
    [self.tabBar setBackgroundImage:[UIImage new]];
    self.tabBar.backgroundColor = blueColor;
    [self setSelectedViewController:self.viewControllers[2]];
    
    [self setupTabBarItems];
    // Do any additional setup after loading the view.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
   
    
    /*
    messagesCount = 9;
    messageItem.badgeValue = [NSString stringWithFormat:@"%d",messagesCount];
     */
}
-(void)setupTabBarItems{
    [[UITabBar appearance] setTintColor:[UIColor whiteColor]]; // for unselected items that are gray
    [UITabBarItem.appearance setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor whiteColor]}
                                           forState:UIControlStateNormal];
    [UITabBarItem.appearance setTitleTextAttributes:
     @{NSForegroundColorAttributeName : [UIColor blackColor]}
                                           forState:UIControlStateSelected];
    self.tabBar.tintColor = [UIColor blackColor];
    
    notificationItem = self.tabBar.items[0];
    friendItem = self.tabBar.items[1];
    messageItem = self.tabBar.items[3];
    
    for (UITabBarItem *item in self.tabBar.items) {
        
        // this icon is used for selected tab and it will get tinted as defined in self.tabBar.tintColor
        item.selectedImage = item.image;
        // this way, the icon gets rendered as it is (thus, it needs to be green in this example)
        item.image = [item.image imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
        
        
    }
}
-(void)fetchCounts{
    
    //NSLog(@"CALLED FETCH COUNTS ! ");
    
    [[webServices sharedServices] getNotificationsFriendRequestsAndMessagesCountWithHandler:^(NSData *data){
        [self connectionGotCountData:data];
    }errorHandler:^(NSError *error){
       // [self connectionFailedWithError:error];
    }];
}
/*
- (void) tabBarController:(UITabBarController*)aTabBarController
  didSelectViewController:(UIViewController*)viewController
{
    viewController.tabBarItem.badgeValue = NULL;
}
*/
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)connectionGotCountData:(NSData *)data{
    NSDictionary *countDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
  // NSLog(@"countDict : %@" , countDict);
    


    NSInteger notif ;
    @try {
        notif = [countDict[@"notification"] integerValue];
    }
    @catch (NSException *exception) {
        notif = 0;
    }
    
        if (notif!=notificationCount) {
            if (notif>notificationCount) {
                
                /*
                BOOL isReachable = YES;
                NSDictionary *dataDict = [NSDictionary dictionaryWithObject:[NSNumber numberWithBool:isReachable]
                                                                     forKey:@"isReachable"];
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"reachabilityChanged" object:self userInfo:dataDict];
                 */
                
                [[NSNotificationCenter defaultCenter] postNotificationName:@"notificationCountChanged" object:self];
                
            }
            notificationCount = notif;
            if (notificationCount >= 0 ) {
                if (notificationCount == 0 ){
                    notificationItem.badgeValue =nil;
                }
                
           
            notificationItem.badgeValue =[NSString stringWithFormat:@"%ld",(long)(notificationCount)];
            }
        }
    if (notificationCount == 0){
        notificationItem.badgeValue =nil;
        
    }
    
    NSInteger msg;
    @try {
        msg = [countDict[@"messages"] integerValue];
    }
    @catch (NSException *exception) {
        msg = 0;
    }
    
    if (msg!=messagesCount) {
        if (msg>messagesCount) {
                
            NSLog(@"POSTING NOTIFICATION : MESSSAGE ! ");
                            [[NSNotificationCenter defaultCenter] postNotificationName:@"messageCountChanged" object:self];
        }
            
        messagesCount = msg;
        if (messagesCount >= 0) {
            if (messagesCount == 0){
                messageItem.badgeValue =nil;
                
            }
             
        messageItem.badgeValue = [NSString stringWithFormat:@"%ld",(long)(messagesCount)];
                
        }
                
    }
    if (messagesCount == 0){
        messageItem.badgeValue =nil;
        
    }
    
    NSInteger fr;
    @try {
        fr = [countDict[@"frind_request"] integerValue];
    }
    @catch (NSException *exception) {
        fr = 0;
    }
    
        if (fr!=friendRequestCount) {
            if (fr>friendRequestCount) {
                                [[NSNotificationCenter defaultCenter] postNotificationName:@"friendRequestCountChanged" object:self];
            }
            friendRequestCount = fr;
            if (friendRequestCount >= 0) {
                if (friendRequestCount == 0){
                    friendItem.badgeValue=nil;
                    
                }
                friendItem.badgeValue = [NSString stringWithFormat:@"%ld",(long)(friendRequestCount)];
                
            }
            
        }
    if (friendRequestCount == 0){
        friendItem.badgeValue=nil;
        
    }



       }

# pragma mark - Rotate
/*
-(BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    // You do not need this method if you are not supporting earlier iOS Versions
    return [self.selectedViewController shouldAutorotateToInterfaceOrientation:interfaceOrientation];
}

-(NSUInteger)supportedInterfaceOrientations
{
    return [self.selectedViewController supportedInterfaceOrientations];
}

-(BOOL)shouldAutorotate
{
    return YES;
}
*/
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
