//
//  PEPSViewAlbumViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 28/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MWPhotoBrowser.h"


@interface PEPSViewAlbumViewController : UIViewController<UICollectionViewDataSource , UICollectionViewDelegate, UICollectionViewDelegateFlowLayout ,MWPhotoBrowserDelegate>

@property (nonatomic ,assign) NSInteger type;// type 0: My Gallery 1:Somebody else's Gallery
@property (nonatomic ,assign) BOOL isPublic; // 0: notPublic 1: Outside Public Albums
//@property (nonatomic ,assign) NSInteger isNotification;// 0: not Notification 1: is notification
@property(nonatomic , strong) NSString *userId;
@property (nonatomic ,strong) NSString *albumID;
@property (nonatomic ,strong) NSString *albumName;
@property (nonatomic ,strong) NSString *albumDescription;
@property (nonatomic ,strong) NSString *userProfilePhotoUrl;
@property (nonatomic ,strong) NSString *coverPhotoURL;
@property (nonatomic ,strong) NSString *userName;
@property (nonatomic ,strong) NSString *likesCount;
@property (nonatomic ,assign) NSString *photosCount;
@property (nonatomic ,assign) NSString *invitesCount;
/*
@property (nonatomic ,strong) NSString *imageURLfromNotifications;
@property (nonatomic ,strong) NSString *imageIdFromNotifications;
@property (nonatomic ,strong) NSString *likesCountNotifications;
*/
@property(nonatomic ,strong) NSString *likeId;


@property (nonatomic, strong) NSMutableArray *photos;
@property (nonatomic, strong) NSMutableArray *photoId;
@property (nonatomic, strong) NSMutableArray *thumbs;
@end
