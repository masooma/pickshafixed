//
//  PEPSFriendsViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 16/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#include "friendRequestCollectionViewCell.h"

@interface PEPSFriendsViewController : UIViewController<UICollectionViewDelegate , UICollectionViewDataSource, UISearchBarDelegate, UITableViewDataSource , UITableViewDelegate,  UISearchDisplayDelegate, UIAlertViewDelegate , friendRequestCellDelegate>

@end
