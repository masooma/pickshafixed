//
//  PEPSHelpTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 28/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSHelpTableViewController.h"
#import "PEPSCreateEditTableViewController.h"

@interface PEPSHelpTableViewController (){
    NSMutableArray *headersArray;
    NSInteger openSection;
    NSArray *profileTitles;
    NSArray *whatIsPicksha;
    NSArray *aboutTitles;
    NSArray *howToUsePicksha;
    
    NSArray *whoCanSeeAlbum;
    NSArray *whoCanUploadPhotos;
    NSArray *WhoCanInviteOthers;
}

@end

@implementation PEPSHelpTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupView];
}

-(void)setupView{
    headersArray = [[NSMutableArray alloc] init];
    
    // ======================= album settings ======================
    
    if (_type ==1) {
        UIBarButtonItem *newBackButton = [[UIBarButtonItem alloc] initWithTitle:@"Back" style:UIBarButtonItemStylePlain target:self action:@selector(home:)];
        self.navigationItem.leftBarButtonItem=newBackButton;
        self.navigationItem.title = @"Album Settings" ;
        NSArray *settingsTitles = @[@"Who can see your Album?" , @"Who can upload photos?" , @"Who can invite others?" /*, @"Password" , @"About Us"*/ ];
        
        whoCanSeeAlbum = @[@"Friends",
                           @"Friends of Friends",
                           @"Only Me",
                           @"Everyone",
                           @"Just Invited Friends"];
        whoCanUploadPhotos= @[@"Friends",
                              @"Friends of Friends",
                              @"Only Me",
                              @"Everyone",
                              @"Just Invited Friends"];
        WhoCanInviteOthers= @[@"Friends",
                              @"Friends of Friends",
                              @"Only Me",
                              @"Everyone"];
        
        //  headersArray = [[NSMutableArray alloc] init];
        for(int i=0; i<[settingsTitles count]; i++){
            UINib *nib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
            PEMenuSectionHeaderView *header  = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
            header.titleLabel.text = settingsTitles[i];
            header.isOpen = NO;
            header.section = i;
            header.delegate = self;
            [headersArray addObject:header];
        }
    }
    
    // ======================= help ======================
    
    else if (_type==0){
        self.navigationItem.title = @"Help/FAQ" ;
        NSArray *sectionTitles = @[@"What is Picksha?" , @"How to use Picksha?" /*, @"Password" , @"About Us"*/ ];
        // profileTitles = @[@"Basic Info" , @"Languages" , @"Deactivate account" , @"Delete Account" ];
        whatIsPicksha = @[@"Picksha is a website to create online albums on specific topics or events. You can add photos to your albums and decide who can see the album and who can't. In addition, you can allow your friends or other people to upload their own pictures to your albums." ];
        
        howToUsePicksha= @[@"The site is quite simple to explain. Imagine you're at a party with friends, you make a lot of photos. Now, many great pictures are on many different cell phones, cameras or other media. Picksha offers you an easy way to upload all the images in an album together, so it saves you the hassle of individual images and send you all the pictures in one place. Each participant can upload his photos taken in the album.\n\nThere are of course innumerable applications for Picksha. So you can upload photos of a wedding in an album together for all guests, pictures of parties, concerts or other events. Just about every moment can be stated with Picksha and manage together with friends." ];
        //aboutTitles = @[@"Imprint/Contact" , @"Help/FAQ" , @"Team" ];
        
        //  headersArray = [[NSMutableArray alloc] init];
        for(int i=0; i<[sectionTitles count]; i++){
            UINib *nib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
            PEMenuSectionHeaderView *header  = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
            header.titleLabel.text = sectionTitles[i];
            header.isOpen = NO;
            header.section = i;
            header.delegate = self;
            [headersArray addObject:header];
        }
        //openSection = NSNotFound;
    }
    openSection = NSNotFound;
}
-(void)home:(UIBarButtonItem *)sender {
    if ([_delegate respondsToSelector:@selector(settingsSeePhotos:uploadPhotos:invite:)])
    {
        [_delegate settingsSeePhotos:_seeAlbumSelectedRow uploadPhotos:_uploadPhotosSelectedRow invite:_inviteOthersSelectedRow];
    }
    [self.navigationController  popViewControllerAnimated:YES];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    
    return headersArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    if (((PEMenuSectionHeaderView *)headersArray[section]).isOpen) {
        if (_type==0) {
            switch (section) {
                case 0:
                    return  whatIsPicksha.count;//profileTitles.count;
                    break;
                case 1:
                    return howToUsePicksha.count;
                    break;
                default:
                    break;
            }
        }
        
        else if (_type==1) {
            switch (section) {
                case 0:
                    return  whoCanSeeAlbum.count;
                    break;
                case 1:
                    return whoCanUploadPhotos.count;
                    break;
                case 2:
                     return WhoCanInviteOthers.count;
                     break;
                default:
                    break;
            }
        }
        
    }
    return 0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rowCell" forIndexPath:indexPath];
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    cell.textLabel.lineBreakMode = NSLineBreakByWordWrapping;
    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    cell.textLabel.numberOfLines = 0;
    
    if (_type==0) {
        switch (indexPath.section) {
            case 0:
                cell.textLabel.text =  whatIsPicksha[indexPath.row];//profileTitles[indexPath.row];
                break;
            case 1:
                cell.textLabel.text =  howToUsePicksha[indexPath.row];//@"Language";
                break;
            default:
                break;
        }
    
    }
    else {
        switch (indexPath.section) {
            case 0:
                cell.textLabel.text =  whoCanSeeAlbum[indexPath.row];//profileTitles[indexPath.row];
                        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_seeAlbumSelectedRow inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
                break;
            case 1:
                cell.textLabel.text =  whoCanUploadPhotos[indexPath.row];//@"Language";
                     [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_uploadPhotosSelectedRow inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
                break;
            case 2:
                cell.textLabel.text = WhoCanInviteOthers[indexPath.row];
                        [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:_inviteOthersSelectedRow inSection:2] animated:YES scrollPosition:UITableViewScrollPositionNone];
                 break;
            default:
                break;
        }
    }
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    return [headersArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (_type==0) {
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            return 200;
        }
        else
        {
            if (indexPath.section==0) {
                return 300;
            }
            else
                return 600;
        }
    }
    else
        return 44;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 48;
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    switch (indexPath.section) {
        case 0:
            _seeAlbumSelectedRow = indexPath.row;
            NSLog(@"for SECTION : %ld seeAlbumSelectedRow : %ld" ,(long)indexPath.section, (long)_seeAlbumSelectedRow);
        
                    
            break;
        case 1:
            _uploadPhotosSelectedRow = indexPath.row;
             NSLog(@"for SECTION : %ld selectedRow : %ld" ,(long)indexPath.section, (long)_uploadPhotosSelectedRow);
            
            break;
        case 2:
            _inviteOthersSelectedRow = indexPath.row;
             NSLog(@"for SECTION : %ld selectedRow : %ld" ,(long)indexPath.section, (long)_inviteOthersSelectedRow);
            
            break;
            
        default:
            break;
    }
    
    
    if ([tableView cellForRowAtIndexPath:indexPath].accessoryType != UITableViewCellAccessoryNone && [tableView cellForRowAtIndexPath:indexPath].tag == indexPath.section) {
        
        [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryCheckmark;
    }
    
}

-(void)tableView:(UITableView *)tableView didDeselectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
}

-(NSIndexPath*)tableView:(UITableView *)tableView willSelectRowAtIndexPath:(NSIndexPath*)indexPath {
    
    for ( NSIndexPath* selectedIndexPath in tableView.indexPathsForSelectedRows ) {
        if ( selectedIndexPath.section == indexPath.section )
            [tableView deselectRowAtIndexPath:selectedIndexPath animated:NO] ;
        
        [tableView cellForRowAtIndexPath:indexPath].tag = indexPath.section;
        
        if ([tableView cellForRowAtIndexPath:indexPath].accessoryType != UITableViewCellAccessoryNone && [tableView cellForRowAtIndexPath:indexPath].tag == indexPath.section) {
            [tableView cellForRowAtIndexPath:indexPath].accessoryType = UITableViewCellAccessoryNone;
        }
        
    }
    return indexPath ;
    
}

#pragma mark - Section Header Delegate

- (void)sectionHeaderView:(PEMenuSectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
    NSInteger countOfRowsToInsert;
    NSInteger countOfRowsToDelete ;
    
    if (_type==0) {
        switch (sectionOpened) {
            case 0:
                countOfRowsToInsert =  whatIsPicksha.count;//profileTitles.count;
                break;
            case 1:
                countOfRowsToInsert =  howToUsePicksha.count;//4;
                break;
            default:
                break;
        }
    }
    else {
        switch (sectionOpened) {
            case 0:
                countOfRowsToInsert =  whoCanSeeAlbum.count;
                break;
            case 1:
                countOfRowsToInsert =  whoCanUploadPhotos.count;
                break;
            case 2:
                 countOfRowsToInsert = WhoCanInviteOthers.count;
                 break;
            default:
                break;
        }
    }
    
    
    // NSInteger countOfRowsToInsert = 4;
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = openSection;
    if (previousOpenSectionIndex != NSNotFound) {
        
        PEMenuSectionHeaderView *previousOpenSection = (headersArray)[previousOpenSectionIndex];
        
        [previousOpenSection toggleOpenWithUserAction:NO];
        
        if (_type==0) {
            switch (previousOpenSectionIndex) {
                case 0:
                    countOfRowsToDelete = whatIsPicksha.count; //profileTitles.count;
                    break;
                case 1:
                    countOfRowsToDelete = howToUsePicksha.count;//4;
                    break;
                default:
                    break;
            }
        }
        else {
            switch (previousOpenSectionIndex) {
                case 0:
                    countOfRowsToDelete = whoCanSeeAlbum.count; 
                    break;
                case 1:
                    countOfRowsToDelete = whoCanUploadPhotos.count;
                    break;
                case 2:
                    countOfRowsToDelete = WhoCanInviteOthers.count;
                     break;
                default:
                    break;
            }
        }
        
        
        
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
        
        [previousOpenSection.disclosureButton setImage:[UIImage imageNamed:@"down.png"] forState:UIControlStateNormal];
    }
    
    // style the animation so that there's a smooth flow in either direction
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // apply the updates
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
    
    openSection = sectionOpened;
    
}

- (void)sectionHeaderView:(PEMenuSectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
    PEMenuSectionHeaderView *sectionInfo = (headersArray)[sectionClosed];
    
    sectionInfo.isOpen = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    openSection = NSNotFound;
}


@end
