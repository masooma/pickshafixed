//
//  PEPSCreateEditTableViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 22/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QBImagePickerController.h"
#import "PEMenuSectionHeaderView.h"
#import "PEPSHelpTableViewController.h"



@interface PEPSCreateEditTableViewController : UITableViewController<UICollectionViewDelegate , UIImagePickerControllerDelegate, UINavigationControllerDelegate,UICollectionViewDataSource , QBImagePickerControllerDelegate ,SectionHeaderViewDelegate , settingsControllerDelegate>

@property (nonatomic ,assign) NSInteger type;// type 0: Create Gallery 1:Edit Gallery
@property(nonatomic ,assign) NSInteger is_me;// type 0: my gallery 1:other user's gallery

@property (nonatomic ,assign)NSInteger seeAlbumSelectedRow;
@property (nonatomic ,assign)NSInteger uploadPhotosSelectedRow;
@property (nonatomic ,assign)NSInteger inviteOthersSelectedRow;

@property (nonatomic ,strong) NSString *albumIdToEdit;
@property (nonatomic ,strong) NSString *otherUserName;


@end
