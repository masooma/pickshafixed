//
//  PEPSDisplayImageViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 27/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEPSDisplayImageViewController : UIViewController
@property(nonatomic , strong) NSString *imageURLToDisplay;
@property(nonatomic , assign) NSInteger type;
@property(nonatomic , strong) UIImage *imageToDisplay;
@property (nonatomic,strong) NSURL *imageURL;
@end
