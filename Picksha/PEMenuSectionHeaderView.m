//
//  PEMenuSectionHeaderView.m
//  DailyTimes
//
//  Created by Pantera Engineering on 24/07/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import "PEMenuSectionHeaderView.h"

@implementation PEMenuSectionHeaderView

- (void)awakeFromNib {
    
    // set the selected image for the disclosure button
    [self.disclosureButton setImage:[UIImage imageNamed:@"up.png"] forState:UIControlStateSelected];
    
    // set up the tap gesture recognizer
    UITapGestureRecognizer *tapGesture = [[UITapGestureRecognizer alloc] initWithTarget:self
                                                                                 action:@selector(toggleOpen:)];
    [self addGestureRecognizer:tapGesture];
}

- (IBAction)toggleOpen:(id)sender {
    
    [self toggleOpenWithUserAction:YES];
}


/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/
- (void)toggleOpenWithUserAction:(BOOL)userAction {
    
    // toggle the disclosure button state
    self.isOpen = !self.isOpen;
    
    // if this was a user action, send the delegate the appropriate message
    if (userAction) {
        if (self.isOpen) {
            if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionOpened:)]) {
                [self.delegate sectionHeaderView:self sectionOpened:self.section];
                
                [self.disclosureButton setImage:[UIImage imageNamed:@"up.png"] forState:UIControlStateNormal];
            }
        }
        else {
            if ([self.delegate respondsToSelector:@selector(sectionHeaderView:sectionClosed:)]) {
                [self.delegate sectionHeaderView:self sectionClosed:self.section];
                
                [self.disclosureButton setImage:[UIImage imageNamed:@"down.png"] forState:UIControlStateNormal];
            }
        }
    }
}
@end
