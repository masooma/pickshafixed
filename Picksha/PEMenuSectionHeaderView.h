//
//  PEMenuSectionHeaderView.h
//  DailyTimes
//
//  Created by Pantera Engineering on 24/07/2014.
//  Copyright (c) 2014 Pantera Engineering. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@protocol SectionHeaderViewDelegate;

@interface PEMenuSectionHeaderView : UITableViewHeaderFooterView

@property (nonatomic, strong) IBOutlet UILabel *titleLabel;
@property (nonatomic, weak) IBOutlet UIButton *disclosureButton;
@property (nonatomic, assign) NSInteger section;
@property (nonatomic, assign) BOOL isOpen;

@property (nonatomic, weak) IBOutlet id <SectionHeaderViewDelegate> delegate;

- (void)toggleOpenWithUserAction:(BOOL)userAction;

@end
@protocol SectionHeaderViewDelegate <NSObject>

@optional
- (void)sectionHeaderView:(PEMenuSectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)section;
- (void)sectionHeaderView:(PEMenuSectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)section;

@end