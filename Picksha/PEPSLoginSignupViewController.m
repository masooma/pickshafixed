//
//  PEPSLoginSignupViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 21/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSLoginSignupViewController.h"
#import "PEPSPublicAlbumsViewController.h"
#import "PEPSTabBarViewController.h"
#import "fullPageWebViewController.h"
#import "MZFormSheetController.h"
#include "settingsFormViewController.h"
//#import "FBSDKCoreKit.h"

@interface PEPSLoginSignupViewController ()

@property(strong,nonatomic)NSString *JsonMsg;
@property(strong,nonatomic)NSString *email;
@property(strong,nonatomic)NSString *password;
@property(strong,nonatomic)NSString *username;
@property(strong,nonatomic)NSString *confirmPassword;


@end

@implementation PEPSLoginSignupViewController{

    IBOutlet UIButton *termsButton;
    IBOutlet UIButton *LoginButton;
    IBOutlet UIButton *createNewAccountButton;
    IBOutlet UILabel *termsLabel;
    IBOutlet UISwitch *termsSwitch;
    IBOutlet UIButton *forgotPasswordLabel;
    IBOutlet UIButton *registerButton;
    
    IBOutlet UIButton *publicAlbumButton;
    IBOutlet UIView *publicAlbumView;
NSMutableArray *returnedArray;
NSMutableData *chunkData;
//user *loggedInUser;
IBOutlet UITextField *textfield1;
IBOutlet UITextField *textfield2;
IBOutlet UITextField *textfield3;
IBOutlet UITextField *textfield4;
BOOL sessionChecked;
BOOL fbGotData;
NSDictionary *fbDictionary;
NSString *fetchedUserEmail;
//NSString *fetchedUserFirstName;
NSString *fetchedUserName;
NSString *fetchedUserId;

    IBOutlet FBLoginView *fbLogtinView;
NSString *fbUserId;
NSString *fbUserName;
NSString *fbUserEmail;
NSString *fbUserCountry;
UITapGestureRecognizer *tapRecognizer;
IBOutlet UIView *fbBackgroundClearView;
}

- (BOOL)shouldAutorotate
{
    return NO;
}
- (void)viewDidLoad {
    
    [super viewDidLoad];
    switch (_type)
    {
        case 0:
        {
            [[webServices sharedServices] checkIfUserLoggedInWithHandler:^(NSData *data){
                [self checkLoginConnectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
    }
    
     self.navigationController.navigationBar.barTintColor = [UIColor whiteColor];

    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor blackColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    fbLogtinView.readPermissions = @[@"public_profile", @"email", @"user_friends"];
    fbLogtinView.delegate=self;
    [self setNeedsStatusBarAppearanceUpdate];
    
    switch (_type) {
            // LOGIN
        case 0:
            textfield2.secureTextEntry = YES;
            textfield3.hidden = YES;
            textfield3.enabled = NO;
            textfield4.hidden = YES;
            textfield4.enabled = NO;
            termsSwitch.enabled = NO;
            termsSwitch.hidden = YES;
            termsLabel.hidden=YES;
            termsButton.enabled=NO;
            termsButton.hidden=YES;
          //  signUpButton.enabled = NO;
           // signUpButton.hidden = YES;
            registerButton.enabled=NO;
            registerButton.hidden=YES;
            self.navigationItem.title = @"Login" ;
            [self.navigationController setNavigationBarHidden:YES];
            publicAlbumButton.hidden=NO;
            publicAlbumButton.enabled=YES;
          
            break;
            //SIGNUP
        case 1:
            //SIGNUP !
            //NSLog(@"SIGNUP");
            termsButton.enabled=YES;
            termsButton.hidden=NO;
            textfield2.secureTextEntry=NO;
            LoginButton.hidden = YES;
            LoginButton.enabled = NO;
            createNewAccountButton.hidden = YES;
            createNewAccountButton.enabled = NO;
            publicAlbumView.hidden=YES;
            publicAlbumButton.hidden=YES;
            publicAlbumButton.enabled=NO;
            forgotPasswordLabel.hidden = YES;
            self.navigationItem.title = @ "Sign Up" ;
            [self.navigationController setNavigationBarHidden:NO];
            break;
        default:
            break;
    }
    [self setPlaceHolders];
    
   // [passwordTextfield setDelegate:self];
    
    UIBarButtonItem *myBarButtonItem = [[UIBarButtonItem alloc] init];
    myBarButtonItem.title = @"";
    self.navigationItem.backBarButtonItem = myBarButtonItem;

    
    //For Hiding Keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    switch (_type) {
            // LOGIN
        case 0:
        {
            textfield1.text=@"";
            textfield2.text=@"";
            [self.navigationController setNavigationBarHidden:YES];
            textfield2.secureTextEntry = YES;
            
            break;
        }
        case 1:
        {
            //SIGNUP !
            [self.navigationController setNavigationBarHidden:NO];
            textfield2.secureTextEntry=NO;
            break;
        }
        default:
            break;
    }
}
-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [textfield1 resignFirstResponder];
    [textfield2 resignFirstResponder];
    [textfield3 resignFirstResponder];
    [textfield4 resignFirstResponder];
    
}
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}

-(UIStatusBarStyle)preferredStatusBarStyle{
    return UIStatusBarStyleLightContent;
}

-(BOOL)textFieldShouldReturn:(UITextField *)textField{
    
    if (textField.tag <2) {
        UITextField *nextField = (UITextField *)[self.view viewWithTag:textField.tag+1];
        [nextField becomeFirstResponder];
    }
    else{
        [self.view endEditing:YES];
    }
    return NO;
}
-(void)textFieldDidEndEditing:(UITextField *)textField{
    // [self.scroll setContentOffset:CGPointZero animated:YES];
}
-(BOOL)textFieldShouldEndEditing:(UITextField *)textField{
    return YES;
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setPlaceHolders{
    switch (_type) {
        case 0:
            textfield1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email"];//attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
            
            textfield2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password"];// attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
            break;
        case 1:
            textfield1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email"];// attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
            textfield2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username"];// attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
            textfield3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Password" ];//attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
            textfield4.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" ];//attributes:@{NSForegroundColorAttributeName: [UIColor blackColor]}];
            break;
        default:
            break;
    }
   
}

-(void)shakeIt:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.07];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}


- (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}
-(BOOL)validatePassword:(NSString *)pwd1 secondPassword : (NSString *)pwd2
{
    if([pwd1 isEqualToString:pwd2]){
        return  YES;
    }
    return NO;
}
-(BOOL)getUserInfo{
    
    switch (_type) {
            
            // LOGIN
        case 0:
            if ([textfield1.text  isEqual: @""] || ![self validateEmail:textfield1 .text]){
                [self shakeIt:textfield1];
                [textfield1 becomeFirstResponder];
            }
            else if ([textfield2.text   isEqual: @""] ){
                [self shakeIt:textfield2];
                [textfield2 becomeFirstResponder];
            }
            else
            {
                _email = textfield1.text;
                _password = textfield2.text;
                return YES;
            }
            break;
            
            //SIGNUP
        case 1:
            if ([textfield1.text  isEqual: @""] || ![self validateEmail:textfield1 .text]){
                [self shakeIt:textfield1];
                [textfield1 becomeFirstResponder];
            }
            else if ([textfield2.text   isEqual: @""] ){
                [self shakeIt:textfield2];
                [textfield2 becomeFirstResponder];
            }
            else if ([textfield3.text   isEqual: @""]){
                [self shakeIt:textfield3];
                [textfield3 becomeFirstResponder];
            }
            else if ([textfield4.text   isEqual: @""] ){
                [self shakeIt:textfield4];
                [textfield4 becomeFirstResponder];
            }
            else if (![self validatePassword:textfield4.text secondPassword:textfield3.text]){
                [self shakeIt:textfield4];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Passwords don't match!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [textfield4 becomeFirstResponder];
            }


            else
            {
                _email = textfield1.text;
                _username = textfield2.text;
                _password = textfield3.text;
               // _confirmPassword = textfield4.text;
                return YES;
            }
            break;
            
        default:
            break;
    }

 
    return NO;
}
- (IBAction)createNewAccountOnTap:(id)sender {
    PEPSLoginSignupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupScene"];
    vc.type=1;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)tapOnPublicAlbums:(id)sender {
    
     PEPSPublicAlbumsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"publicAlbumsScene"];
    [self.navigationController pushViewController:vc animated:YES];
   // NSLog(@"public albums tapped!");

}

#pragma mark - CONNECTION

-(void)startLoginWithUserName:(NSString *)username password:(NSString *)password{
    [[webServices sharedServices] attemptLoginWithUsername:username password:password handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}


-(void)connectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSInteger code = [returnedDict[@"code"] integerValue];
    
    //NSLog(@"returnedDict : %@" , returnedDict);
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    
    switch (_type) {
        case 0:
            switch (code) {
                case 200:
                {
                    NSArray *userArray =returnedDict[@"user"];
                    
                    [[webServices sharedServices] setMyUser:userArray[0]];
                    
                    PEPSTabBarViewController *tabBarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarScene"];
                    //  tabBarViewController.userID = _userID;
                    [self presentViewController:tabBarViewController animated:YES completion:nil];

                    break;
                }
                case 201:
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Error", nil)
                                                                    message:NSLocalizedString(@"Invalid Email or Password.", nil)
                                                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    [alert show];
                    break;
                
                }
                case 300:
                {
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:NSLocalizedString(@"Login Error", nil)
                                                                    message:NSLocalizedString(@"Please activate your activation link in your mailbox.", nil)
                                                                   delegate:nil cancelButtonTitle:NSLocalizedString(@"OK", nil) otherButtonTitles:nil];
                    [alert show];
                    break;
                    
                }
                default:
            break;
            }
    }
    /*
    switch (code) {
        case 200:
        {
            loggedInUser.userID=[returnedDict[@"msg"][@"User"][@"user_id"]integerValue];
            loggedInUser.firstName=returnedDict[@"msg"][@"User"][@"first_name"] ;
            loggedInUser.lastName=returnedDict[@"msg"][@"User"][@"last_name"];
            loggedInUser.email=returnedDict[@"msg"][@"User"][@"email"];
            loggedInUser.userID = [returnedDict[@"msg"][@"User"][@"user_id"] integerValue ];
            _userID = returnedDict[@"User"][@"msg"][@"user_id"];
            
            NSLog(@"LOGIN !!! USER ID : %ld" ,(long)loggedInUser.userID);
            tabBarViewController *tabBarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarControllerScene"];
            //  tabBarViewController.userID = _userID;
            [self presentViewController:tabBarViewController animated:YES completion:nil];
            break;
        }
            
        case 201:
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Alert!"
                                                            message:@"Incorrect Login Details"
                                                           delegate:self
                                                  cancelButtonTitle:@"OK"
                                                  otherButtonTitles:nil];
            [alert show];
            [self shakeIt: usernameTextfield];
            [passwordTextfield isFirstResponder];
            break;
        }
            
        default:
            break;
    }
    */
}


-(void)checkLoginConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSString *code = returnedDict[@"code"];
    
    if ([code isEqualToString:@"200"]) {
        //NSLog(@"user Already logged in!");
        PEPSTabBarViewController *tabBarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarScene"];
        [self presentViewController:tabBarViewController animated:YES completion:nil];
        NSArray *userArray =returnedDict[@"user"];
        
        [[webServices sharedServices] setMyUser:userArray[0]];
        
        
    }
    //NSLog(@"returnedDict : %@" , returnedDict);
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


- (IBAction)tapOnSignInButton:(id)sender {
    if ([self getUserInfo])
    {
        //NSLog(@"starting LOGIN");
        //   [self logIn];
        
        [self startLoginWithUserName:_email password:_password];
    }
    
}
- (IBAction)tapOnRegisterButton:(id)sender {
    if ([self getUserInfo])
    {
        //NSLog(@"starting REGISTER");
        
        if([termsSwitch isOn])

        [self startSignUpWithUserName:_username password:_password email:_email];
        
        else{
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please read the terms and conditions." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        }
        
    }
    
}
-(void)startSignUpWithUserName:(NSString *)username password:(NSString *)password email:(NSString *)email{
    
    [[webServices sharedServices] attemptSignUpUserWithEmail:email password:password username:username handler:^(NSData *data){
        [self signUpConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
 }
-(void)signUpConnectionGotData:(NSData *)data{
        [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        //NSLog(@"returnedDict for SIGNUP : %@" , returnedDict);
    
    NSString *code = returnedDict[@"code"];
    NSString *message = returnedDict[@"message"];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    if ([code isEqualToString:@"200"]) {
        [self.navigationController popToRootViewControllerAnimated:NO];
    }
}
- (IBAction)termsOnTap:(id)sender {
    fullPageWebViewController *viewController = [self.storyboard instantiateViewControllerWithIdentifier:@"fullPageWebViewScene"];
    viewController.urlString =@"http://www.picksha.com/home/terms";
    [self.navigationController pushViewController:viewController animated:YES];
}
- (IBAction)forgotPasswordOnTap:(id)sender {
    
    settingsFormViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"formScene"];
    
    vc.type = 5;
    
    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
    
   if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
            formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-300, self.view.frame.size.height/4);
    } else {
            formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-30, self.view.frame.size.height/2);
    }
    

    formSheet.shadowRadius = 5.0;
    formSheet.shadowOpacity = 0.3;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    [[MZFormSheetController sharedBackgroundWindow] setBackgroundBlurEffect:YES];
    [[MZFormSheetController sharedBackgroundWindow] setBlurRadius:2.0];
    [[MZFormSheetController sharedBackgroundWindow] setBackgroundColor:[UIColor clearColor]];
    
    [formSheet setMovementWhenKeyboardAppears:MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard];
    
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromBottom;
    [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController)
     {
         
     }];
    
}

#pragma mark - FACEBOOK LOGIN

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView
                            user:(id<FBGraphUser>)user {
    //NSLog(@"FACEBOOK User's INFO : %@",(NSDictionary *)user);
    fbGotData = YES;
    fbDictionary = (NSDictionary *)user;
    
    fetchedUserEmail =  fbDictionary[@"email"];
    fetchedUserId = fbDictionary[@"id"];
    fetchedUserName = fbDictionary[@"name"];
    //  fetchedUserId = fbDictionary[@"id"];
    
    // if (sessionChecked) {
   [self startLoginWithFacebook];
    // }
}

-(void)startLoginWithFacebook{
    
  /*  FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc]
                                  initWithGraphPath:@"/{user-id}/friendlists"
                                  parameters:nil
                                  HTTPMethod:@"GET"];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection,id result,NSError *error) {
        NSLog(@"%@",result);
    }];
    */
    if([fbDictionary[@"verified"] integerValue]==1){
        
        
        [[webServices sharedServices]attemptFacebookLoginWithfacebookId:fetchedUserId email:fetchedUserEmail handler:^(NSData *data){
            [self fbConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error ];
        }];
    }
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)fbConnectionGotData:(NSData *)data{
        [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *fbResponseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //  NSInteger code = [fbResponseDictionary[@"code"] integerValue];
    //NSLog(@"fbResponseDictionary : %@" , fbResponseDictionary);
    //  NSLog(@"code : %ld" , (long)code);
  
    if (fbResponseDictionary !=nil) {
        
        fbUserId=fbResponseDictionary[@"User"][@"user_id"];
        fbUserCountry=fbResponseDictionary[@"User"][@"country"];
        fbUserName=fbResponseDictionary[@"User"][@"name"];
        fbUserEmail=fbResponseDictionary[@"User"][@"email"];
        
        NSArray *userArray =fbResponseDictionary[@"user"];
        
        [[webServices sharedServices] setMyUser:userArray[0]];
        
        PEPSTabBarViewController *tabBarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarScene"];
        //  tabBarViewController.userID = _userID;
        [self presentViewController:tabBarViewController animated:YES completion:nil];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error!"
                                                        message:@"Something went wrong."
                                                       delegate:self
                                              cancelButtonTitle:@"OK"
                                              otherButtonTitles:nil];
        [alert show];
    }
}


@end
