//
//  PEPSProfileViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 09/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSProfileViewController.h"
#import <CoreImage/CoreImage.h>
#import "UIImageView+WebCache.h"
#import <QuartzCore/QuartzCore.h>
#import "PEPSPicturesViewController.h"
#import "PEPSPicturesCollectionViewController.h"
#import "PEPSCreateEditTableViewController.h"
#import "PEPSViewAlbumViewController.h"
#import "SOConversationViewController.h"
#import "PopUpViewController.h"
#import "PEPSDisplayImageViewController.h"
#import "AsyncImageView.h"

@interface PEPSProfileViewController ()

@end

@implementation PEPSProfileViewController{
    
    NSArray *searchAlbumsArray;
    
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    BOOL toggleIsOn;
    
    IBOutlet UILabel *lineLabel;
    IBOutlet UIImageView *friendsImageView;
    IBOutlet UIImageView *albumsImageView;
    IBOutlet UIButton *viewPictureButton;
    IBOutlet NSLayoutConstraint *infoViewHeightConstraint;
    IBOutlet UIView *infoView;
    IBOutlet UIButton *showHideButton;
    IBOutlet NSLayoutConstraint *heightConstraint;
    
    IBOutlet UIBarButtonItem *createGalleryBarButtonItem;
    
    IBOutlet UIView *upperCompleteView;
    IBOutlet UIImageView *backgroundImageView;
    
    IBOutlet UIButton *friendshipButton;

    IBOutlet UIButton *chatButton;
    IBOutlet UILabel *albumsCountLabel;
    IBOutlet UILabel *friendsCountLabel;
    IBOutlet UILabel *nameLabel;
    
    IBOutlet UIImageView *frontProfileImageView;
    
    IBOutlet UISegmentedControl *albumSegmentedControl;
    NSArray *returnedDataArray;
    NSMutableArray *allAlbumsOfaSpecificFriend;
    NSDictionary *profileCompleteInfoDictionary;
    IBOutlet UICollectionView *albumsCollectionView;
  //  NSString *username;
    NSString *numberOfAlbums;
    NSString *numberOfFriends;
    NSString *profileImageURL;
    NSString *isFriendCode;
    NSString *isFriendMessage;
    NSString *friendshipId;
    NSString *userId;
    IBOutlet UIView *endFriendshipView;
    IBOutlet UIButton *yesButton;
    IBOutlet UIButton *noButton;
 
    UIRefreshControl *refreshControl;
    
    UITableView *searchTableView;
}

- (void)viewDidLoad {
    [super viewDidLoad];

    NSLog(@"TYPE FRR PRO : %ld" , (long)_type);
    
    toggleIsOn = 1;
    myId =[[webServices sharedServices] myUser][@"id"];
   
    if (_type == 1) {
        
        if ([_otherUserUserId integerValue] == [myId integerValue]) {
            
            _type = 0;
           
        }

    }

  //  [self setUpRefreshControl];
   // [albumsCollectionView setContentOffset:CGPointMake(0,0) animated:YES];
 
    
    
    allAlbumsOfaSpecificFriend = [[NSMutableArray alloc] init];
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@""];
    [self setUpRefreshControl];
 
    [self setUpLayout];
  
   
    
}

-(void)setUpLayout{
    
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    frontProfileImageView.layer.cornerRadius = frontProfileImageView.frame.size.height/2;
    frontProfileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    frontProfileImageView.layer.borderWidth = 3.0;
    frontProfileImageView.clipsToBounds = YES;
    
    NSDictionary *attributes = [NSDictionary dictionaryWithObjectsAndKeys:
                                [UIFont fontWithName:@"Helvetica" size:14.0], NSFontAttributeName,
                                [UIColor whiteColor], NSForegroundColorAttributeName, nil];
    
    switch (_type) {
        case 0:{
            NSLog(@"case 0 for my profile");
            albumSegmentedControl.selectedSegmentIndex = 1;
            albumSegmentedControl.hidden = NO;
            albumSegmentedControl.enabled = YES;
            friendshipButton.hidden=YES;
            friendshipButton.enabled = NO;
            chatButton.hidden= YES;
            chatButton.enabled=NO;
            createGalleryBarButtonItem.enabled=YES;
            [createGalleryBarButtonItem setBackgroundImage:[UIImage imageNamed:nil] forState:UIControlStateNormal barMetrics:UIBarMetricsDefault];
            
            [[webServices sharedServices] viewMyAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
          //  [self getAdminUsername];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            [albumSegmentedControl setTitleTextAttributes:attributes forState:UIControlStateNormal];
            
            [albumSegmentedControl setTitleTextAttributes:attributes forState:UIControlStateSelected];
            break;
        }
        case 1:{
            
            albumSegmentedControl.hidden = YES;
            albumSegmentedControl.enabled = NO;
            friendshipButton.hidden=NO;
            friendshipButton.enabled = YES;
            chatButton.hidden= NO;
            chatButton.enabled=YES;
            createGalleryBarButtonItem.enabled=NO;
            [self.navigationItem setRightBarButtonItem:nil];
            [self getAdminProfileInfo];
            [self getAlbumsForSpecificFriendWithUserId];
            
            
            break;
            
        }
        default:
            break;
    }
}
-(void)setUpRefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [albumsCollectionView addSubview:refreshControl];
    // [self showPullToRefreshMessage:NO];
    
    
    [refreshControl addTarget:self action:@selector(refreshAlbums:) forControlEvents:UIControlEventValueChanged];
    
    albumsCollectionView.alwaysBounceVertical = YES;

   
    /////////////////////////////////////////////
    [refreshControl endRefreshing];
}
- (IBAction)refreshAlbums:(id)sender {
    //[self showPullToRefreshMessage:NO];
    
    if (_type == 0) {

    
    switch (albumSegmentedControl.selectedSegmentIndex)
    {
        case 0:{

            [[webServices sharedServices] viewPublicAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        }
            break;
        case 1:{
 
            [[webServices sharedServices] viewMyAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        }
            break;
        case 2:{

            [[webServices sharedServices] viewFriendsAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        }
            break;
        default:
            break;
    }
        
    }
    
    else if (_type == 1){
    
        [self getAlbumsForSpecificFriendWithUserId];
        
    }
      [refreshControl endRefreshing];
}
-(void)showPullToRefreshMessage:(BOOL)show{
    if (show) {
        refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull Down to refresh"];
    }else if (!show){
        
        refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating albums"];
    }
  
    
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    /*
    NSNumber *value = [NSNumber numberWithInt:UIInterfaceOrientationPortrait];
    [[UIDevice currentDevice] setValue:value forKey:@"orientation"];
    */
    //[self setUpRefreshControl];
    //[refreshControl endRefreshing];
    
    myId =[[webServices sharedServices] myUser][@"id"];
    if (_type == 1) {
        
        if ([_otherUserUserId integerValue] == [myId integerValue]) {
            NSLog(@"my id is : %@ , _otherUserUserId  :%@" , myId , _otherUserUserId);
            _type = 0;
            NSLog(@"assigning type 0");
        }
        
    }
    
    self.tabBarController.tabBar.hidden = NO;


    

    NSLog(@"my user is: %@",[[webServices sharedServices] myUser]);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    NSLog(@"myUserName : %@ " , myUserName);
    NSLog(@"myUserId : %@ " , myId);

    
    [[webServices sharedServices] deleteTemporaryPicturesUploadedByUserId:myId handler:^(NSData *data){
       // [self sendFriendRequestConnectionGotData:data];
    }errorHandler:^(NSError *error){
        // [self connectionFailedWithError:error];
    }];
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
   
        self.navigationController.navigationBar.translucent = YES;
    switch (_type) {
        case 0:{
            
            [self getAdminProfileInfo];
            break;
        }
        case 1:{

            [self getAdminProfileInfo];
           // [self getAlbumsForSpecificFriendWithUserId];
          
            break;
            
        }
        default:
            break;
    }

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
    // Dispose of any resources that can be recreated.
}
- (IBAction)showHideButtonOnTap:(id)sender {
    
    if(toggleIsOn) {
        NSLog(@"Toggle is off ! height 0");
        
        frontProfileImageView.hidden=YES;
        viewPictureButton.enabled=NO;
        nameLabel.hidden=YES;
        albumsCountLabel.hidden=YES;
        friendsCountLabel.hidden=YES;
        albumsImageView.hidden=YES;
        friendsImageView.hidden=YES;
        lineLabel.hidden= YES;
        if (_otherUserUserId == myId) {
                    _type = 0;
        }
        if (_type==1){
            friendshipButton.hidden=YES;
            friendshipButton.enabled = NO;
            chatButton.hidden = YES;
            chatButton.enabled = NO;
            }

        
        infoViewHeightConstraint.constant = 22;
        [infoView updateConstraints];
        [UIView animateWithDuration:0.0 animations:^(void){
            [infoView layoutIfNeeded];
        }];
        [showHideButton setSelected:YES];
    }
    else{
        NSLog(@"Toggle is on ! height 100");
        
        frontProfileImageView.hidden=NO;
        viewPictureButton.enabled=YES;
        nameLabel.hidden=NO;
        albumsCountLabel.hidden=NO;
        friendsCountLabel.hidden=NO;
        albumsImageView.hidden=NO;
        friendsImageView.hidden=NO;
        lineLabel.hidden= NO;
        
        if (_otherUserUserId == myId) {
            _type = 0;
        }
        if (_type==1){
            friendshipButton.hidden=NO;
            friendshipButton.enabled = YES;
            chatButton.hidden = NO;
            chatButton.enabled = YES;
        }
        
        infoViewHeightConstraint.constant = 100.0;
        [infoView updateConstraints];
        [UIView animateWithDuration:0.0 animations:^(void){
            [infoView layoutIfNeeded];
        }];
        [showHideButton setSelected:NO];
        
        
    }
    toggleIsOn = !toggleIsOn;
}

-(void)getAlbumsForSpecificFriendWithUserId{


    [[webServices sharedServices] getAlbumsForUserId:_otherUserUserId handler:^(NSData *data){
        [self getalbumsForSpecificFriendConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

}
-(void)getalbumsForSpecificFriendConnectionGotData:(NSData *)data{
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@""];
    [refreshControl endRefreshing];
    
   //  [self showPullToRefreshMessage:YES];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
 //   [allAlbumsOfaSpecificFriend removeAllObjects];
    allAlbumsOfaSpecificFriend = returnedDict[@"Data"];
    NSLog(@"allAlbumsOfaSpecificFriend : %@" , allAlbumsOfaSpecificFriend);
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [albumsCollectionView reloadData];
}
- (IBAction)albumSegmentIndexChanged:(id)sender {
    switch (albumSegmentedControl.selectedSegmentIndex)
    {
        case 0:{

            [[webServices sharedServices] viewPublicAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        }
            break;
        case 1:{

            [[webServices sharedServices] viewMyAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        }
            break;
        case 2:{

            [[webServices sharedServices] viewFriendsAlbumsWithHandler:^(NSData *data){
                [self connectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];

        }
            break;
        default:
            break; 
    }
}

- (IBAction)createAlbumOnTap:(id)sender {
    PEPSCreateEditTableViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"createEditAlbumScene"];
    [self.navigationController pushViewController:vc animated:YES];

}
- (IBAction)friendshipOnTap:(id)sender {
    
    [[webServices sharedServices] isFriendWithUserWithId:_otherUserUserId handler:^(NSData *data){
        [self isFriendConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    

   
    
        
}
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    
    switch (actionSheet.tag ) {
        case 2002:
            if (buttonIndex == 0)
            {
                NSLog(@"NO");
                NSLog(@"case 2002 for deleting");
        
            }
            else
            {
                NSLog(@"YES");
                NSLog(@"case 2002 for deleting");
                NSLog(@"deleting user with user id : %@" , _otherUserUserId);
                
                
                [[webServices sharedServices] deleteFriendWithUserId:friendshipId handler:^(NSData *data){
                    [self deleteFriendConnectionGotData:data];
                }errorHandler:^(NSError *error){
                    [self connectionFailedWithError:error];
                }];
                
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
            }
            break;
        case 2001:
            if (buttonIndex == 0)
            {
                NSLog(@"NO");
                NSLog(@"case 2001 for adding");
            }
            else
            {
                NSLog(@"YES");
                NSLog(@"case 2001 for adding");
                NSLog(@"aading user with user id : %@" , _otherUserUserId);
  
                
                
                [[webServices sharedServices] sendFriendRequestTo:_otherUserUserId from:myId handler:^(NSData *data){
                    [self sendFriendRequestConnectionGotData:data];
                }errorHandler:^(NSError *error){
                    [self connectionFailedWithError:error];
                }];
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                
            }
            break;
            
        default:
            break;
    }
}

- (IBAction)chatOnTap:(id)sender {
    SOConversationViewController *chatTableView = [self.storyboard instantiateViewControllerWithIdentifier:@"conversationScene"];
    
    NSLog(@"CHAT ON TAP !!! : _otherUserUserId : %@ , _otherUserUsername : %@ profileImageURL : %@"  , _otherUserUserId , _otherUserUsername , profileImageURL);
    
    chatTableView.messageFromUserId  =  _otherUserUserId;
    chatTableView.messageFromUserName  =  _otherUserUsername;
    chatTableView.messageFromUserProfilePicture =profileImageURL;
    
    
    chatTableView.myUserId = myId;
    
    [self.navigationController pushViewController:chatTableView animated:YES];

}
#pragma mark - Collection View Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{   if(_type==0)
    return returnedDataArray.count;
    else
        return allAlbumsOfaSpecificFriend.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
   // [refreshControl endRefreshing];
   
    
    
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
    AsyncImageView *imageView = (AsyncImageView*)[cell viewWithTag:1];
    AsyncImageView *bkImageView = (AsyncImageView *)[cell viewWithTag:6];
    UILabel *albumNameLabel  = (UILabel *)[cell viewWithTag:2];
    UILabel *invitesLabel = (UILabel *)[cell viewWithTag:3];
    UILabel *likesLabel = (UILabel *)[cell viewWithTag:4];
    UILabel *photosLabel = (UILabel *)[cell viewWithTag:5];
    
    if (_type == 0) {
    
    NSString *albumName = [returnedDataArray objectAtIndex:indexPath.row][@"albumName"];
    NSString *coverPhotoURL = [returnedDataArray objectAtIndex:indexPath.row][@"coverPhoto"];
    NSString *invitesCount = [returnedDataArray objectAtIndex:indexPath.row][@"invitiesCount"];
    NSString *likesCount = [returnedDataArray objectAtIndex:indexPath.row][@"likesCount"];
    NSString *photosCount = [returnedDataArray objectAtIndex:indexPath.row][@"photosCount"];
        
        
        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",coverPhotoURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
        [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
        [bkImageView sd_setImageWithURL:theURL];

   // [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]] placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
   // [bkImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]]];
     /*   imageView.image=[UIImage imageNamed:@"placeholderPahaar"];
        bkImageView.image=nil;
        imageView.showActivityIndicator=YES;
      */
  
     
  //        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",coverPhotoURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
        
        NSLog(@"URL NEW : %@" , theURL);
      /*
        imageView.imageURL=theURL;
        bkImageView.imageURL =theURL;
       */
       // bkImageView.image = imageView.image;
    albumNameLabel.text = albumName;
    invitesLabel.text =  [ NSString stringWithFormat:@"%@ Invited" , invitesCount];
    likesLabel.text = likesCount;
    photosLabel.text = [ NSString stringWithFormat:@"%@ Photos" , photosCount];
    }
    if (_type == 1) {
        
        NSString *albumName = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"albumName"];
        NSString *coverPhotoURL = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"coverPhoto"];
        NSString *invitesCount = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"invitiesCount"];
        NSString *likesCount = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"likesCount"];
        NSString *photosCount = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"photosCount"];
        
        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",coverPhotoURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
        [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
        [bkImageView sd_setImageWithURL:theURL];
        
       /* imageView.image=[UIImage imageNamed:@"placeholderPahaar"];
        bkImageView.image=nil;
        imageView.showActivityIndicator=YES;
        imageView.imageURL =theURL;
        bkImageView.imageURL =theURL;
        
        */
        //[imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]] placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
       // [bkImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]]];
        albumNameLabel.text = albumName;
        invitesLabel.text =  [ NSString stringWithFormat:@"%@ Invited" , invitesCount];
        likesLabel.text = likesCount;
        photosLabel.text = [ NSString stringWithFormat:@"%@ Photos" , photosCount];
    }


    
    return cell;
}




-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *albumIdToSend;
    NSString *albumName;
    NSString *albumDescription;
    NSString *coverPhotoURL;
    NSString *userName;
    NSString *invitesCount;
    NSString *likesCount;
    NSString *photosCount;
    
    if (_type == 0) {
   

    albumIdToSend = [returnedDataArray objectAtIndex:indexPath.row][@"albumId"];
    albumName = [returnedDataArray objectAtIndex:indexPath.row][@"albumName"];
    albumDescription = [returnedDataArray objectAtIndex:indexPath.row][@"description"];
    coverPhotoURL = [returnedDataArray objectAtIndex:indexPath.row][@"coverPhoto"];
    userName = [returnedDataArray objectAtIndex:indexPath.row][@"from"];
    invitesCount = [returnedDataArray objectAtIndex:indexPath.row][@"invitiesCount"];
    likesCount = [returnedDataArray objectAtIndex:indexPath.row][@"likesCount"];
    photosCount = [returnedDataArray objectAtIndex:indexPath.row][@"photosCount"];
        
    }
    else if (_type == 1) {
        
        
        albumIdToSend = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"albumId"];
        albumName = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"albumName"];
        albumDescription = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"description"];
        coverPhotoURL = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"coverPhoto"];
        userName = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"from"];
        invitesCount = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"invitiesCount"];
        likesCount = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"likesCount"];
        photosCount = [allAlbumsOfaSpecificFriend objectAtIndex:indexPath.row][@"photosCount"];
        
    }
    PEPSViewAlbumViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewAlbumScene"];
    
    NSLog(@"FROM : %@" ,returnedDataArray[0][@"from"] );
    
    if ([returnedDataArray[indexPath.row][@"from"] isEqualToString: myUserName]) {
        
         NSLog(@"Sending type 0 to View albums");
            vc.type = 0;
    }
    else {
        NSLog(@"Sending type 1 to View albums");
        vc.type = 1;
    }

    
    vc.albumID = albumIdToSend;
    vc.albumDescription = albumDescription;
    vc.albumName = albumName;
    vc.coverPhotoURL = coverPhotoURL;
    vc.userName = userName;
    vc.likesCount = likesCount;
    vc.photosCount = photosCount;
    vc.invitesCount = invitesCount;
    NSLog(@"userId from profile to albums view: %@ " , userId );
    vc.userId = userId;
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:vc animated:YES];
}


#pragma mark - CONNECTION

// get Username

/*

-(void)getAdminUsername{
    
    [[webServices sharedServices] getAdminProfileImageAndUsernameWithHandler:^(NSData *data){
        [self getAdminUsernameConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
}
 */

/*
-(void)getAdminUsernameConnectionGotData:(NSData *)data{
   
        NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        username= returnedDict[@"name"];
    
        NSLog(@"username : %@" , username);
        [self getAdminProfileInfo];
        
        MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
        [hud setMode:MBProgressHUDModeText];
        // [hud setLabelText:responseDictionary[@"message"]];
        [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    

}
*/
// get complete profile info

-(void)getAdminProfileInfo{
    if (_type == 0) {
        NSLog(@"calling web service for Admin PRofile with MY username : %@", myUserName);
        
        NSString *fixedUsername =  [myUserName stringByReplacingOccurrencesOfString:@" " withString:@"%20" ];
        
        [[webServices sharedServices] getProfileInfoWithUsername:fixedUsername handler:^(NSData *data){
            [self getAdminProfileInfoConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        NSLog(@"called Admin PRofile");
    }
    else if (_type == 1) {
        NSLog(@"calling web service for other user's  PRofile with username : %@", _otherUserUsername);
        NSString *fixedUsername =  [_otherUserUsername stringByReplacingOccurrencesOfString:@" " withString:@"%20" ];
        
        [[webServices sharedServices] getProfileInfoWithUsername:fixedUsername handler:^(NSData *data){
            [self getAdminProfileInfoConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        NSLog(@"called other user's PRofile");
    }
    
    //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)getAdminProfileInfoConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSLog(@" getAdminProfileInfoConnectionGotData returnedDict for %@" , returnedDict);
    profileCompleteInfoDictionary= returnedDict[@"Data"];
    NSArray *friendsArray =returnedDict[@"Friends"];
    numberOfFriends= [NSString stringWithFormat:@"%lu",(unsigned long)friendsArray.count];

    NSLog(@"profileCompleteInfoArray : %@" , profileCompleteInfoDictionary);
    numberOfAlbums=profileCompleteInfoDictionary[@"gallery_count"];
   // numberOfFriends=profileCompleteInfoDictionary[@"friend_count"];
    profileImageURL=profileCompleteInfoDictionary[@"image"];
    NSLog(@"*********\n**********\n********\n profileImageURL : %@" , profileImageURL );
    
    
    [self setupView];
    //backgroundImageView.image = [self blurWithCoreImage:backgroundImageView.image];
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}
-(void)setupView{
    [ nameLabel performSelectorOnMainThread:@selector(setText:) withObject:profileCompleteInfoDictionary[@"username"] waitUntilDone:NO];
    [ friendsCountLabel performSelectorOnMainThread:@selector(setText:) withObject:numberOfFriends waitUntilDone:NO];
    [ albumsCountLabel performSelectorOnMainThread:@selector(setText:) withObject:numberOfAlbums waitUntilDone:NO];

    
    [frontProfileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",profileImageURL]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    [backgroundImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",profileImageURL]]];
}

- (IBAction)openPictureOnTap:(id)sender {
    PEPSDisplayImageViewController *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"displayImageScene"];
    
    vc.imageURLToDisplay = profileImageURL;
    vc.type=1;
    
    [self presentViewController:vc animated:YES completion:nil];

   }





// get albums

-(void)connectionGotData:(NSData *)data{

    
    refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@""];
    
    [refreshControl endRefreshing];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    returnedDataArray = returnedDict[@"Data"];
    
    NSLog(@"returnedDataArray for albums details : %@" , returnedDataArray);

    //[self showPullToRefreshMessage:YES];
   // [refreshControl endRefreshing];
    [albumsCollectionView reloadSections:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(0,1)]];
     //[self showPullToRefreshMessage:YES];
  /*  NSIndexPath *lastIndexPath = [NSIndexPath indexPathForItem:0 inSection:0];
    [albumsCollectionView scrollToItemAtIndexPath:nil atScrollPosition:UICollectionViewScrollPositionTop animated:YES];
   */
    
    
}



-(void)isFriendConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    isFriendCode =returnedDict[@"code"];
    isFriendMessage=returnedDict[@"message"];
    

      NSLog(@"returnedDict for isFriendConnectionGotData : %@" , returnedDict);
     NSLog(@"isFriendMessage for isFriendConnectionGotData : %@" , isFriendMessage);



    if([isFriendCode isEqualToString:@"200"])
    {
        if ([isFriendMessage containsString:@"No Record Found"]) {
            UIAlertView *addAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Add Friend?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            addAlert.tag = 2001;
            [addAlert show];
        }
        else{
        friendshipId =returnedDict[@"friend_id"];
        UIAlertView *endAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"End Friendship?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
        endAlert.tag = 2002;
        [endAlert show];
        }
        
    }
    else if([isFriendCode isEqualToString:@"300"])
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Pending Friend Request" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
}
-(void)deleteFriendConnectionGotData:(NSData *)data{

    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"returnedDict for deleteFriendConnectionGotData : %@" , returnedDict);
    NSString *code =returnedDict[@"code"];
    if ([code isEqualToString:@"200"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Friend Deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        [self.navigationController popToRootViewControllerAnimated:YES];
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Friend could not be deleted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
    
}-(void)sendFriendRequestConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
   // userId =returnedDict[@"id"];
    NSLog(@"returned Dict sendFriendRequestConnectionGotData : %@" , returnedDict);

        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:returnedDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];


}
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}



#pragma mark - Search Display Table View
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (!searchTableView) {
        searchTableView = tableView;
    }
    return searchAlbumsArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 66)];
  //  UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width, 44)];
    NSLog(@"album search result : %@", [searchAlbumsArray objectAtIndex:indexPath.row][@"Name"]);
    NSLog(@"Cat Name : %@" , [searchAlbumsArray objectAtIndex:indexPath.row][@"Cat Name"]);
    
  //  AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
  //  NSString *imageUrl = [searchAlbumsArray objectAtIndex:indexPath.row][@"imageUrl"];

    
  //  imageView.image =[UIImage imageNamed:@"personPlaceholder"];
 //   imageView.showActivityIndicator = YES;
  //  imageView.imageURL =[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageUrl]] ;
    

    if ([searchAlbumsArray[indexPath.row][@"Cat Name"] isEqualToString:@"gallery"]) {
        NSString *labelTextToDisplay = [NSString stringWithFormat:@"%@ - %@ Photos",[searchAlbumsArray objectAtIndex:indexPath.row][@"Name"],[searchAlbumsArray objectAtIndex:indexPath.row][@"photosCount"]];
        cell.textLabel.text = labelTextToDisplay;
    }
    else{
        cell.textLabel.text = [searchAlbumsArray objectAtIndex:indexPath.row][@"Name"];
    }
    
    //cell.textLabel.text =[searchAlbumsArray objectAtIndex:indexPath.row][@"Name"];
    cell.detailTextLabel.text =[searchAlbumsArray objectAtIndex:indexPath.row][@"Cat Name"];
   // label.text = [searchAlbumsArray objectAtIndex:indexPath.row][@"Name"];
   // [cell addSubview:label];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {

    if ([searchAlbumsArray[indexPath.row][@"Cat Name"] isEqualToString:@"gallery"]) {    PEPSViewAlbumViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewAlbumScene"];
        NSLog(@"FROM : %@" ,searchAlbumsArray[indexPath.row][@"userName"] );
        if ([searchAlbumsArray[indexPath.row][@"userName"] isEqualToString: myUserName]) {
            
            NSLog(@"Sending type 0 to View albums");
            vc.type = 0;
        }
        else {
            NSLog(@"Sending type 1 to View albums");
            vc.type = 1;
        }
        vc.albumID = searchAlbumsArray[indexPath.row][@"galleryId"];
        vc.albumDescription = searchAlbumsArray[indexPath.row][@"albumDescription"];
        vc.albumName = searchAlbumsArray[indexPath.row][@"Name"];
        vc.coverPhotoURL = searchAlbumsArray[indexPath.row][@"imageUrl"];
        vc.userName = searchAlbumsArray[indexPath.row][@"userName"];
        vc.likesCount = searchAlbumsArray[indexPath.row][@"likesCount"];
        vc.photosCount = searchAlbumsArray[indexPath.row][@"photosCount"];
        vc.invitesCount = searchAlbumsArray[indexPath.row][@"invitesCount"];
        
        NSLog(@"userId from profile to albums view: %@ " , userId );
        vc.userId = userId;
        [self.navigationController pushViewController:vc animated:YES];
    }
    else{
        PEPSProfileViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileScene"];
        vc.type=1;
        vc.otherUserUsername=[searchAlbumsArray objectAtIndex:indexPath.row][@"Name"];
        vc.otherUserUserId=[searchAlbumsArray objectAtIndex:indexPath.row][@"UserId"];
        [self.navigationController pushViewController:vc animated:YES];
    }

    
    
}

/*
- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar
{
  [self.searchDisplayController.searchResultsTableView reloadData];

}
 */




-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{
 //   [self.searchDisplayController.searchResultsTableView reloadData];
    
    NSLog(@"changed TEXT : %@" , searchText);
    [[webServices sharedServices] getSearchWithString:searchText handler:^(NSData *data){
        [self searchConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
    // [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Do the search and show the results in tableview
    // Deactivate the UISearchBar
    
    // You'll probably want to do this on another thread
    // SomeService is just a dummy class representing some
    // api that you are using to do the search
    
    //NSLog(@"searchBar.text : %@", searchBar.text);
    
    [[webServices sharedServices] getSearchWithString:searchBar.text handler:^(NSData *data){
        [self searchConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    
    //  NSArray *results = [SomeService doSearch:searchBar.text];
    
    //[self searchBar:searchBar activate:NO];
    
    
    
    
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {
    
    NSLog(@"searchSTRING : %@" , searchString);
    return YES;
}

-(void)searchConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
   NSLog(@"returnedDictionary for search results: %@" , returnedDictionary);
    
    searchAlbumsArray = returnedDictionary[@"Data"];
    //NSLog(@"usersArray : %@" , usersArray);
    //  self.searchResult = returnedDictionary[@"Users"];
    

    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    //[self.searchDisplayController.searchResultsTableView reloadData];
    if (searchTableView) {
        [searchTableView reloadData];
    }
}

#pragma mark- rotate

- (BOOL)shouldAutorotate{
    return  NO;
}
@end
