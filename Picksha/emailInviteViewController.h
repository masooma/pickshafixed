//
//  emailInviteViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 22/06/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface emailInviteViewController : UIViewController<UITableViewDataSource , UITableViewDelegate , UITextFieldDelegate>
@property (nonatomic ,strong) NSString *albumID;

@end
