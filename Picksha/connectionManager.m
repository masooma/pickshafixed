//
//  ConnectionManager.m
//  bringThings
//
//  Created by iOS Department-Pantera on 25/02/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "connectionManager.h"
typedef void (^CompletionBlock)(NSData*);
typedef void (^errorBlock)(NSError*);
@implementation connectionManager{
    
    CompletionBlock block;
    errorBlock theErrorBlock;
    
    NSMutableData *responseData;
}
-(void)startConnectionWithRequest:(NSURLRequest*)request handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSURLConnection *con = [NSURLConnection connectionWithRequest:request delegate:self];
    if (con) {
        block = handler;
        theErrorBlock = errorHandler;
    }
    [con start];
}

#pragma mark - URL Connection Data Delegate
-(void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response{
    responseData = [[NSMutableData alloc] init];
   
    /*
     NSHTTPURLResponse *HTTPResponse = (NSHTTPURLResponse *)response;
    NSDictionary *fields = [HTTPResponse allHeaderFields];
    //NSString *cookie = [fields valueForKey:@"Set-Cookie"];
    NSLog(@"cookie in response: %@",fields[@"Set-Cookie"]);
     */
}
-(void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data{
    [responseData appendData:data];
    
}
-(void)connectionDidFinishLoading:(NSURLConnection *)connection{
    block(responseData);
}
-(void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error{
    theErrorBlock(error);
}


@end
