//
//  PEPSLanguagesTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 20/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSLanguagesTableViewController.h"

@interface PEPSLanguagesTableViewController ()

@end

@implementation PEPSLanguagesTableViewController{

NSIndexPath *selectedIndex;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Language" ;
    //NSLog(@"Language !");
    [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1] animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 2;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier" forIndexPath:indexPath];
      UILabel *theLabel = (UILabel *)[cell viewWithTag:1];
    
              [self.tableView selectRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:0] animated:YES scrollPosition:UITableViewScrollPositionNone];
    
    switch (indexPath.row) {
        case 0:
            theLabel.text = @"English";
            break;
        case 1:
            theLabel.text = @"German";
            break;
      /*  case 2:
            theLabel.text = @"Language 3";
            break;
        case 3:
            theLabel.text = @"Language 4";
            break;
        case 4:
            theLabel.text = @"Language 5";
            break;
       */
            
        default:
            break;
    }
    
    /*
    cell.accessoryType = [indexPath isEqual:selectedIndex] ?UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
*/
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSIndexPath * prevIndexPath = selectedIndex;
    
    selectedIndex = indexPath;
    
   // NSLog(@"selected indexpath : %@" , selectedIndex);
    /*
    [tableView reloadRowsAtIndexPaths:[NSArray arrayWithObjects:prevIndexPath, indexPath, nil] withRowAnimation:UITableViewRowAnimationAutomatic];
     */
}
@end
