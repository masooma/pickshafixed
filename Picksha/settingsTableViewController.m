//
//  settingsTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 13/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "settingsTableViewController.h"

@interface settingsTableViewController (){
NSMutableArray      *sectionTitleArray;
NSMutableDictionary *sectionContentDict;
NSMutableArray      *arrayForBool;
}
@end

@implementation settingsTableViewController{
    UITextField *name;
    UITextField *email;

}

- (void)viewDidLoad
{
    [super viewDidLoad];
  
    if (!sectionTitleArray) {
        sectionTitleArray = [NSMutableArray arrayWithObjects:@"Aachen", @"Berlin", @"Düren", @"Essen", @"Münster", nil];
    }
    if (!arrayForBool) {
        arrayForBool    = [NSMutableArray arrayWithObjects:[NSNumber numberWithBool:NO],
                           [NSNumber numberWithBool:NO],
                           [NSNumber numberWithBool:NO],
                           [NSNumber numberWithBool:NO],
                           [NSNumber numberWithBool:NO] , nil];
    }
    if (!sectionContentDict) {
        sectionContentDict  = [[NSMutableDictionary alloc] init];
        
        
        
        NSArray *array1     = [NSArray arrayWithObjects:@"Change Name", @"Change Email", @"Change Password", @"Language", nil];
        [sectionContentDict setValue:array1 forKey:[sectionTitleArray objectAtIndex:0]];
        NSArray *array2     = [NSArray arrayWithObjects:@"Login Alerts", @"Login Approvals", @"Where you're logging", nil];
        [sectionContentDict setValue:array2 forKey:[sectionTitleArray objectAtIndex:1]];
        NSArray *array3     = [NSArray arrayWithObjects:@"Block Users", @"Block App Invites", @"Block Event Invites", nil];
        [sectionContentDict setValue:array3 forKey:[sectionTitleArray objectAtIndex:2]];
        /*
        NSArray *array4     = [NSArray arrayWithObjects:@"hoden", @"pute", @"eimer", @"wichtel", @"karl", @"dreirad", nil];
        [sectionContentDict setValue:array4 forKey:[sectionTitleArray objectAtIndex:3]];
        NSArray *array5     = [NSArray arrayWithObjects:@"Ei", @"kanone", nil];
        [sectionContentDict setValue:array5 forKey:[sectionTitleArray objectAtIndex:4]];
         */
    }
}





#pragma mark - Table View

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return [sectionTitleArray count];
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ([[arrayForBool objectAtIndex:section] boolValue]) {
        return [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:section]] count];
    }
    return 1;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section{
    UIView *headerView              = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 50)];
    headerView.tag                  = section;
    headerView.backgroundColor      = [UIColor whiteColor];
    UILabel *headerString           = [[UILabel alloc] initWithFrame:CGRectMake(10, 0, self.view.frame.size.width-20-50, 50)];
    BOOL manyCells                  = [[arrayForBool objectAtIndex:section] boolValue];
    if (!manyCells) {
        switch (section) {
            case 0:
                headerString.text = @"General Account Settings";
                break;
            case 1:
                headerString.text = @"Security Settings";
                break;
            case 2:
                headerString.text = @"Manage Blocking";
                break;
            default:
                break;
        }
    }
    else{
        switch (section) {
            case 0:
                headerString.text = @"General Account Settings";
                break;
            case 1:
                headerString.text = @"Security Settings";
                break;
            case 2:
                headerString.text = @"Manage Blocking";
                break;
            default:
                break;
        }
        
    }
    headerString.textAlignment      = NSTextAlignmentLeft;
    headerString.textColor          = [UIColor blackColor];
    headerString.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    [headerView addSubview:headerString];
    
    UITapGestureRecognizer  *headerTapped   = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(sectionHeaderTapped:)];
    [headerView addGestureRecognizer:headerTapped];
    
    //up or down arrow depending on the bool
    UIImageView *upDownArrow        = [[UIImageView alloc] initWithImage:manyCells ? [UIImage imageNamed:@"up"] : [UIImage imageNamed:@"down"]];
    upDownArrow.autoresizingMask    = UIViewAutoresizingFlexibleLeftMargin;
    upDownArrow.frame               = CGRectMake(self.view.frame.size.width-40, 10, 30, 30);
    [headerView addSubview:upDownArrow];
    
    return headerView;
}/*
- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section{
    UIView *footer  = [[UIView alloc] initWithFrame:CGRectZero];
    return footer;
}
*/
- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 44;
}
- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section{
    return 1;
}
- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if ([[arrayForBool objectAtIndex:indexPath.section] boolValue]) {
        return 50;
    }
    return 0.0f;
}
// Customize the appearance of table view cells.
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    NSString *CellIdentifier = [NSString stringWithFormat:@"RCell%1ldR%1ld",(long)indexPath.section,(long)indexPath.row];

    //static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        if ([[UIDevice currentDevice] userInterfaceIdiom] == UIUserInterfaceIdiomPhone) {
            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        }
    }
    BOOL manyCells  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
    if (!manyCells) {
        cell.textLabel.text = @"click to enlarge";
    }
    else{
    
        if (indexPath.section == 0) {
            switch (indexPath.row) {
                case 0:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Change Name";
                    //name.text = @"Elena Gilbert";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 1:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Change Email";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 2:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Change Password";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 3:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Language";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                    
                default:
                    break;
            }

        }
        
        else if (indexPath.section == 1) {
            switch (indexPath.row) {
                case 0:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Login Alerts";
                    //name.text = @"Elena Gilbert";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 1:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Logging Approvals";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 2:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Where you're Logging";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                    
                default:
                    break;
            }
            
        }
        
        else if (indexPath.section == 2) {
            switch (indexPath.row) {
                case 0:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Login Alerts";
                    //name.text = @"Elena Gilbert";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 1:
                    name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
                    name.placeholder = @"Logging Approvals";
                    name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                    name.autocorrectionType = UITextAutocorrectionTypeNo;
                    [name setClearButtonMode:UITextFieldViewModeWhileEditing];
                    //cell.accessoryView = self.firstName;
                    [cell addSubview:name];
                    
                    break;
                case 2:
                    cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
                   // cell.textLabel.text = [content objectAtIndex:indexPath.row];
                    cell.textLabel.text = @"Block Event Invites";
                    
                    break;
                    
                default:
                    break;
            }
            
        }

        
             /*   if (indexPath.row == 0){
            //self.firstName = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, 280, 21)];
            name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
            name.placeholder = @"Elena Gilbert";
            name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
            name.autocorrectionType = UITextAutocorrectionTypeNo;
            [name setClearButtonMode:UITextFieldViewModeWhileEditing];
            //cell.accessoryView = self.firstName;
            [cell addSubview:name];
        }
        if (indexPath.row == 0){
            //self.firstName = [[UITextField alloc] initWithFrame:CGRectMake(5, 0, 280, 21)];
            name = [[UITextField alloc] initWithFrame:CGRectMake(13, 13, 375, 30)];
            name.placeholder = @"Elena Gilbert";
            name.font =[UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
            name.autocorrectionType = UITextAutocorrectionTypeNo;
            [name setClearButtonMode:UITextFieldViewModeWhileEditing];
            //cell.accessoryView = self.firstName;
            [cell addSubview:name];
        }
        */
        
        /*
        NSArray *content = [sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:indexPath.section]];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:20.0];
        cell.textLabel.text = [content objectAtIndex:indexPath.row];
         
         */
    }
    
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
   // DetailViewController *dvc;
    DetailViewController *dvc = [self.storyboard instantiateViewControllerWithIdentifier:@"detailViewController"];
    

   /* if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPhone) {
        dvc = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPhone"  bundle:[NSBundle mainBundle]];
    }else{
        dvc = [[DetailViewController alloc] initWithNibName:@"DetailViewController_iPad"  bundle:[NSBundle mainBundle]];
    }
    dvc.title        = [sectionTitleArray objectAtIndex:indexPath.section];
    dvc.detailItem   = [[sectionContentDict valueForKey:[sectionTitleArray objectAtIndex:indexPath.section]] objectAtIndex:indexPath.row];
    [self.navigationController pushViewController:dvc animated:YES];
    */
}


#pragma mark - gesture tapped
- (void)sectionHeaderTapped:(UITapGestureRecognizer *)gestureRecognizer{
    NSIndexPath *indexPath = [NSIndexPath indexPathForRow:0 inSection:gestureRecognizer.view.tag];
    if (indexPath.row == 0) {
        BOOL collapsed  = [[arrayForBool objectAtIndex:indexPath.section] boolValue];
        collapsed       = !collapsed;
        [arrayForBool replaceObjectAtIndex:indexPath.section withObject:[NSNumber numberWithBool:collapsed]];
        
        //reload specific section animated
        NSRange range   = NSMakeRange(indexPath.section, 1);
        NSIndexSet *sectionToReload = [NSIndexSet indexSetWithIndexesInRange:range];
        [self.tableView reloadSections:sectionToReload withRowAnimation:UITableViewRowAnimationFade];
    }
}

@end
