//
//  PEPSAlertsTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 20/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSAlertsTableViewController.h"

@interface PEPSAlertsTableViewController ()

@end

@implementation PEPSAlertsTableViewController{
    NSDictionary *returnedDict;
    IBOutlet UISwitch *friendRequestSwitch;

    IBOutlet UISwitch *confirmFriendrequestSwitch;
    IBOutlet UISwitch *confirmInvitationSwitch;
    IBOutlet UISwitch *getInvitationSwitch;
    IBOutlet UISwitch *newCommentsSwitch;
    IBOutlet UISwitch *newCommentsWhereICommentedSwitch;
    IBOutlet UISwitch *getMessageSwitch;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Alerts" ;

    [[webServices sharedServices] getAlertsSettingsWithHandler:^(NSData *data){
        [self gotAlertsConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)setSwitches{
    


    [friendRequestSwitch setOn:[returnedDict[@"i_got_a_new_friend_request"] integerValue] animated:YES];
    [confirmFriendrequestSwitch setOn:[returnedDict[@"a_friend_confirms_my_friend_request"] integerValue] animated:YES];
    [confirmInvitationSwitch setOn:[returnedDict[@"a_friends_confirms_my_invition_to_a_gallery"] integerValue] animated:YES];
    [getInvitationSwitch setOn:[returnedDict[@"i_get_a_invition_to_a_gallery"] integerValue] animated:YES];
    [newCommentsSwitch setOn:[returnedDict[@"about_new_comments_in_my_gallerys"] integerValue] animated:YES];
    [newCommentsWhereICommentedSwitch setOn:[returnedDict[@"about_new_comments_where_i_commented"] integerValue] animated:YES];
    [getMessageSwitch setOn:[returnedDict[@"i_got_a_new_personal_message"] integerValue] animated:YES];


}
- (IBAction)saveButtonOnTap:(id)sender {
    
    [[webServices sharedServices]postAlertsSettingsIfGetNewFriendRequest:[NSString stringWithFormat:@"%d",friendRequestSwitch.isOn] confirmRequest:[NSString stringWithFormat:@"%d",confirmFriendrequestSwitch.isOn] confirmInvitation:[NSString stringWithFormat:@"%d",confirmInvitationSwitch.isOn] getInvitation:[NSString stringWithFormat:@"%d",getInvitationSwitch.isOn] commentInMyGallery:[NSString stringWithFormat:@"%d",newCommentsSwitch.isOn]  commentWhereICommented:[NSString stringWithFormat:@"%d",newCommentsWhereICommentedSwitch.isOn] gotMessage:[NSString stringWithFormat:@"%d",getMessageSwitch.isOn] handler:^(NSData *data){
        [self postConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)postConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *postReturnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //NSLog(@"returnedDict for postConnectionGotData: %@" , postReturnedDict);
    NSString *code = postReturnedDict[@"code"];

            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:postReturnedDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
}

    

-(void)gotAlertsConnectionGotData:(NSData *)data{
   returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
  //  NSInteger code = [returnedDict[@"code"] integerValue];
    
   // NSLog(@"returnedDict for gotAlertsConnectionGotData: %@" , returnedDict);
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [self performSelectorOnMainThread:@selector(setSwitches) withObject:nil waitUntilDone:NO];
}
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}



@end
