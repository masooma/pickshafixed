//
//  settingsTableViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 13/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
@class DetailViewController;

@interface settingsTableViewController : UITableViewController

@property (strong, nonatomic) DetailViewController *detailViewController;

@end
