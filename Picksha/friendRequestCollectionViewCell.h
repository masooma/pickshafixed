//
//  friendRequestCollectionViewCell.h
//  Picksha
//
//  Created by iOS Department-Pantera on 07/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
@protocol friendRequestCellDelegate <NSObject>
@required
- (void)friendRequestAccepted;
- (void)friendRequestCancelled;
@end
@interface friendRequestCollectionViewCell : UICollectionViewCell{
    id<friendRequestCellDelegate> delegate;
}
@property (nonatomic, strong) id<friendRequestCellDelegate> delegate;

@property (nonatomic,strong) NSDictionary *dataDictionary;

- (IBAction)tickButtonOnTap:(id)sender;
- (IBAction)crossButtonOnTap:(id)sender;

@property (strong, nonatomic) IBOutlet UIButton *tickButton;
@property (strong, nonatomic) IBOutlet UIButton *crossButton;

-(void)setupCell;
-(void)showControls;
-(void)hideControls;
@end
