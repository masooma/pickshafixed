
//  main.m
//  Picksha
//
//  Created by iOS Department-Pantera on 09/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
        
    }
    
}
