//
//  commentTableViewCell.m
//  Picksha
//
//  Created by iOS Department-Pantera on 19/06/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "commentTableViewCell.h"
#import "UIImageView+WebCache.h"

@implementation commentTableViewCell{

    IBOutlet UILabel *nameLabel;
    IBOutlet UILabel *timeLabel;
    IBOutlet UILabel *commentCountLabel;
    IBOutlet UILabel *commentTextLabel;
    IBOutlet UIImageView *thumbImageView;
    IBOutlet UIButton *likeButton;
    BOOL  isLike;
    NSString  *likeId;
    BOOL toggle;
   
}

-(void)setupCell{
    

    NSLog(@"DATA DICTIONARY : %@ ", _dataDictionaryforCell);
    
    NSString *imageUrl = _dataDictionaryforCell[@"image"];
    // NSLog(@"");
    [thumbImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    
    thumbImageView.layer.cornerRadius = thumbImageView.frame.size.height/2;
   thumbImageView.layer.borderColor = [UIColor blackColor].CGColor;
    thumbImageView.layer.borderWidth = 1.5;
    thumbImageView.clipsToBounds = YES;
    //[thumbImageView removeFromSuperview];
    
    nameLabel.text=_dataDictionaryforCell[@"username"];

    commentTextLabel.text = _dataDictionaryforCell[@"text"];
 
    commentCountLabel.text =_dataDictionaryforCell[@"like_count"];
    isLike =[_dataDictionaryforCell[@"like"] boolValue];
    likeId = _dataDictionaryforCell[@"like_id"];

    NSString *time2Show = [self timeToShowForDate:[self convert:_dataDictionaryforCell[@"comment_time"]]];
    timeLabel.text = time2Show;
    
    if (isLike) {
        NSLog(@"isLike yes : %hhd" , isLike);
        [likeButton setSelected:YES];
        toggle = YES;
    }
    else{
        NSLog(@"isLike no: %hhd" , isLike);
        [likeButton setSelected:NO];
        toggle = NO;
    }
        
}


- (NSDate *)convert:(NSString *)strdate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:strdate];
    
    return date;
    
}
- (IBAction)likeButtonOnTap:(id)sender {
    
    NSLog(@"likeButton on tap!");
    
    NSLog(@"isLike : %hhd  and toggle : %hhd" , isLike , toggle);
    
    NSString *myId =[[webServices sharedServices] myUser][@"id"];
    NSString *objectId = _dataDictionaryforCell[@"id"];
   
    if (!toggle) {
        
        NSLog( @" !toggle , going to like !");
        
        [[webServices sharedServices] likeObjectType:@"comment" userId: myId objectId:objectId handler:^(NSData *data){
            [self likeConnectionGotData:data];
       }errorHandler:^(NSError *error){
           // [self connectionFailedWithError:error];
        }];
        //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
     else if (toggle) {
         NSLog( @" !toggle , going to UNlike !");

            [[webServices sharedServices] unLikeObjectType:@"comment" userId:myId objectId:objectId likeId:likeId handler:^(NSData *data){
                [self unlikeConnectionGotData:data];
            }errorHandler:^(NSError *error){
              //  [self connectionFailedWithError:error];
            }];
            //[MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
    
    
    
}

- (NSString *)timeToShowForDate:(NSDate *)date{
    NSDate *currentDate = [NSDate date];
    NSString *dateFormat;
    
    //NSTimeInterval timePassed = [currentDate timeIntervalSinceDate:date];
    //NSInteger hoursPassed = timePassed/3600;
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
    NSDateComponents *components = [gregorianCalendar components:NSCalendarUnitDay
                                                        fromDate:date
                                                          toDate:currentDate
                                                         options:0];
    NSInteger daysPassed = [components day];
    if (daysPassed<1) {
        dateFormat = @"HH:mm a";
    }else if(daysPassed<2){
        return @"Yesterday";
    }else if (daysPassed<5){
        dateFormat = @"EEE";
    }else{
        dateFormat = @"MMM dd";
    }
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:dateFormat];
    NSString * depResult = [df stringFromDate:date];
    
    return depResult;
}

-(void)likeConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
       NSLog(@"returned LIKE : %@" , returnedDict);



    
   // if ([returnedDict[@"code"] isEqualToString:@"200"]) {
    if([returnedDict[@"code"]  integerValue]== 200) {
         toggle = !toggle;
        likeId =returnedDict[@"likeId"];
        [commentCountLabel performSelectorOnMainThread:@selector(setText:) withObject:returnedDict[@"likeCount"] waitUntilDone:NO];
        NSLog(@"LIKE 200");
        [likeButton setSelected:YES];
        
    }
    
    
}
-(void)unlikeConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"returned UNIKE : %@" , returnedDict);
    

    
 //   if ([returnedDict[@"code"]  isEqualToString:@"200"]) {
    if([returnedDict[@"code"]  integerValue]== 200) {

        toggle = !toggle;
          NSLog(@"unLIKE 200");
        NSString *likesCounts =  [NSString stringWithFormat:@"%@",returnedDict[@"likeCount"]] ;
        //[commentCountLabel performSelectorOnMainThread:@selector(setText:) withObject:likesCounts waitUntilDone:NO];
        
        [commentCountLabel setText:likesCounts];
        
        NSLog(@"unLIKE 200     22");
        
       [likeButton setSelected:NO];
        
    }
    
}

@end
