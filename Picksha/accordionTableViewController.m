//
//  accordionTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 21/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "accordionTableViewController.h"
#import "PEPSLoginSignupViewController.h"

@interface accordionTableViewController ()

@end

@implementation accordionTableViewController{
    NSMutableArray *headersArray;
    NSInteger openSection;
    NSArray *profileTitles;
    NSArray *aboutTitles;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
  //  [self.tabBarItem setFinishedSelectedImage:[UIImage imageNamed:@"Settings-gray_picsha.png"]withFinishedUnselectedImage:[UIImage imageNamed:@"Settings-white_picsha.png"]];
    self.tabBarItem.selectedImage = [[UIImage imageNamed:@"Settings-gray_picsha.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    self.tabBarItem.image = [[UIImage imageNamed:@"Settings-white_picsha.png"] imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal ];
    
    // Initialise our two images
  //  UIImage *btnImage = [UIImage imageNamed:@"settings.png"];
   // UIImage *btnImageSelected = [UIImage imageNamed:@"settingsSelected.png"];
    
  //  self.settingsButton = [UIButton buttonWithType:UIButtonTypeCustom]; //Setup the button
    //settingsButton.frame = CGRectMake(10, 426, 100, 54); // Set the frame (size and position) of the button)
  //  [self.tabBarItem setBackgroundImage:[UIImage imageNamed:@"Settings-white_picsha.png"] forState:UIControlStateNormal]; // Set the image for the normal state of the button
   // [settingsButton setBackgroundImage:[UIImage imageNamed:@"Settings-gray_picsha.png"] forState:UIControlStateSelected];
    
    [self.tabBarItem setImage:[UIImage imageNamed:@"Settings-white_picsha.png"]];
    [self.tabBarItem setSelectedImage:[UIImage imageNamed:@"Settings-gray_picsha.png"]];
    
    //tabBarItem1.title = @"xxxx";
    
    
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    self.navigationItem.title = @"SETTINGS" ;

    NSArray *sectionTitles = @[@"Profile" , @"Alerts" , @"Password" , @"About Us" ];
    profileTitles = @[@"Basic Info" , @"Languages" , @"Deactivate account" , @"Delete Account" ];
    aboutTitles = @[@"Imprint/Contact" , @"Help/FAQ" , @"Team" ];
    
    headersArray = [[NSMutableArray alloc] init];
    for(int i=0; i<[sectionTitles count]; i++){
        UINib *nib = [UINib nibWithNibName:@"SectionHeaderView" bundle:nil];
        PEMenuSectionHeaderView *header  = [[nib instantiateWithOwner:self options:nil] objectAtIndex:0];
        header.titleLabel.text = sectionTitles[i];
        header.isOpen = NO;
        header.section = i;
        header.delegate = self;
        [headersArray addObject:header];
    }
    openSection = NSNotFound;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return headersArray.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    if (((PEMenuSectionHeaderView *)headersArray[section]).isOpen) {
        
        
        switch (section) {
            case 0:
                return profileTitles.count;
                break;
            case 1:
                return 4;

                break;
            case 2:
                return 4;
                break;
            case 3:
                return aboutTitles.count;
                break;
            default:
                break;
        }
    }
    return  0;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"rowCell" forIndexPath:indexPath];
        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
 
        switch (indexPath.section) {
            case 0:
                cell.textLabel.text = profileTitles[indexPath.row];
                break;
            case 1:
                cell.textLabel.text = @"Language";
                break;
            case 2:
                cell.textLabel.text = @"Deactivate Account";
                break;
            case 3:
                cell.textLabel.text = aboutTitles[indexPath.row];
                break;

                
            default:
                break;
        }

        cell.textLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
  
    
    return cell;
}


- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    
    
    return [headersArray objectAtIndex:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    return 44;
}
-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section{
    return 48;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    
}
#pragma mark - Section Header Delegate

- (void)sectionHeaderView:(PEMenuSectionHeaderView *)sectionHeaderView sectionOpened:(NSInteger)sectionOpened {
     NSInteger countOfRowsToInsert;
    NSInteger countOfRowsToDelete ;
    switch (sectionOpened) {
        case 0:
            countOfRowsToInsert = profileTitles.count;
            break;
        case 1:
            countOfRowsToInsert = 4;
            
            break;
        case 2:
            countOfRowsToInsert = 4;
            break;
        case 3:
            countOfRowsToInsert = aboutTitles.count;
            break;
        default:
            break;
    }

    
   // NSInteger countOfRowsToInsert = 4;
    NSMutableArray *indexPathsToInsert = [[NSMutableArray alloc] init];
    for (NSInteger i = 0; i < countOfRowsToInsert; i++) {
        [indexPathsToInsert addObject:[NSIndexPath indexPathForRow:i inSection:sectionOpened]];
    }
    
    /*
     Create an array containing the index paths of the rows to delete: These correspond to the rows for each quotation in the previously-open section, if there was one.
     */
    NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
    
    NSInteger previousOpenSectionIndex = openSection;
    if (previousOpenSectionIndex != NSNotFound) {
        
        PEMenuSectionHeaderView *previousOpenSection = (headersArray)[previousOpenSectionIndex];
        
        [previousOpenSection toggleOpenWithUserAction:NO];
        
        switch (sectionOpened) {
            case 0:
                countOfRowsToDelete = profileTitles.count;
                break;
            case 1:
                countOfRowsToDelete = 4;
                
                break;
            case 2:
                countOfRowsToDelete = 4;
                break;
            case 3:
                countOfRowsToDelete = aboutTitles.count;
                break;
            default:
                break;
        }
        
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:previousOpenSectionIndex]];
        }
        
        [previousOpenSection.disclosureButton setImage:[UIImage imageNamed:@"down.png"] forState:UIControlStateNormal];
    }
    
    // style the animation so that there's a smooth flow in either direction
    UITableViewRowAnimation insertAnimation;
    UITableViewRowAnimation deleteAnimation;
    if (previousOpenSectionIndex == NSNotFound || sectionOpened < previousOpenSectionIndex) {
        insertAnimation = UITableViewRowAnimationTop;
        deleteAnimation = UITableViewRowAnimationBottom;
    }
    else {
        insertAnimation = UITableViewRowAnimationBottom;
        deleteAnimation = UITableViewRowAnimationTop;
    }
    
    // apply the updates
    [self.tableView beginUpdates];
    [self.tableView insertRowsAtIndexPaths:indexPathsToInsert withRowAnimation:insertAnimation];
    [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:deleteAnimation];
    [self.tableView endUpdates];
    
    openSection = sectionOpened;
    
}

- (void)sectionHeaderView:(PEMenuSectionHeaderView *)sectionHeaderView sectionClosed:(NSInteger)sectionClosed {
    /*
     Create an array of the index paths of the rows in the section that was closed, then delete those rows from the table view.
     */
    PEMenuSectionHeaderView *sectionInfo = (headersArray)[sectionClosed];
    
    sectionInfo.isOpen = NO;
    NSInteger countOfRowsToDelete = [self.tableView numberOfRowsInSection:sectionClosed];
    
    if (countOfRowsToDelete > 0) {
        NSMutableArray *indexPathsToDelete = [[NSMutableArray alloc] init];
        for (NSInteger i = 0; i < countOfRowsToDelete; i++) {
            [indexPathsToDelete addObject:[NSIndexPath indexPathForRow:i inSection:sectionClosed]];
        }
        [self.tableView deleteRowsAtIndexPaths:indexPathsToDelete withRowAnimation:UITableViewRowAnimationTop];
    }
    openSection = NSNotFound;
}

- (IBAction)logoutOnTap:(id)sender {
    [[webServices sharedServices] logoutUserWithHandler:^(NSData *data){
        [self logoutConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)logoutConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"logoutConnectionGotData: returnedDictionary : %@" , returnedDictionary);
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    if ([returnedDictionary[@"code"] isEqualToString:@"200"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Logout Successful!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
         PEPSLoginSignupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupScene"];
        
         [self presentViewController:vc animated:YES completion:nil];
    }
    else if ([returnedDictionary[@"code"] isEqualToString:@"300"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Could not Logout.\nPlease try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
           }
    
}


@end
