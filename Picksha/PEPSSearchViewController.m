//
//  ViewController.m
//  iOS7SearchBarTableViewTutorial
//
//  Created by Arthur Knopper on 24-02-14.
//  Copyright (c) 2014 Arthur Knopper. All rights reserved.
//

#import "PEPSSearchViewController.h"

@interface PEPSSearchViewController () 

@property (nonatomic, strong) NSArray *tableData;
@property (nonatomic, strong) NSMutableArray *searchResult;


@end

@implementation PEPSSearchViewController{
    NSArray *usersArray;

    IBOutlet UITableView *searchTableView;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    
   // self.tableData = @[@"Masooma",@"Hassan"];
    self.searchResult = [[NSMutableArray alloc] init];
   // self.searchResult =[NSMutableArray arrayWithCapacity:[self.tableData count]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return 5;//usersArray.count;
    /*if (tableView == self.searchDisplayController.searchResultsTableView)
    {
       // return [self.searchResult count];
        return usersArray.count;
    }
    else
    {
        return [self.tableData count];
    }
     */
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    
    /*static NSString *CellIdentifier = @"Cell";
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    
    if (cell == nil)
    {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
    }
    
   // if (tableView == self.searchDisplayController.searchResultsTableView)
   // {
        cell.textLabel.text = [usersArray objectAtIndex:indexPath.row][@"Name"];
        NSLog(@" cell.textLabel.text  :%@" ,  cell.textLabel.text);
   // }*/
  /* else
    {
        cell.textLabel.text = self.tableData[indexPath.row];
    }
    */
    UITableViewCell *cell = [[UITableViewCell alloc] init];
    cell.backgroundColor = [UIColor redColor];
    NSLog(@"");
    cell.detailTextLabel.text = @"hello";
    return cell;
}

- (void)filterContentForSearchText:(NSString*)searchText scope:(NSString*)scope
{
  //  [self.searchResult removeAllObjects];
   // NSPredicate *resultPredicate = [NSPredicate predicateWithFormat:@"SELF contains[c] %@", searchText];
    
  //  self.searchResult = [NSMutableArray arrayWithArray: [self.tableData filteredArrayUsingPredicate:resultPredicate]];
   
}

-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString
{
  //  [self filterContentForSearchText:searchString scope:[[self.searchDisplayController.searchBar scopeButtonTitles] objectAtIndex:[self.searchDisplayController.searchBar selectedScopeButtonIndex]]];
    
    return YES;
}

#pragma mark UISearchBarDelegate Methods

- (void)searchBar:(UISearchBar *)searchBar
    textDidChange:(NSString *)searchText {
    // We don't want to do anything until the user clicks
    // the 'Search' button.
    // If you wanted to display results as the user types
    // you would do that here.
}

- (void)searchBarTextDidBeginEditing:(UISearchBar *)searchBar {
    // searchBarTextDidBeginEditing is called whenever
    // focus is given to the UISearchBar
    // call our activate method so that we can do some
    // additional things when the UISearchBar shows.
    [self searchBar:searchBar activate:YES];
}

- (void)searchBarTextDidEndEditing:(UISearchBar *)searchBar {
    // searchBarTextDidEndEditing is fired whenever the
    // UISearchBar loses focus
    // We don't need to do anything here.
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    // Clear the search text
    // Deactivate the UISearchBar
    searchBar.text=@"";
    [self searchBar:searchBar activate:NO];
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Do the search and show the results in tableview
    // Deactivate the UISearchBar
    
    // You'll probably want to do this on another thread
    // SomeService is just a dummy class representing some
    // api that you are using to do the search
    
    NSLog(@"searchBar.text : %@", searchBar.text);
    
    [[webServices sharedServices] getSearchWithString:searchBar.text handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];

    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    
    
  //  NSArray *results = [SomeService doSearch:searchBar.text];
    
    //[self searchBar:searchBar activate:NO];
    
    [self.searchResult removeAllObjects];
    //[self.tableData addObjectsFromArray:usersArray];
    self.tableData = usersArray;
    [searchTableView reloadData];
  
}
-(void)connectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"returnedDictionary for search results: %@" , returnedDictionary);
    
    usersArray = returnedDictionary[@"Users"];
    NSLog(@"usersArray : %@" , usersArray);
  //  self.searchResult = returnedDictionary[@"Users"];
    
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [searchTableView reloadData];
    
    
    
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
// We call this when we want to activate/deactivate the UISearchBar
// Depending on active (YES/NO) we disable/enable selection and
// scrolling on the UITableView
// Show/Hide the UISearchBar Cancel button
// Fade the screen In/Out with the disableViewOverlay and
// simple Animations
- (void)searchBar:(UISearchBar *)searchBar activate:(BOOL) active{
   /*
    self.theTableView.allowsSelection = !active;
    self.theTableView.scrollEnabled = !active;
    if (!active) {
        [disableViewOverlay removeFromSuperview];
        [searchBar resignFirstResponder];
    } else {
        self.disableViewOverlay.alpha = 0;
        [self.view addSubview:self.disableViewOverlay];
        
        [UIView beginAnimations:@"FadeIn" context:nil];
        [UIView setAnimationDuration:0.5];
        self.disableViewOverlay.alpha = 0.6;
        [UIView commitAnimations];
        
        // probably not needed if you have a details view since you
        // will go there on selection
        NSIndexPath *selected = [self.theTableView
                                 indexPathForSelectedRow];
        if (selected) {
            [self.theTableView deselectRowAtIndexPath:selected
                                             animated:NO];
        }
    }
    [searchBar setShowsCancelButton:active animated:YES];
    */
}




@end
