//
//  SOConversationViewController.m
//  snapOpen
//
//  Created by Pantera Engineering on 06/03/2015.
//  Copyright (c) 2015 Pantera Engineering. All rights reserved.
//

#import "SOConversationViewController.h"
#import "SOChatStream.h"
#import "UIImageView+WebCache.h"
#import "PEPSDisplayImageViewController.h"
#import <CoreFoundation/CoreFoundation.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import <MediaPlayer/MediaPlayer.h>



#import "ASIFormDataRequest.h"

#import <MobileCoreServices/UTCoreTypes.h>
#import <AssetsLibrary/AssetsLibrary.h>
#import <AVFoundation/AVFoundation.h>
#import <AVFoundation/AVAsset.h>
#import <AVFoundation/AVPlayer.h>
#import <AVFoundation/AVPlayerItem.h>
#import <CoreMedia/CoreMedia.h>


@interface SOConversationViewController ()
@property (strong,nonatomic) IBOutlet UILabel *progressLabel;
@property(strong,nonatomic)IBOutlet  UIProgressView *progressIndicator;
@end

@implementation SOConversationViewController{
    
    MPMoviePlayerController *movieController;
    
    UIView * aProgressView ;
    UIProgressView *progressIndicator;
    MBProgressHUD *aMBProgressHUD;
    ASIFormDataRequest *request;
    
    UIImagePickerController *ipc;
    UIPopoverController *popover;
    
    NSMutableArray *messagesArray;
    NSString *userId;
    NSString *username;
    NSString *userProfileImageURL;
    
    NSString *textForEmail;
    
    
    NSString * myUserName;
    NSString * myId;
    
    NSString *otherUserUsername;
    NSString *otherUserId;
    // NSString *otherUserProfileImageURL;
    NSString *otherImageURL;
    
    UITapGestureRecognizer *tapRecognizer;
    
    NSInteger msgCount;
    UIImage *imageToBeSentToServer;
    
    NSURL* videoUrl;
    JSQMessage *videoMessage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleMessageChange:)
                                                 name:@"messageCountChanged"
                                               object:nil];
    NSLog(@"bottom");
   // [self scrollToBottomAnimated:NO];
    self.tabBarController.tabBar.hidden = YES;
    msgCount=NSIntegerMax;
    //    [NSTimer scheduledTimerWithTimeInterval:15.0f target:self selector:@selector(getConversation) userInfo:nil repeats:YES];
    
    otherUserId = _messageFromUserId;
    messagesArray = [[NSMutableArray alloc] init];
    
    self.inputToolbar.contentView.rightBarButtonItem = [JSQMessagesToolbarButtonFactory defaultSendButtonItem];
    // self.inputToolbar.contentView.leftBarButtonItem = [JSQMessagesToolbarButtonFactory defaultAccessoryButtonItem];
    self.inputToolbar.sendButtonOnRight = YES;
    self.collectionView.collectionViewLayout.messageBubbleFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
    [self setUpView];
    NSLog(@"my user is: %@",[[webServices sharedServices] myUser]);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    NSLog(@"myUserName : %@ " , myUserName);
    NSLog(@"myUserId : %@ " , myId);
    
    self.senderDisplayName = myUserName;
    self.senderId = myId;
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    [[webServices sharedServices]getMessagesForSpecificUserWithId:_messageFromUserId handler:^(NSData *data){
     [self connectionGotData:data];
     }errorHandler:^(NSError *error){
     [self connectionFailedWithError:error];
     }];
    NSLog(@"LOADING THIS VIEWWW ******00");
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}

-(void)getConversation{
    [[webServices sharedServices]getMessagesForSpecificUserWithId:_messageFromUserId handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        // [self connectionFailedWithError:error];
    }];
   // [self.collectionView reloadData];
     NSLog(@"bottom");
   // [self scrollToBottomAnimated:NO];
    
}

- (void)handleMessageChange:(NSNotification *)note {
    
    NSLog(@"Getting CONVERSATION in listener!");
    [self getConversation];
}

-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self.view endEditing:YES];
    
   // self.tabBarController.tabBar.hidden = YES;
    
   /* [[webServices sharedServices]getMessagesForSpecificUserWithId:_messageFromUserId handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    */
    
    /*
     [[webServices sharedServices] getAdminProfileImageAndUsernameWithHandler:^(NSData *data){
     [self getUserInfoConnectionGotData:data];
     }errorHandler:^(NSError *error){
     [self connectionFailedWithError:error];
     }];
     [MBProgressHUD showHUDAddedTo:self.view animated:YES];
     */
}

-(void)viewDidDisappear:(BOOL)animated{
    [super viewDidDisappear:animated];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
-(void)imageMessageConnectionGotData:(NSData *)data image:(JSQMessage *)image{
    
    
    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"response for messageConnectionGotData: %@",responseDictionary);
    
    if ([responseDictionary[@"code"] integerValue] ==200) {
        [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
        [messagesArray addObject:image];
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        
        [self finishSendingMessageAnimated:YES];
    }
}

-(void)connectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
     NSLog(@"response for specific user: %@",responseDictionary);
    messagesArray = [[NSMutableArray alloc] init];
    otherImageURL =  responseDictionary[@"Data"][0][@"profileimage"];
    
    //if ([responseDictionary[@"code"] integerValue]==100) {
    NSArray *messagesTexts= [responseDictionary[@"Data"] mutableCopy];
    otherUserUsername= messagesTexts[0][@"username"];
    NSLog(@"otherUserUsername : %@" , otherUserUsername);
    
    for (int i = messagesTexts.count-1; i>= 0; i--) {
        
        NSDictionary *msg = messagesTexts[i];
        NSString *senderId = msg[@"user_from"];
        NSString *msgText = msg[@"message"];
        JSQMessage *message;
        if ([msg[@"attachments"] integerValue]==1) {
            /*JSQVideoMediaItem *vid = [[JSQVideoMediaItem alloc] initWithFileURL:[NSURL URLWithString:msg[@"attachments_details"]] isReadyToPlay:YES];
             //message = [JSQMessage messageWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:vid];
             message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:vid];*/
            NSString *urlStr = [NSString stringWithFormat:@"http://%@",msg[@"attachments_details"]];
            NSString *ext = [urlStr pathExtension];
            
            NSLog(@"extension: %@",ext);
            urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            NSURL *imageURL = [NSURL URLWithString:urlStr];
            
            if (![ext isEqualToString:@"jpg"]) {
                NSLog(@"VIDEO MESSAGE HERE !");
                
                JSQVideoMediaItem *vid= [[JSQVideoMediaItem alloc] initWithFileURL:imageURL isReadyToPlay:YES];
       
                
               // JSQVideoMediaItem *vid = [[JSQVideoMediaItem alloc] initWithFileURL:imageURL isReadyToPlay:NO];
                //message = [JSQMessage messageWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:vid];
                message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:vid];
                [self addMessage:message];
                
                
                
            }else{
                /*dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^(void){
                 UIImage *image;
                 image = [UIImage imageWithData:[NSData dataWithContentsOfURL:imageURL]];
                 int jf=messagesTexts.count-1-i;
                 dispatch_async(dispatch_get_main_queue(), ^{
                 JSQPhotoMediaItem *photo = [[JSQPhotoMediaItem alloc] initWithImage:image];
                 JSQMessage *aMessage = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:photo];
                 [messagesArray replaceObjectAtIndex:jf withObject:aMessage];
                 [self finishReceivingMessageAnimated:YES];
                 NSLog(@"got data");
                 });
                 
                 });
                 
                 JSQPhotoMediaItem *photo = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                 message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:photo];
                 [self addMessage:message];*/
                
                /*JSQPhotoMediaItem *photo = [[JSQPhotoMediaItem alloc] initWithImageUrl:imageURL];
                 message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:photo];
                 [self addMessage:message];*/
                
                /*JSQPhotoMediaItem *photo = [[JSQPhotoMediaItem alloc] initWithImage:nil];
                 message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:photo];
                 NSLog(@"image url is %@",imageURL);
                 [self addMessage:message];
                 UIImageView *imageview = [[UIImageView alloc] init];
                 int jf=messagesTexts.count-1-i;
                 [imageview sd_setImageWithURL:imageURL completed:^(UIImage *image, NSError *error, SDImageCacheType cacheType, NSURL *imageURL){
                 JSQPhotoMediaItem *photo = [[JSQPhotoMediaItem alloc] initWithImage:image];
                 JSQMessage *aMessage = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:photo];
                 [messagesArray replaceObjectAtIndex:jf withObject:aMessage];
                 [self finishReceivingMessageAnimated:YES];
                 NSLog(@"got data");
                 }];*/
                JSQPhotoMediaItem *photo = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"placeholderPahaar"]];
                photo.imageURL=imageURL;
                JSQMessage *aMessage = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] media:photo];
                [self addMessage:aMessage];
                [self finishReceivingMessageAnimated:YES];
                
            }
        }else {
            message = [[JSQMessage alloc] initWithSenderId:senderId senderDisplayName:msg[@"username"] date:[self convert:msg[@"msg_send_time"]] text:msgText];
            [self addMessage:message];
        }
        [JSQMessage messageWithSenderId:senderId displayName:msg[@"username"] text:msgText];
        
    }
    
    //}
    [self setUpView];
    [self finishReceivingMessage];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
    
    if (msgCount<messagesTexts.count) {
        //play sound
        [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
    }
    msgCount=messagesTexts.count;
    
}
-(void)sendMessageConnectionGotData:(NSData *)data message:(JSQMessage *)theMessage{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *responseDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"response sendMesssageConnectionGotData: %@",responseDictionary);
    
    if ([responseDictionary[@"code"] isEqualToString:@"400"]) {
        UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry! Your message could not be sent.\nPlease try again later." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        
        [Alert show];
        
    }
    else {
        [self addMessage:theMessage];
        
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        msgCount++;
        
        [self finishSendingMessageAnimated:YES];
        [[webServices sharedServices] messageAlertWithToUserId:_messageFromUserId messageText:textForEmail handler:^(NSData *data){
            //[self connectionGotData:data];
        }errorHandler:^(NSError *error){
            // [self connectionFailedWithError:error];
        }];
    }
    
    //  messagesArray = [[NSMutableArray alloc] init];
    
    
    
    
    //if ([responseDictionary[@"code"] integerValue]==100) {
    // NSArray *messagesTexts= [responseDictionary[@"Data"] mutableCopy];
    
    [self finishReceivingMessage];
    [MBProgressHUD hideHUDForView:self.view animated:YES];
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
/*
 - (void)didPressAccessoryButton:(UIButton *)sender
 {
 UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
 delegate:self
 cancelButtonTitle:@"Cancel"
 destructiveButtonTitle:nil
 otherButtonTitles:@"Take Photo", @"Choose Photo", @"Send video", nil];
 
 [sheet showFromToolbar:self.inputToolbar];
 }
 
 - (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
 {
 if (buttonIndex == actionSheet.cancelButtonIndex) {
 return;
 }
 
 switch (buttonIndex) {
 case 0:
 [self.demoData addPhotoMediaMessage];
 break;
 
 case 1:
 {
 __weak UICollectionView *weakView = self.collectionView;
 
 [self.demoData addLocationMediaMessageCompletion:^{
 [weakView reloadData];
 }];
 }
 break;
 
 case 2:
 [self.demoData addVideoMediaMessage];
 break;
 }
 
 [JSQSystemSoundPlayer jsq_playMessageSentSound];
 
 [self finishSendingMessageAnimated:YES];
 }
 
 
 - (void)selectImagesByCapturing:(id)sender
 {
 ipc = [[UIImagePickerController alloc] init];
 ipc.delegate = self;
 if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
 {
 ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
 [self presentViewController:ipc animated:YES completion:NULL];
 }
 else
 {
 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
 [alert show];
 alert = nil;
 }
 }
 
 #pragma mark - ImagePickerController Delegate
 
 -(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
 {
 if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
 [picker dismissViewControllerAnimated:YES completion:nil];
 } else {
 [popover dismissPopoverAnimated:YES];
 }
 
 
 imageToBeSentToServer = [info objectForKey:UIImagePickerControllerOriginalImage];
 
 float width,height;
 width = imageToBeSentToServer.size.width;
 height = imageToBeSentToServer.size.height;
 if (width>1024.0f && height>1024.0f) {
 if (width<height) {
 height = height/width*1024.0f;
 width=1024.0f;
 }else{
 width = width/height*1024.0f;
 height = 1024.0f;
 }
 }
 CGSize newSize = CGSizeMake(width, height);
 UIGraphicsBeginImageContext(newSize);
 
 [imageToBeSentToServer drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
 imageToBeSentToServer = UIGraphicsGetImageFromCurrentImageContext();
 UIGraphicsEndImageContext();
 
 NSLog(@"imageToBeSentToSe rver : %@" , imageToBeSentToServer);
 [allTemporaryImagesArrayyToDisplay addObject:imageToBeSentToServer];
 
 NSLog(@"allTemporaryImagesArrayyToDisplay.count : %lu" , (unsigned long)allTemporaryImagesArrayyToDisplay.count);
 NSLog(@"allTemporaryImagesArrayyToDisplay : %@" , allTemporaryImagesArrayyToDisplay);
 
 [picker dismissViewControllerAnimated:YES completion:nil];
 [[webServices sharedServices]uploadImagesTemporarilywithImage:imageToBeSentToServer withImageName:@"" aboutPhoto:@"My First Photo" cover:@"0" handler:^(NSData *data){
 [self connectionGotData:data];
 }errorHandler:^(NSError *error){
 [self connectionFailedWithError:error];
 }];
 [MBProgressHUD showHUDAddedTo:selectedImagesCollectionView animated:YES];
 }
 
 -(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
 {
 [picker dismissViewControllerAnimated:YES completion:nil];
 }
 
 */

-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)addMessage:(JSQMessage *)message{
    [messagesArray addObject:message];
    
}
-(void)messageReceived:(NSDictionary *)theMessage{
    self.showTypingIndicator = !self.showTypingIndicator;
    NSLog(@"received: %@",theMessage);
    if ([theMessage[@"sender_id"] isEqualToString: otherUserId]) {
        JSQMessage *message = [[JSQMessage alloc] initWithSenderId:[theMessage[@"sender_id"] stringValue] senderDisplayName:otherUserUsername/*_theUser.firstName*/ date:[NSDate date]/*[self convert:theMessage[@"created_at"]]*/ text:theMessage[@"content"]]; //[JSQMessage messageWithSenderId:[theMessage[@"sender_id"] stringValue] displayName:_theUser.firstName text:theMessage[@"content"]];
        
        [self addMessage:message];
    }
    [JSQSystemSoundPlayer jsq_playMessageReceivedSound];
    [self finishReceivingMessageAnimated:YES];
}
-(void)setUpView{
    
    
    UIImageView *thumb = [[UIImageView alloc] initWithFrame:CGRectMake(0,0, 33, 33)];
    UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 0, 0)];
    
    
    label.textAlignment= NSTextAlignmentCenter;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
    
    //set the position of the button
    button.frame = CGRectMake(0, 0, 400, 33);
    
    //set the button's title
    // [button setTitle:@"Click Me!" forState:UIControlStateNormal];
    
    [button addTarget:self
               action:@selector(openPictureOnTap)
     forControlEvents:UIControlEventTouchUpInside];
    
    
    [thumb setContentMode:UIViewContentModeScaleAspectFill];
    thumb.clipsToBounds = YES;
    thumb.layer.cornerRadius = 16.5;
    ////// UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(40, 0, 300, 33)];
    
    [thumb sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",_messageFromUserProfilePicture]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    
    
    label.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:16.0];
    label.textColor = [UIColor whiteColor];
    NSLog(@"otherUserUsername : %@ " , _messageFromUserName);
    [label performSelectorOnMainThread:@selector(setText:) withObject:_messageFromUserName waitUntilDone:NO];
    label.text = _messageFromUserName;
    [label sizeToFit];
    
    UIView *titleView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, label.frame.size.width+40, 33)];
    [titleView addSubview:thumb];
    [titleView addSubview:label];
    
    [titleView addSubview:button];
    
    [self.navigationItem setTitleView:titleView];
    self.senderId = myId;
    self.senderDisplayName = myUserName;
    
    JSQMessagesBubbleImageFactory *bubbleFactory = [[JSQMessagesBubbleImageFactory alloc] init];
    
    self.outgoingBubbleImageData = [bubbleFactory outgoingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleLightGrayColor]];
    self.incomingBubbleImageData = [bubbleFactory incomingMessagesBubbleImageWithColor:[UIColor jsq_messageBubbleBlueColor]];
    
    self.collectionView.collectionViewLayout.incomingAvatarViewSize = CGSizeZero;
    self.collectionView.collectionViewLayout.outgoingAvatarViewSize = CGSizeZero;
}
-(void)closeChat{
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (NSDate *)convert:(NSString *)strdate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    // [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:strdate];
    NSLog( @"DATe : %@ " , date);
    return date;
    
}
-(void)openPictureOnTap{
    
    PEPSDisplayImageViewController *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"displayImageScene"];
    vc.imageURLToDisplay = otherImageURL;
    vc.type=1;
    [self presentViewController:vc animated:YES completion:nil];
}


#pragma mark - JSQMessagesViewController method overrides

- (void)didPressSendButton:(UIButton *)button
           withMessageText:(NSString *)text
                  senderId:(NSString *)senderId
         senderDisplayName:(NSString *)senderDisplayName
                      date:(NSDate *)date
{
    textForEmail = text;
    //JSQMessage *message = [[JSQMessage alloc] initWithSenderId:userId senderDisplayName:username date:date text:text];
    JSQMessage *message = [[JSQMessage alloc] initWithSenderId:myId
                                             senderDisplayName:myUserName
                                                          date:date
                                                          text:text];
    //[self addMessage:message];
    
    // [JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    
    
    
    /* JSQMessage *aMessage = [[JSQMessage alloc] initWithSenderId:senderId
     senderDisplayName:senderDisplayName
     date:date
     text:text];
     
     [messagesArray addObject:aMessage];*/
    
    
    [[webServices sharedServices] sendMessageTo:_messageFromUserId from:myId textMessage:text handler:^(NSData *data){
        [self sendMessageConnectionGotData:data message:message];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
}
#pragma mark - JSQMessages CollectionView DataSource

- (id<JSQMessageData>)collectionView:(JSQMessagesCollectionView *)collectionView messageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    return [messagesArray objectAtIndex:indexPath.item];
}

- (id<JSQMessageBubbleImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView messageBubbleImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  You may return nil here if you do not want bubbles.
     *  In this case, you should set the background color of your collection view cell's textView.
     *
     *  Otherwise, return your previously created bubble image data objects.
     *
     
     JSQMessage *message = [self.demoData.messages objectAtIndex:indexPath.item];
     
     if ([message.senderId isEqualToString:self.senderId]) {
     return self.demoData.outgoingBubbleImageData;
     }
     
     return self.demoData.incomingBubbleImageData; */
    if ([((JSQMessage *)messagesArray[indexPath.row]).senderId isEqualToString:_messageFromUserId]) {
        return self.incomingBubbleImageData;
    }
    return self.outgoingBubbleImageData;
}

- (id<JSQMessageAvatarImageDataSource>)collectionView:(JSQMessagesCollectionView *)collectionView avatarImageDataForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    return nil;
    
}
- (void)collectionView:(JSQMessagesCollectionView *)collectionView  didSelectItemAtIndexPath:(NSIndexPath *)indexPath{
    NSLog(@"ITEM SELECTED AT INDEX : %ld" , (long)indexPath.row);
    
    
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    
    /**
     *  This logic should be consistent with what you return from `heightForCellTopLabelAtIndexPath:`
     *  The other label text delegate methods should follow a similar pattern.
     *
     *  Show a timestamp for every 3rd message
     */
    JSQMessage *message = [messagesArray objectAtIndex:indexPath.item];
    
    return [[JSQMessagesTimestampFormatter sharedFormatter] attributedTimestampForDate:message.date];
    
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    JSQMessage *message = [messagesArray objectAtIndex:indexPath.item];
    
    /**
     *  iOS7-style sender name labels
     */
    /* if ([message.senderId isEqualToString:self.senderId]) {
     return nil;
     }*/
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [messagesArray objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:message.senderId]) {
            return nil;
        }
    }
    
    /**
     *  Don't specify attributes to use the defaults.
     */
    return [[NSAttributedString alloc] initWithString:message.senderDisplayName];
}

- (NSAttributedString *)collectionView:(JSQMessagesCollectionView *)collectionView attributedTextForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    NSAttributedString *attrString =
    [[NSAttributedString alloc] initWithString:@"strigil"
                                    attributes:nil];
    return attrString;
    //  return nil;
}

- (void)didPressAccessoryButton:(UIButton *)sender
{
    UIActionSheet *sheet = [[UIActionSheet alloc] initWithTitle:@"Media messages"
                                                       delegate:self
                                              cancelButtonTitle:@"Cancel"
                                         destructiveButtonTitle:nil
                                              otherButtonTitles:@"Capture Photo", @"Choose Photo",@"Choose Video", nil];
    
    [sheet showFromToolbar:self.inputToolbar];
}

- (void)actionSheet:(UIActionSheet *)actionSheet didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    if (buttonIndex == actionSheet.cancelButtonIndex) {
        return;
    }
    
    switch (buttonIndex) {
        case 0:
            [self selectImagesByCapturing];
            //[self addPhotoMediaMessage];
            break;
            
        case 1:
        {
            [self selectImagesByUploading];
            break;
        }
        case 2:
            [self selectVideoFromLibrary];
            // [self addVideoMediaMessage];
            break;
    }
    
}

- (void)addPhotoMediaMessage:(UIImage *)image
{
    //  JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:[UIImage imageNamed:@"aare.jpg"]];
    JSQPhotoMediaItem *photoItem = [[JSQPhotoMediaItem alloc] initWithImage:image];
    
    JSQMessage *photoMessage = [JSQMessage messageWithSenderId:myId
                                                   displayName:myUserName
                                                         media:photoItem];
    NSLog(@"sending image !");
    
    [[webServices sharedServices] sendMessagewithImage:image toUserId:_messageFromUserId handler:^(NSData *data){
        [self imageMessageConnectionGotData:data image:photoMessage];
    }errorHandler:^(NSError *error){
        // [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    
    // [messagesArray addObject:photoMessage];
    //[JSQSystemSoundPlayer jsq_playMessageSentSound];
    
    //[self finishSendingMessageAnimated:YES];
}


- (void)addVideoMediaMessage:(NSURL *)url urlString:(NSString*)urlString
{
    NSLog(@"URLLLLLL : %@" , url);
    NSLog(@"URLSTRING : %@" , urlString);
    JSQVideoMediaItem *videoItem = [[JSQVideoMediaItem alloc] initWithFileURL:url isReadyToPlay:YES];
    videoMessage = [JSQMessage messageWithSenderId:myId
                                                   displayName:myUserName
                                                         media:videoItem];
    
    request = [ASIFormDataRequest requestWithURL:url];
    [request setPostValue:@"video" forKey:@"write_msg"];
    [request setFile:urlString forKey:@"msg_file"];
    [request setRequestMethod:@"POST"];
    [request setDelegate:self];
    [request setDidStartSelector:@selector(requestStarted:)];
    
    /*
    [request setDidFinishSelector:@selector(requestFinished:):^{
                         [animatingView setAlpha:0];
                         [animatingView setCenter:CGPointMake(animatingView.center.x+50.0,
                                                              animatingView.center.y+50.0)];
                     }
                     completion:^(BOOL finished) {
                         [animatingView removeFromSuperview];
                     }];
    
    */
    [request setDidFinishSelector:@selector(requestFinished:)];
    [request setDidFailSelector:@selector(requestFailed:)];
    [request setUploadProgressDelegate:self];
    [request setTimeOutSeconds:50000];
    [request startAsynchronous];
    NSLog(@"responseStatusCode %i",[request responseStatusCode]);
    NSLog(@"responseStatusCode %@",[request responseString]);
    progressIndicator.hidden=NO;
    
    // don't have a real video, just pretending
 //   NSURL *videoURL = [NSURL URLWithString:@"file://"];
    

    
   // [messagesArray addObject:videoMessage];
    
}

#pragma mark - JSQMessages collection view flow layout delegate

#pragma mark - Adjusting cell label heights

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Each label in a cell has a `height` delegate method that corresponds to its text dataSource method
     */
    
    /**
     *  This logic should be consistent with what you return from `attributedTextForCellTopLabelAtIndexPath:`
     *  The other label height delegate methods should follow similarly
     *
     *  Show a timestamp for every 3rd message
     */
    JSQMessage *message = [messagesArray objectAtIndex:indexPath.item];
    if (indexPath.item>0) {
        
        JSQMessage *previousMessage = [messagesArray objectAtIndex:indexPath.item-1];
        NSDateComponents *components = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:message.date];
        NSDateComponents *previousComponents = [[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:previousMessage.date];
        
        if ([components year]==[previousComponents year] && [components month]==[previousComponents month] && [components day]==[previousComponents day]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
    
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForMessageBubbleTopLabelAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  iOS7-style sender name labels
     */
    JSQMessage *currentMessage = [messagesArray objectAtIndex:indexPath.item];
    if ([[currentMessage senderId] isEqualToString:self.senderId]) {
        return 0.0f;
    }
    
    if (indexPath.item - 1 > 0) {
        JSQMessage *previousMessage = [messagesArray objectAtIndex:indexPath.item - 1];
        if ([[previousMessage senderId] isEqualToString:[currentMessage senderId]]) {
            return 0.0f;
        }
    }
    
    return kJSQMessagesCollectionViewCellLabelHeightDefault;
}

- (CGFloat)collectionView:(JSQMessagesCollectionView *)collectionView
                   layout:(JSQMessagesCollectionViewFlowLayout *)collectionViewLayout heightForCellBottomLabelAtIndexPath:(NSIndexPath *)indexPath
{
    return 0.0f;
}



#pragma mark - UICollectionView DataSource

- (NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    // NSLog(@"messagesArray : %@" , messagesArray);
    return [messagesArray count];
}

- (UICollectionViewCell *)collectionView:(JSQMessagesCollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    /**
     *  Override point for customizing cells
     */
    JSQMessagesCollectionViewCell *cell = (JSQMessagesCollectionViewCell *)[super collectionView:collectionView cellForItemAtIndexPath:indexPath];
    
    
    JSQMessage *msg = [messagesArray objectAtIndex:indexPath.item];
    
    if (!msg.isMediaMessage) {
        
        if ([msg.senderId isEqualToString: _myUserId]) {
            cell.textView.textColor = [UIColor blackColor];
        }
        else {
            cell.textView.textColor = [UIColor whiteColor];
        }
        
        cell.textView.linkTextAttributes = @{ NSForegroundColorAttributeName : cell.textView.textColor,
                                              NSUnderlineStyleAttributeName : @(NSUnderlineStyleSingle | NSUnderlinePatternSolid) };
    }
    
    return cell;
}

- (void)collectionView:(JSQMessagesCollectionView *)collectionView didTapMessageBubbleAtIndexPath:(NSIndexPath *)indexPath {
    
    JSQMessage *msg = [messagesArray objectAtIndex:indexPath.item];

    if (msg.isMediaMessage) {
        id<JSQMessageMediaData> mediaItem = msg.media;
        
        if ([mediaItem isKindOfClass:[JSQPhotoMediaItem class]]) {
            JSQPhotoMediaItem *photoItem = (JSQPhotoMediaItem *)mediaItem;
            UIImage *image = photoItem.image;
            PEPSDisplayImageViewController *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"displayImageScene"];
            vc.imageURLToDisplay = [NSString stringWithFormat:@"%@",photoItem.imageURL];
            vc.imageURL =photoItem.imageURL;
            vc.imageToDisplay = image;
           // vc.type=2;
            vc.type=3;
            [self presentViewController:vc animated:YES completion:nil];
        }
        
        else if ([mediaItem isKindOfClass:[JSQVideoMediaItem class]]) {
            NSLog(@"should open video !");
            JSQVideoMediaItem *vidItem = (JSQVideoMediaItem *)mediaItem;
            NSURL *url = vidItem.fileURL;
            NSLog(@"fileURL being displayed now : %@" , url);

           /* NSString *urlStr = [NSString stringWithFormat:@"http://%@",url];
            urlStr = [urlStr stringByReplacingOccurrencesOfString:@" " withString:@"%20"];
            NSURL *videoUrlToSend = [NSURL URLWithString:urlStr];
            */
            
            
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willEnterFullscreen:) name:MPMoviePlayerWillEnterFullscreenNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(willExitFullscreen:) name:MPMoviePlayerWillExitFullscreenNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(enteredFullscreen:) name:MPMoviePlayerDidEnterFullscreenNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(exitedFullscreen:) name:MPMoviePlayerDidExitFullscreenNotification object:nil];
            [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(playbackFinished:) name:MPMoviePlayerPlaybackDidFinishNotification object:nil];
            
          //  NSURL* movieURL =  [NSURL fileURLWithPath:[[NSBundle mainBundle] pathForResource:@"tron" ofType:@"mov"]];
            
            movieController = [[MPMoviePlayerController alloc] initWithContentURL:url];
            movieController.view.frame = self.view.frame;
            [self.view addSubview:movieController.view];
            [movieController setFullscreen:YES animated:NO];
            [movieController play];
        
            
            
            
            /*
            MPMoviePlayerViewController *mp = [[MPMoviePlayerViewController alloc] initWithContentURL:videoUrlToSend];
            
            [[NSNotificationCenter defaultCenter] addObserver:self
                                                     selector:@selector(moviePlaybackDidFinish:)
                                                         name:MPMoviePlayerPlaybackDidFinishNotification
                                                       object:mp];
            
            mp.moviePlayer.movieSourceType = MPMovieSourceTypeStreaming;
            
            [self presentMoviePlayerViewControllerAnimated:mp];
             */
            
            
            
            
            /*
            JSQVideoMediaItem *vidItem = (JSQVideoMediaItem *)mediaItem;
            UIImage *image = photoItem.image;
            PEPSDisplayImageViewController *vc =[self.storyboard instantiateViewControllerWithIdentifier:@"displayImageScene"];
            vc.imageURLToDisplay = [NSString stringWithFormat:@"%@",photoItem.imageURL];
            vc.imageToDisplay = image;
            vc.type=2;
            [self presentViewController:vc animated:YES completion:nil];
             */
        }

    }
}

- (void)willEnterFullscreen:(NSNotification*)notification {
    NSLog(@"willEnterFullscreen");
}

- (void)enteredFullscreen:(NSNotification*)notification {
    NSLog(@"enteredFullscreen");
}

- (void)willExitFullscreen:(NSNotification*)notification {
    NSLog(@"willExitFullscreen");
}

- (void)exitedFullscreen:(NSNotification*)notification {
    NSLog(@"exitedFullscreen");
    [movieController.view removeFromSuperview];
    movieController = nil;
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

- (void)playbackFinished:(NSNotification*)notification {
    NSNumber* reason = [[notification userInfo] objectForKey:MPMoviePlayerPlaybackDidFinishReasonUserInfoKey];
    switch ([reason intValue]) {
        case MPMovieFinishReasonPlaybackEnded:
            NSLog(@"playbackFinished. Reason: Playback Ended");
            break;
        case MPMovieFinishReasonPlaybackError:
            NSLog(@"playbackFinished. Reason: Playback Error");
            break;
        case MPMovieFinishReasonUserExited:
            NSLog(@"playbackFinished. Reason: User Exited");
            break;
        default:
            break;
    }
 //   [movieController setFullscreen:NO animated:NO];
    
}


-(void)moviePlaybackDidFinish{

    NSLog(@"MOVIE FINISHED PLAYING ! ");
}
#pragma mark - Images Selection

-(void)selectImagesByUploading
{
    if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
    {
        UIImagePickerController *controller = [[UIImagePickerController alloc] init];
        controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        controller.allowsEditing = NO;
        controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
                                 UIImagePickerControllerSourceTypePhotoLibrary];
        controller.delegate = self;
        [self.navigationController presentViewController: controller animated: YES completion: nil];
    }
}

-(void)selectImagesByCapturing
{
    ipc = [[UIImagePickerController alloc] init];
    ipc.delegate = self;
    if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
    {
        ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
        [self presentViewController:ipc animated:YES completion:NULL];
    }
    else
    {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
        [alert show];
        alert = nil;
    }
}
- (void)selectVideoFromLibrary
{
    UIImagePickerController* imagePicker = [[UIImagePickerController alloc] init];
    imagePicker.delegate = self;
    imagePicker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
    imagePicker.mediaTypes = @[(NSString*)kUTTypeMovie];
    
    [self presentViewController:imagePicker animated:YES completion:nil];
}
#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    NSString *mediaType = info[UIImagePickerControllerMediaType];
    
        //================= if Media is a video ==================
    
    if ([mediaType isEqualToString:(NSString *)kUTTypeMovie]) //
    {
        NSURL* url1 = info[UIImagePickerControllerMediaURL];
        videoUrl = [[NSURL alloc] initWithString:[url1 absoluteString]];
        NSLog(@"URL : %@" , videoUrl);
        
        [self progressIndicatorView];
        NSURL *urlvideo = [info objectForKey:UIImagePickerControllerMediaURL];
        NSLog(@"urlvideo is :::%@",urlvideo);
        
        NSError *error = nil;
        NSDictionary * properties = [[NSFileManager defaultManager] attributesOfItemAtPath:urlvideo.path error:&error];
        NSNumber * size = [properties objectForKey: NSFileSize];
        NSLog(@"size: %@", size);
        
        AVPlayerItem *playerItem = [AVPlayerItem playerItemWithURL:urlvideo];
        CMTime duration = playerItem.duration;
        float seconds = CMTimeGetSeconds(duration);
        NSLog(@"duration: %.2f", seconds);
        
        /*urlvideo contains the URL of that video file that has to be uploaded. Then convert the url into NSString type because setFile method requires NSString as a parameter
         */
        NSString *urlString=[urlvideo path];
        NSString *ext = [urlString pathExtension];
        ext = [ext lowercaseString];
        NSLog(@" video extension: %@" , ext);
        
        if ([ext isEqualToString:@"mov"] ||[ext isEqualToString:@"flv"] ||[ext isEqualToString:@"mp4"] ||[ext isEqualToString:@"m4v"]) {
            
            NSData* videoData = [NSData dataWithContentsOfFile:[videoUrl path]];
            int videoSize = [videoData length]/1024/1024;
            if(videoSize <= 10) // check size of video
            {
               // NSString *videoName = urlString.lastPathComponent;
                NSString *urlpath = [NSString stringWithFormat:@"http://www.panteraenergy.pk/picksha/api/submitNewMsg.php?to=%@",_messageFromUserId];
                NSURL *url = [NSURL URLWithString:[urlpath stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding]];
                
                [self addVideoMediaMessage:url urlString:urlString ];
                [picker dismissViewControllerAnimated:YES completion:nil];

                /*
                request = [ASIFormDataRequest requestWithURL:url];
                [request setPostValue:@"video" forKey:@"write_msg"];
                [request setFile:urlString forKey:@"msg_file"];
                [request setRequestMethod:@"POST"];
                [request setDelegate:self];
                [request setDidStartSelector:@selector(requestStarted:)];
                [request setDidFinishSelector:@selector(requestFinished:)];
                [request setDidFailSelector:@selector(requestFailed:)];
                [request setUploadProgressDelegate:self];
                [request setTimeOutSeconds:50000];
                [request startAsynchronous];
                NSLog(@"responseStatusCode %i",[request responseStatusCode]);
                NSLog(@"responseStatusCode %@",[request responseString]);
                progressIndicator.hidden=NO;
                 */
            }
            else{
                
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Video Size should not be more than 10 MB"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
                [alert show];
            }
        }
        else
        {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"File format not supported"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
            [alert show];
        }
    }
    
    //================= if it's an image ==================
    
    else{
        imageToBeSentToServer = [info objectForKey:UIImagePickerControllerOriginalImage];
        
        float width,height;
        width = imageToBeSentToServer.size.width;
        height = imageToBeSentToServer.size.height;
        
        if (width>720.0f && height>720.0f) {
            if (width<height) {
                height = height/width*720.0f;
                width=720.0f;
            }else{
                width = width/height*720.0f;
                height = 720.0f;
            }
        }
        
        CGSize newSize = CGSizeMake(width, height);
        UIGraphicsBeginImageContext(newSize);
        
        [imageToBeSentToServer drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
        imageToBeSentToServer = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        [self addPhotoMediaMessage:imageToBeSentToServer];
        
        //NSLog(@"imageToBeSentToSe rver : %@" , imageToBeSentToServer);
        
        /////////////////      [allTemporaryImagesArrayyToDisplay addObject:imageToBeSentToServer];
        
        //NSLog(@"allTemporaryImagesArrayyToDisplay.count : %lu" , (unsigned long)allTemporaryImagesArrayyToDisplay.count);
        //NSLog(@"allTemporaryImagesArrayyToDisplay : %@" , allTemporaryImagesArrayyToDisplay);
        
        [picker dismissViewControllerAnimated:YES completion:nil];
        
        /* if (_type ==0) {
         
         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         [[webServices sharedServices] uploadImagesTemporarilywithImage:imageToBeSentToServer withImageName:@"" aboutPhoto:@"My First Photo" cover:@"0" handler:^(NSData *data){
         [self connectionGotData:data];
         [hud hide:YES];
         }errorHandler:^(NSError *error){
         [self connectionFailedWithError:error];
         [hud hide:YES];
         }];
         }
         else if(_type==1){
         
         MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
         [[webServices sharedServices] editGallerywithImage:imageToBeSentToServer userUploadingUsername:_otherUserName withGalleryId:_albumIdToEdit cover:@"0" handler:^(NSData *data){
         [self connectionGotData:data];
         [hud hide:YES];
         }errorHandler:^(NSError *error){
         [self connectionFailedWithError:error];
         [hud hide:YES];
         }];
         
         
         }
         */
    }
    
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - video request

- (void)requestStarted:(ASIHTTPRequest *)theRequest {
    NSLog(@"response started new::%@",[theRequest responseString]);
    //[self showProgress];
}
//- (void)requestFinished:(ASIHTTPRequest *)theRequest videoMessage:(JSQMessage *)videoMessage {
- (void)requestFinished:(ASIHTTPRequest *)theRequest  {
    NSLog(@"response finished new ::%@",[theRequest responseString]);
    NSString *responseString =[theRequest responseString];
    if ([responseString containsString:@"200"]) {
        [messagesArray addObject:videoMessage];
        [JSQSystemSoundPlayer jsq_playMessageSentSound];
        
        [self finishSendingMessageAnimated:YES];
        [[webServices sharedServices]getMessagesForSpecificUserWithId:_messageFromUserId handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
    }
    //[self hideProgress];
    progressIndicator.hidden = YES;
    [aProgressView removeFromSuperview];
    
  //  UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Success" message:@"Video upload to server successfully!"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
   // [alert show];
    
}


- (void)requestFailed:(ASIHTTPRequest *)theRequest {
    NSLog(@"response Failed new ::%@, Error:%@",[theRequest responseString],[theRequest error]);
    //[self hideProgress];
    progressIndicator.hidden = YES;
    [aProgressView removeFromSuperview];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Failed" message:@"Video Upload to server failed, please try again"  delegate:self cancelButtonTitle:@"Ok" otherButtonTitles: nil];
    [alert show];
    
    
}

#pragma mark Upload Progress Tracking


- (void)setProgress:(float)newProgress {
    
    [progressIndicator setProgress:newProgress];
    NSString* formattedNumber = [NSString stringWithFormat:@"%.f %@", [progressIndicator progress]*100, @"%"];
    
    self.progressLabel.text = formattedNumber;
    self.progressLabel.textColor = [UIColor blackColor];
    [self.progressLabel setFont:[UIFont fontWithName:@"Arial" size:12]];
    
}

- (void)video:(NSString*)videoPath didFinishSavingWithError:(NSError*)error contextInfo:(void*)contextInfo
{
    if (error) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:@"Video Save failed, please try again"  delegate:nil cancelButtonTitle:@"OK" otherButtonTitles: nil, nil];
        [alert show];
    }else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Video Saved" message:@"Saved To Photo Album"  delegate:self cancelButtonTitle:@"OK" otherButtonTitles: nil];
        [alert show];
    }
}


#pragma UserDefined Method:::
-(void)progressIndicatorView{
    
    //self.Record_button.hidden =YES;
    CGRect UploadProgressFrame = CGRectMake(60, 150, 200,100);
    aProgressView = [[UIView alloc] initWithFrame:UploadProgressFrame];
    [aProgressView setBackgroundColor:[UIColor whiteColor]];
    [aProgressView.layer setCornerRadius:10.0f];
    [aProgressView.layer setBorderWidth:1.0f];
    [aProgressView.layer setBorderColor:[UIColor blackColor].CGColor];
    
    UILabel *progressTitleLabel = [[UILabel alloc] initWithFrame:CGRectMake(40, 25, 150, 30)];
    progressTitleLabel.backgroundColor = [UIColor whiteColor];
    [progressTitleLabel setFont:[UIFont fontWithName:@"Arial" size:15]];
    progressTitleLabel.text = @"Uploading Video";
    progressTitleLabel.textColor = [UIColor blackColor];
    
    self.progressLabel = [[UILabel alloc]initWithFrame:CGRectMake(90, 80, 70, 15)];
    [self.progressLabel setBackgroundColor:[UIColor whiteColor]];
    progressIndicator = [[UIProgressView alloc] init];
    progressIndicator.frame = CGRectMake(30,65,140,20);
    [aProgressView addSubview:progressTitleLabel];
    [aProgressView addSubview:self.progressLabel];
    [aProgressView addSubview:progressIndicator];
    [self.view addSubview:aProgressView];
    
}


#pragma UIProgressBar Method::
-(void)showProgress
{
    if (!aMBProgressHUD)
        aMBProgressHUD = [[MBProgressHUD alloc] initWithView:self.view];
    
    [self.view addSubview:aMBProgressHUD];
    aMBProgressHUD.labelText = @"Uploading Video";
    [aMBProgressHUD show:YES];
}

-(void)hideProgress
{
    [aMBProgressHUD hide:YES];
    [aMBProgressHUD removeFromSuperview];
    aMBProgressHUD=nil;
}

@end
