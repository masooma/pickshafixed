//
//  commentTableViewCell.h
//  Picksha
//
//  Created by iOS Department-Pantera on 19/06/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>




@protocol commentTableViewCellDelegate <NSObject>
@required
//- (void)friendRequestAccepted;
- (void)commentLiked;

@end
@interface commentTableViewCell : UITableViewCell{
    id<commentTableViewCellDelegate> delegate;
}
@property (nonatomic, strong) id<commentTableViewCellDelegate> delegate;

@property (nonatomic,strong) NSDictionary *dataDictionaryforCell;
@property (nonatomic,strong) NSIndexPath *indexPathNumber;

//@property (strong, nonatomic) IBOutlet UIButton *crossButton;

-(void)setupCell;
@end




