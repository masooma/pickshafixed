//
//  PEPSInviteFriendsViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 15/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSInviteFriendsViewController.h"
#import "UIImageView+WebCache.h"
@interface PEPSInviteFriendsViewController ()

@end

@implementation PEPSInviteFriendsViewController{
    
    IBOutlet UIButton *theButton;
    IBOutlet UICollectionView *friendsCollectionView;
    NSMutableArray *friendArray;
    NSArray *returnedDataArray;
    NSMutableArray *selectedItems;
    NSMutableArray *selectedPhotos;
    NSMutableArray *selectedindexPaths;
    
    NSInteger count;
    NSMutableArray *alreadyInvitedFriendsIds;
    
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    
    NSInteger success;
    NSInteger failed;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    //NSLog(@"my user is: %@",[[webServices sharedServices] myUser]);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    //NSLog(@"myUserName : %@ " , myUserName);
    //NSLog(@"myUserId : %@ " , myId);
    

   
    friendArray = [[NSMutableArray alloc] init];
    selectedItems = [[NSMutableArray alloc] init];
    selectedindexPaths = [[NSMutableArray alloc] init];
    selectedPhotos = [[NSMutableArray alloc] init];
    alreadyInvitedFriendsIds = [[NSMutableArray alloc] init];
    friendsCollectionView.allowsMultipleSelection = YES;
    if (_type==0) {

        UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Invite" style: UIBarButtonItemStylePlain target:self action:@selector(inviteFriends)];[self.navigationItem setRightBarButtonItem:addButton];
        
      //  [theButton setTitle:@"Invite Friends" forState:UIControlStateNormal];
    }
    
    if (_type==1 || _type==2) {
        [self getGalleryDetailswithAlbumID:_albumId];
        if (_type==1){
                UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Delete Photos" style: UIBarButtonItemStylePlain target:self action:@selector(inviteFriends)];[self.navigationItem setRightBarButtonItem:addButton];
       // [theButton setTitle:@"Delete Photos" forState:UIControlStateNormal];
        }
        if (_type==2) {
        [theButton setTitle:@"" forState:UIControlStateNormal];
            friendsCollectionView.multipleTouchEnabled = NO;
        }
    }
    
    
}

-(void)getGalleryDetailswithAlbumID:(NSString *)albumID{
    
    [[webServices sharedServices]getGalleryDetailWithAlbumId:albumID handler:^(NSData *data){
        [self getGalleryDetailsConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    [friendsCollectionView reloadData];
    
}
-(void)getGalleryDetailsConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    returnedDataArray = returnedDict[@"Data"];
    NSLog(@"returnedDataArray.count CONNECTION GOT DATA : %lu" , (unsigned long)returnedDataArray.count);
    
    [friendArray removeAllObjects];
    
    if (_type==1) {

    
    for (int i = 0; i<returnedDataArray.count; i++) {
        if ([[returnedDataArray objectAtIndex:i][@"delete"] integerValue ] == 1) {
            [friendArray addObject:[returnedDataArray objectAtIndex:i]];
        }
    }
  
    }
    else
        friendArray = [returnedDataArray mutableCopy];
    NSLog(@"friendArray.count CONNECTION GOT DATA : %lu" , (unsigned long)friendArray.count);
    
    NSLog(@"NEW ARRAY !@ : %@" , friendArray);


    NSLog(@"DataArray getGalleryDetails: %@" , friendArray);
    
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];

    [friendsCollectionView reloadData];
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    if (_type==0) {

        [theButton setTitle:@"Invite Friends" forState:UIControlStateNormal];
        
        [[webServices sharedServices] getInvitedFriendsForAlbumId:_albumId handler:^(NSData *data){
            [self getInvitedFriendsConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
    if (_type==1) {
        [self getGalleryDetailswithAlbumID:_albumId];
        [theButton setTitle:@"Delete Photos" forState:UIControlStateNormal];
    }


    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)inviteFriends{
    

    
    
    if (_type ==0) {

    
        if (selectedItems.count == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Select friends to invite." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            // alert.tag = 1001;
            [alert show];
        }
        else{
            for (int i =0; i<selectedItems.count; i++) {
                
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
                [[webServices sharedServices]inviteFriendToAlbumId:_albumId senderId:myId receiverId:[selectedItems objectAtIndex:i] handler:^(NSData *data){
                    BOOL isLast = (i==selectedItems.count-1);
                    [self inviteFriendsConnectionGotData:data isLast:isLast ];
                    [hud hide:YES];
                }errorHandler:^(NSError *error){
                    [self connectionFailedWithError:error];
                    [hud hide:YES];
                }];
            }
        }
        
    }
    
    else if (_type==1){
        
        
        
        if (selectedPhotos.count == 0) {
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Select photos to delete." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            // alert.tag = 1001;
            [alert show];
        }
        else{
            
            
            UIAlertView *uploadAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Delete Photos?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            uploadAlert.tag = 1002;
            [uploadAlert show];
            
            

        }
        
    }

    
        
    
    
    }
    
    
- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    
    switch (actionSheet.tag ) {
        case 1002:
            if (buttonIndex == 0)
            {
               // NSLog(@"NO");
               // NSLog(@"case 1002 for NOT deleting photos");
                
            }
            else
            {
               // NSLog(@"YES deleting photos");

                for (int i =0; i<selectedPhotos.count; i++) {
                    MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                    BOOL isLastPhoto = (i==selectedPhotos.count-1);
                    NSString * isLastPhotoString;
                    isLastPhotoString = @"0";
                 
                    /*if (selectedPhotos.count == friendArray.count ) {
                        isLastPhotoString =@"1";
                    }
                    else
                        isLastPhotoString = @"0";
                     */
                    
                    
                    [[webServices sharedServices] deletePhotoByUsername:myUserName galleryId:_albumId objectId:selectedPhotos[i] deleteAll:isLastPhotoString handler:^(NSData *data){
                        [self deletePhotosConnectionGotData:data isLastPhoto:isLastPhoto];
                        [hud hide:YES];
                    }errorHandler:^(NSError *error){
                        [self deleteConnectionFailedWithError:error];
                        [hud hide:YES];
                    }];
                }
                
            }
            break;
    }
}

-(void)getFriendList{
    //NSLog(@"getting friend list !");
    [[webServices sharedServices] getFriendListWithHandler:^(NSData *data){
        [self getFriendListConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}


#pragma mark - Collection View Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    

        return friendArray.count;

}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *placeholder;
    if (_type == 1 || _type == 2) {
        placeholder =  @"placeholderPahaar";
    }
    else
        placeholder =@"personPlaceholder";
    UICollectionViewCell *cell;
    
    if (_type == 0) {
        
        if (alreadyInvitedFriendsIds != nil) {
        //NSLog(@"type 0");
        
        
            if ([alreadyInvitedFriendsIds containsObject:friendArray[indexPath.row][@"id"] ]) {
            
        
                //NSLog(@"yes, contains that id");
        
                cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"disabledCell" forIndexPath:indexPath];
    
            }
    
            else{
        
    
                cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
    
            }
        
            if ([selectedindexPaths containsObject:indexPath]) {
                   cell.contentView.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
            }
            else{
                cell.contentView.backgroundColor = [UIColor clearColor];
            
            }
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:22];
        UILabel *name = (UILabel *)[cell viewWithTag:2];
    
        NSString *imageURL = friendArray[indexPath.row][@"imageUrl"];

        imageView.layer.cornerRadius = imageView.frame.size.height/2;
        imageView.layer.borderColor = [UIColor blackColor].CGColor;
        imageView.layer.borderWidth = 1.5;
        imageView.clipsToBounds = YES;

            
        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
            [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];


        /*
            
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] placeholderImage:[UIImage imageNamed:placeholder]];
         */
        
        name.text = friendArray[indexPath.row][@"name"];
    
        
    }
    
    }
    else if (_type == 1 || _type ==2) {
     
        
        
        cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
        
        if ([selectedindexPaths containsObject:indexPath]) {
            cell.contentView.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
        }
        else{
            cell.contentView.backgroundColor = [UIColor clearColor];
            
        }
        
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:22];

        imageView.layer.cornerRadius = imageView.frame.size.height/2;
        imageView.layer.borderColor = [UIColor blackColor].CGColor;
        imageView.layer.borderWidth = 1.5;
        imageView.clipsToBounds = YES;
        
        NSString *imageURL = friendArray[indexPath.row][@"imageUrl"];
        
        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
        [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
        
      /*

        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
       */
  
    }
    
        return cell;
  
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    if (_type==0) {

    
    NSString *friendId = friendArray[indexPath.row][@"id"];
    //NSLog(@" friendId : %@" ,friendId);
        
    [selectedItems addObject: friendId];
    [selectedindexPaths addObject:indexPath];
        
    //NSLog(@"selectedItems : %@" , selectedItems);
        
    UICollectionViewCell* cell=[friendsCollectionView cellForItemAtIndexPath:indexPath];
        
    //cell.selectedBackgroundView.backgroundColor= [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
   cell.contentView.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
        
        
    }
    
    else if (_type==1){
    
         NSString *photoId = friendArray[indexPath.row][@"imageId"];
        //NSLog(@" photoId : %@" ,photoId);
        
        [selectedPhotos addObject: photoId];
        [selectedindexPaths addObject:indexPath];
        
       // NSLog(@"selectedPhotos : %@" , selectedPhotos);
        UICollectionViewCell* cell=[friendsCollectionView cellForItemAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
    
    }
    
    else if (_type==2){
        
        NSString *photoId = friendArray[indexPath.row][@"imageId"];
        //NSLog(@" photoId : %@" ,photoId);

        [selectedindexPaths addObject:indexPath];
        
       // NSLog(@"selectedPhotos : %@" , selectedPhotos);
        UICollectionViewCell* cell=[friendsCollectionView cellForItemAtIndexPath:indexPath];
        cell.contentView.backgroundColor = [UIColor colorWithRed:0.0f/255.0f green:145.0f/255.0f blue:208.0f/255.0f alpha:1.0f];
        
        [[webServices sharedServices] makeCoverPhotoWithImageId:photoId galleryId:_albumId handler:^(NSData *data){
            [self makeCoverConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        
    }
    
}
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (_type==0) {

    NSString *friendId = friendArray[indexPath.row][@"id"];
    //NSLog(@" friendId deselected : %@" ,friendId);
        
    [selectedItems removeObject:friendId];
    [selectedindexPaths removeObject:indexPath];
        
    //NSLog(@"selectedItems : %@" , selectedItems);
    UICollectionViewCell* cell=[friendsCollectionView cellForItemAtIndexPath:indexPath];
    cell.contentView.backgroundColor = nil;
        
    }
    
    else if (_type==1){
        NSString *photoId = friendArray[indexPath.row][@"imageId"];
        //NSLog(@" photoId deselected : %@" ,photoId);
        
        [selectedPhotos removeObject: photoId];
        [selectedindexPaths removeObject:indexPath];
        
        //NSLog(@"selectedPhotos  : %@" ,selectedPhotos );
        UICollectionViewCell* cell=[friendsCollectionView cellForItemAtIndexPath:indexPath];
            cell.contentView.backgroundColor = nil;
    }
}

#pragma -mark conection

-(void)getFriendListConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //NSLog(@"getFriendListConnectionGotData returnedDict for %@" , returnedDict);
    
    if (returnedDict != nil) {
        friendArray = returnedDict[@"Data"];
        NSLog(@"ARRRRAYYY !!!! : %@", friendArray);
    }
    [friendsCollectionView reloadData];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];*/
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
}


-(void)inviteFriendsConnectionGotData:(NSData *)data isLast:(BOOL) isLast{


    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //NSLog(@"INVITEFRIENDS ConnectionGotData returnedDict for %@" , returnedDict);
    if ([returnedDict[@"code"] isEqualToString:@"200"]) {
        count++;
    }
    
   // userId = returnedDict[@"id"];
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    if (isLast) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Friends have been invited to the gallery." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        // alert.tag = 1001;
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
   }
-(void)getInvitedFriendsConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSArray * dataArray = returnedDict[@"Data"];
   // NSLog(@"returnedDict getInvitedFriends : %@" , returnedDict);
   // NSLog(@" GET INVITED FRIENDS ConnectionGotData returnedDict for %@" , returnedDict);
    for (int i = 0; i<dataArray.count; i++) {
        
    
   // if ([dataArray[i][@"is_view"] isEqualToString:@"1"]) {
        [alreadyInvitedFriendsIds addObject:dataArray[i][@"receiver"]];
       // NSLog(@"dataArray : %@" , dataArray);
        // NSLog(@"alreadyInvitedFriendsIds : %@" , alreadyInvitedFriendsIds);
       // [self getFriendList];
   // }
    }
    [self getFriendList];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}

-(void)deletePhotosConnectionGotData:(NSData *)data isLastPhoto:(BOOL) isLastPhoto{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];

    //NSLog(@" deletePhotosConnectionGotData ConnectionGotData returnedDict for %@" , returnedDict);
    if ([returnedDict[@"code"] integerValue] == 200) {
        count++;
        

        NSString *messageToShow;
        if (isLastPhoto) {
            if (failed > 0) {
                 messageToShow = [NSString stringWithFormat:@"Photos have been deleted.\n%ld files deleted, %ld files failed" , (long)count , (long)failed];
            }
            else{
                messageToShow = [NSString stringWithFormat:@"Photos have been deleted.\n%ld files deleted" , (long)count];
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:messageToShow delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
            /*UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Photos have been deleted." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        // alert.tag = 1001;
        [alert show];
             */
            
            [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshAlbum" object:self];
            [self.navigationController popViewControllerAnimated:NO];
    }
   }
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}


-(void)makeCoverConnectionGotData:(NSData *)data {
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    
    NSString *message = returnedDict[@"message"];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:message delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    // alert.tag = 1001;
    [alert show];
    
    //NSLog(@"makeCoverConnectionGotData returnedDict for %@" , returnedDict);
    if ([returnedDict[@"code"] integerValue] == 200) {
 
        [[NSNotificationCenter defaultCenter] postNotificationName:@"refreshAlbum" object:self];
        
        
            [self.navigationController popViewControllerAnimated:YES];
        }
    }
    



-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 100;
    [alert show];
}
-(void)deleteConnectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    failed++;

    
}


@end
