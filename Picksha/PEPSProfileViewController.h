//
//  PEPSProfileViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 09/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEPSProfileViewController : UIViewController< UICollectionViewDelegate , UICollectionViewDataSource , UIAlertViewDelegate , UISearchBarDelegate, UITableViewDataSource , UITableViewDelegate,UISearchDisplayDelegate>
@property (nonatomic ,assign) NSInteger type;// type 0: User's Own Profile 1: Other members profile
@property(nonatomic , strong) NSString *otherUserUsername;
@property(nonatomic , strong) NSString *otherUserUserId;


@end
