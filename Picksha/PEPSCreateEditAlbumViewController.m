//
//  PEPSCreateEditAlbumViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 21/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSCreateEditAlbumViewController.h"

@interface PEPSCreateEditAlbumViewController ()

@end

@implementation PEPSCreateEditAlbumViewController{

    IBOutlet UICollectionView *selectedImagesCollectionView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return 40;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
    UIImageView *selectedImage = (UIImageView *) [cell viewWithTag:11];
    selectedImage.layer.cornerRadius = selectedImage.frame.size.height/4;
    selectedImage.layer.borderColor = [UIColor whiteColor].CGColor;
    //selectedImage.layer.borderWidth = 3.0;
   // selectedImage.clipsToBounds = YES;
    
    
    /*
    
    if (collectionView == friendsCollectionView) {
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:1];
        //  [imageView setImageWithURL:theItem.thumbURL placeholderImage:[UIImage imageNamed:@"sample_product"]];
        imageView.layer.cornerRadius = imageView.frame.size.height/2;
        imageView.layer.borderColor = [UIColor blackColor].CGColor;
        imageView.layer.borderWidth = 1.5;
        imageView.clipsToBounds = YES;
        
        return cell;
    } else  {
        UIImageView *imageView = (UIImageView *)[cell viewWithTag:11];
        //  [imageView setImageWithURL:theItem.thumbURL placeholderImage:[UIImage imageNamed:@"sample_product"]];
        imageView.layer.cornerRadius = imageView.frame.size.height/2;
        imageView.layer.borderColor = [UIColor blackColor].CGColor;
        imageView.layer.borderWidth = 1.5;
        imageView.clipsToBounds = YES;
        
        UIButton *tickButton = (UIButton *)[cell viewWithTag:1];
        UIButton *crossButton = (UIButton *)[cell viewWithTag:1];
        
        
        tickButton.hidden = YES;
        crossButton.hidden = YES;
        tickButton.enabled = NO;
        crossButton.enabled= NO;
     */
        
        return cell;
        
    }


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    // Return the number of rows in the section.
    //return messagesArray.count;
    return 20;
}
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
 
  
    
   UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:reuseId forIndexPath:indexPath];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}
*/

@end
