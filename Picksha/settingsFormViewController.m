//
//  settingsFormViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 01/06/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "settingsFormViewController.h"

@interface settingsFormViewController ()

@end

@implementation settingsFormViewController{

    IBOutlet UITextField *textfield1;
    IBOutlet UITextField *textfield2;
    IBOutlet UITextField *textfield3;

    IBOutlet UIButton *deleteButton;
    
    IBOutlet UIButton *sendPassword;
    IBOutlet UIButton *saveChangesButton;
    IBOutlet UIButton *changePasswordButton;
    IBOutlet UIButton *deactivateButton;
    UIGestureRecognizer *tapRecognizer;
    
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    NSString *myEmail;
    NSString *myRealName;
    NSString *usernameToSend;
    NSString *realNameToSend;
    NSString *emailToSend;
    


}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    NSLog(@"type FRO SETTINGS FORM ! : %ld" , (long)_type);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    myRealName=[[webServices sharedServices] myUser][@"realname"];
    myEmail=[[webServices sharedServices] myUser][@"email"];

    NSLog(@"myUserName : %@ " , myUserName);
    NSLog(@"myUserId : %@ " , myId);
    NSLog(@"myImageURL : %@ " , myImageURL);
    NSLog(@"myEmail : %@ " , myEmail);
    NSLog(@"myRealName : %@ " , myRealName);
    [self setupView];
    
    switch (_type) {
        case 1:
        {

            deactivateButton.hidden=NO;
            deactivateButton.enabled = YES;
            deleteButton.hidden = YES;
            deleteButton.enabled=NO;
            changePasswordButton.hidden = YES;
            changePasswordButton.enabled=NO;
            saveChangesButton.hidden= YES;
            saveChangesButton.enabled=NO;
            sendPassword.hidden=YES;
            sendPassword.enabled=NO;
            break;
        }
        case 2:
        {
            NSLog(@"TYPE DELETE !!!");
            
            deactivateButton.hidden=YES;
            deactivateButton.enabled = NO;
            
            NSLog(@"SHOWING DELETE BUTTON !!!");
            deleteButton.hidden = NO;
            deleteButton.enabled=YES;
            
            changePasswordButton.hidden = YES;
            changePasswordButton.enabled=NO;
            saveChangesButton.hidden= YES;
            saveChangesButton.enabled=NO;
            sendPassword.hidden=YES;
            sendPassword.enabled=NO;
            
            break;
        }
        case 3:
        {
            deactivateButton.hidden=YES;
            deactivateButton.enabled = NO;
            deleteButton.hidden = YES;
            deleteButton.enabled=NO;
            changePasswordButton.hidden = NO;
            changePasswordButton.enabled=YES;
            saveChangesButton.hidden= YES;
            saveChangesButton.enabled=NO;
            sendPassword.hidden=YES;
            sendPassword.enabled=NO;
            break;
        }
        case 4:
        {
            deactivateButton.hidden=YES;
            deactivateButton.enabled = NO;
            deleteButton.hidden = YES;
            deleteButton.enabled=NO;
            changePasswordButton.hidden = YES;
            changePasswordButton.enabled=NO;
            saveChangesButton.hidden= NO;
            saveChangesButton.enabled=YES;
            sendPassword.hidden=YES;
            sendPassword.enabled=NO;
            break;
        }
        case 5:
        {
            NSLog(@"forgot password wali config");
            deactivateButton.hidden=YES;
            deactivateButton.enabled = NO;
            deleteButton.hidden = YES;
            deleteButton.enabled=NO;
            changePasswordButton.hidden = YES;
            changePasswordButton.enabled=NO;
            saveChangesButton.hidden= YES;
            saveChangesButton.enabled=NO;
            sendPassword.hidden=NO;
            sendPassword.enabled=YES;
            break;
        }
            
        default:
            break;
    }
    //For Hiding Keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    /* [albumTitleField resignFirstResponder];
     [realNameField resignFirstResponder];
     [emailField resignFirstResponder];
     */
}
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    
}
-(void)setupView{
    if (_type == 3) {
        NSLog(@"type 3 !");
        textfield1.enabled = YES;
        textfield1.hidden = NO;
        textfield2.enabled = YES;
        textfield2.hidden = NO;
        textfield3.enabled = YES;
        textfield3.hidden = NO;
        

        textfield1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Current Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield1.secureTextEntry = YES;

        textfield2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield2.secureTextEntry = YES;

        textfield3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield3.secureTextEntry = YES;

    }
    else if (_type == 4) {
        
        NSLog(@"type 4 !");
        
        textfield1.enabled = YES;
        textfield1.hidden = NO;
        textfield2.enabled = YES;
        textfield2.hidden = NO;
        textfield3.enabled = YES;
        textfield3.hidden = NO;
        NSLog(@"1");
        textfield1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield1.text = myUserName;

NSLog(@"1");
        textfield2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Real Name" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield2.text = myRealName;
NSLog(@"1");

        textfield3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield3.text = myEmail;
        NSLog(@"1");

    }
  
    
    else if (_type == 5) {
        
        NSLog(@"type 5 !");
        
        textfield1.enabled = NO;
        textfield1.hidden = YES;
        textfield2.enabled = NO;
        textfield2.hidden = YES;
        textfield3.enabled = YES;
        textfield3.hidden = NO;

        textfield3.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter your email" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        NSLog(@"1");
        
    }
    
    
    
    else
       
        
    { 
        textfield1.enabled = YES;
        textfield1.hidden = NO;
        textfield2.enabled = YES;
        textfield2.hidden = NO;
        textfield3.enabled = NO;
        textfield3.hidden = YES;

        textfield1.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield1.secureTextEntry = YES;


        textfield2.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        textfield2.secureTextEntry = YES;

    }

}


-(BOOL)getUserInfo{

        if (_type == 1 || _type == 2) {
            if ([textfield1.text   isEqual: @""]){
                [self shakeIt:textfield1];
                [textfield1 becomeFirstResponder];
            }
            else if ([textfield2.text   isEqual: @""] ){
                [self shakeIt:textfield2];
                [textfield2 becomeFirstResponder];
            }
            else if (![self validatePassword:textfield2.text secondPassword:textfield1.text]){
                [self shakeIt:textfield2];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Passwords don't match!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                [alert show];
                [textfield2 becomeFirstResponder];
            }
            else
            {
                return YES;
            }
        }
            else if (_type ==4){
                
                NSLog(@"getUSER info type4" );
            
                if ([textfield1.text  isEqual: @""] ){
                    [self shakeIt:textfield1];
                    [textfield1 becomeFirstResponder];
                }
                else if ([textfield2.text   isEqual: @""] ){
                    [self shakeIt:textfield2];
                    [textfield2 becomeFirstResponder];
                }
                else if ([textfield3.text   isEqual: @""] || ![self validateEmail:textfield3 .text] ){
                    [self shakeIt:textfield3];
                    [textfield3 becomeFirstResponder];
                }
                else
                {
                    return YES;
                }
            
            }
    
            else if (_type ==5){
                
                NSLog(@"getUSER info type5" );

                if ([textfield3.text   isEqual: @""] || ![self validateEmail:textfield3 .text] ){
                    [self shakeIt:textfield3];
                    [textfield3 becomeFirstResponder];
                }
                else
                {
                    return YES;
                }
                
            }
            else if (_type ==3){
                if ([textfield1.text   isEqual: @""] ){
                    [self shakeIt:textfield1];
                    [textfield1 becomeFirstResponder];
                }
                else if ([textfield2.text   isEqual: @""] ){
                    [self shakeIt:textfield2];
                    [textfield2 becomeFirstResponder];
                }
                else if ([textfield3.text   isEqual: @""] ){
                    [self shakeIt:textfield3];
                    [textfield3 becomeFirstResponder];
                }
                else if (![self validatePassword:textfield2.text secondPassword:textfield3.text]){
                    [self shakeIt:textfield3];
                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Passwords don't match!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
                    [alert show];
                    [textfield3 becomeFirstResponder];
                }
                else
                {
                    return YES;
                }

            }
  
    return NO;
}
-(void)shakeIt:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.07];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}
- (BOOL)validateEmail:(NSString *)emailStr {
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}
-(BOOL)validatePassword:(NSString *)pwd1 secondPassword : (NSString *)pwd2
{
    if([pwd1 isEqualToString:pwd2]){
        return  YES;
    }
    return NO;
}
- (IBAction)deactivateOnTap:(id)sender {
    
    if ([self getUserInfo]) {
        [[webServices sharedServices]deactivateAccountWithPassword:textfield1.text handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }

}

- (IBAction)deleteOnTap:(id)sender {
    if ([self getUserInfo]) {
        [[webServices sharedServices] deleteAccountWithPassword:textfield1.text handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}
- (IBAction)changePasswordOnTap:(id)sender {
    if ([self getUserInfo]) {
        [[webServices sharedServices] changePasswordWithNewPassword:textfield2.text oldPassword:textfield1.text handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
   }

}
- (IBAction)sendPasswordForForgotPassword:(id)sender {
        NSLog(@"sendPasswordForForgotPassword TAPPED");
    if ([self getUserInfo]) {
        [[webServices sharedServices] forgotPasswordEnterEmail:textfield3.text handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    
}
- (IBAction)saveChangesOnTap:(id)sender {
            NSLog(@"saving changes TAPPED");
    if ([self getUserInfo]) {
        NSLog(@"saving changes");
        [[webServices sharedServices] updateUsername:textfield1.text realName:textfield2.text email:textfield3.text handler:^(NSData *data){
            [self saveChangesConnectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}
-(void)saveChangesConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"save changes profile SETTINGS  %@" , [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSLog(@"returnedDict for save changes profile SETTINGS: %@" , returnedDict);

    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:returnedDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
    NSString *code = returnedDict[@"code"];
    if ([code isEqualToString:@"200"]) {
        [[webServices sharedServices]updateMyUser];
    }

    
}

-(void)connectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
        NSLog(@"returnedDict for profile SETTINGS: %@" , returnedDict);
    NSString *code = returnedDict[@"code"];
    NSString *message =returnedDict[@"message"];
    NSLog(@"message : %@" , returnedDict[@"message"]);
    NSLog(@"mesage : %@" , message);

   // else {
     //   if (_type==5) {
          //  NSLog(@"TYPE 5 !!!");
    
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:returnedDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
    
    textfield1.text=@"";
    textfield2.text=@"";
    textfield3.text=@"";
    
     //   }
    
    if (_type == 1 || _type ==2){
        if ([code integerValue] == 200) {
            [self logoutUser];
        }
        
    }
    

  //  }
   
}
-(void)logoutUser{

    [[webServices sharedServices] logoutUserWithHandler:^(NSData *data){
        [self logoutConnectionGotData:data];
      //  [FBSession.activeSession closeAndClearTokenInformation];
    }errorHandler:^(NSError *error){
        //[self connectionFailedWithError:error];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)logoutConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    // NSLog(@"logoutConnectionGotData: returnedDictionary : %@" , returnedDictionary);
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    if ([returnedDictionary[@"code"] isEqualToString:@"200"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Logout Successful!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        /* PEPSLoginSignupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupScene"];
         
         [self presentViewController:vc animated:YES completion:nil];*/
        [self.tabBarController dismissViewControllerAnimated:YES completion:nil];
    }
    else if ([returnedDictionary[@"code"] isEqualToString:@"300"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Could not Logout.\nPlease try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
@end
