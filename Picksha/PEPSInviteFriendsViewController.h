//
//  PEPSInviteFriendsViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 15/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEPSInviteFriendsViewController : UIViewController<UICollectionViewDataSource,UICollectionViewDelegate>
@property (nonatomic ,strong) NSString *albumId;
@property (nonatomic ,assign) NSInteger type; // 0 : invite friends , 1 : delete photos 2: change Cover

@end
