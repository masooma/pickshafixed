//
//  emailInviteViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 22/06/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "emailInviteViewController.h"

@interface emailInviteViewController ()

@end

@implementation emailInviteViewController
{
    IBOutlet UITableView *theTableView;
    NSMutableArray *notifications;
    NSMutableArray *dataArray;


}

- (void)viewDidLoad {
    [super viewDidLoad];
    dataArray = [[NSMutableArray alloc] init ];//WithObjects:@"Tiger",@"Leopard",@"Snow Leopard",@"Lion",nil];
    self.title = @"Invite Friends";
    
    [self addORDeleteRows];
    
    UIBarButtonItem *addButton = [[UIBarButtonItem alloc] initWithTitle:@"Invite" style: UIBarButtonItemStylePlain target:self action:@selector(sendInvite)];[self.navigationItem setRightBarButtonItem:addButton];
}
-(void)sendInvite{

    NSLog(@"send invite");
    if (dataArray.count == 0) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter an email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    else
    {
        for (int i =0; i<dataArray.count; i++) {
            
            MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
            [[webServices sharedServices] inviteFriendWithEmail:[dataArray objectAtIndex:i] galleryId:_albumID handler:^(NSData *data){
                BOOL isLast = (i==dataArray.count-1);
                [self inviteFriendsConnectionGotData:data isLast:isLast ];
                [hud hide:YES];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
                [hud hide:YES];
            }];
        }

    }
}
-(void)inviteFriendsConnectionGotData:(NSData *)data isLast:(BOOL) isLast{
    
    
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@"INVITEFRIENDS ConnectionGotData returnedDict for %@" , returnedDict);
   /* if ([returnedDict[@"code"] isEqualToString:@"200"]) {
        count++;
    }
    */
    
    // userId = returnedDict[@"id"];
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    if (isLast) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:returnedDict[@"message"] delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        // alert.tag = 1001;
        [alert show];
        [self.navigationController popViewControllerAnimated:YES];
    }
    
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 100;
    [alert show];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    if (section==0) {
        return [dataArray count];
    }else{
        if (self.editing) {
            return 1;
        }
        return 0;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    
    NSLog(@"dataArray  :%@" , dataArray);
    
    static NSString *CellIdentifier = @"emailCell";

    
    UITableViewCell *cell;
  
    if (indexPath.section==0) {
        NSLog(@"else");
        cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
        UILabel *emailLabel = (UILabel *)[cell viewWithTag:1];
        emailLabel.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:18.0];
        emailLabel.text = dataArray[indexPath.row];
    }
    else{
       /* cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.editingAccessoryType = YES;
       // cell.textLabel.text = @"Append a new row";
        emailTextfield = [[UITextField alloc] initWithFrame:CGRectMake(10, 13, 375, 30)];
        emailTextfield.placeholder = @"email";
        emailTextfield.autocorrectionType = UITextAutocorrectionTypeNo;
        [emailTextfield setClearButtonMode:UITextFieldViewModeWhileEditing];
        //cell.accessoryView = self.firstName;
        [cell.contentView addSubview:emailTextfield];
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;*/
        cell = [tableView dequeueReusableCellWithIdentifier:@"editCell"];
    }
    //cell.textLabel.text = [dataArray objectAtIndex:indexPath.row];
    return cell;
}
- (void)addORDeleteRows
{
    if(self.editing)
    {
        [super setEditing:NO animated:NO];
        [theTableView setEditing:NO animated:NO];
        [theTableView reloadData];
        [self.navigationItem.rightBarButtonItem setTitle:@"Dd Email"];
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStylePlain];
    }
    else
    {
        [super setEditing:YES animated:YES];
        [theTableView setEditing:YES animated:YES];
        [theTableView reloadData];
        [self.navigationItem.rightBarButtonItem setTitle:@"Done"];
        [self.navigationItem.rightBarButtonItem setStyle:UIBarButtonItemStyleDone];
    }
}
- (UITableViewCellEditingStyle)tableView:(UITableView *)aTableView editingStyleForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (self.editing == NO || !indexPath)
        return UITableViewCellEditingStyleNone;
    
    if (self.editing && indexPath.section == 1)
        return UITableViewCellEditingStyleInsert;
    else
        return UITableViewCellEditingStyleDelete;
    
    return UITableViewCellEditingStyleNone;
}
- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle) editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (editingStyle == UITableViewCellEditingStyleDelete)
    {
        [dataArray removeObjectAtIndex:indexPath.row];
        [theTableView reloadData];
    }
    else if (editingStyle == UITableViewCellEditingStyleInsert)
        
    {
        NSLog(@"adding");
        UITableViewCell *theCell = [theTableView cellForRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:1]];
        UITextField *emailField = (UITextField *)[theCell viewWithTag:2];
        
        
        if ([emailField.text  isEqual: @""] || ![self validateEmail:emailField .text]){
            NSLog(@"here");
            //[self shakeIt:emailField];
            //[emailField becomeFirstResponder];
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Please enter a valid email address." delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
            [alert show];
            
        }
        else{
            NSLog(@"here adding in dataArray");
        [dataArray addObject:emailField.text];
        emailField.text = nil;
        }
        
        
       // [emailTextfield removeFromSuperview];

        [theTableView reloadData];
        
    }
}
-(void)shakeIt:(UIView *)viewToShake{
    CABasicAnimation *animation = [CABasicAnimation animationWithKeyPath:@"position"];
    [animation setDuration:0.07];
    [animation setRepeatCount:3];
    [animation setAutoreverses:YES];
    [animation setTimingFunction:[CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut]];
    [animation setFromValue:[NSValue valueWithCGPoint:
                             CGPointMake([viewToShake center].x - 5.0f, [viewToShake center].y)]];
    [animation setToValue:[NSValue valueWithCGPoint:
                           CGPointMake([viewToShake center].x + 5.0f, [viewToShake center].y)]];
    [viewToShake.layer addAnimation:animation forKey:@"key"];
}


- (BOOL)validateEmail:(NSString *)emailStr {
    
    NSLog(@"validating text : %@" , emailStr);

    NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emailStr];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
   
        [textField resignFirstResponder];
    NSLog(@"redigning");

    
    return YES;
}
@end
