//
//  PEPSHelpTableViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 28/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PEMenuSectionHeaderView.h"

@protocol settingsControllerDelegate <NSObject>
@optional
- (void)settingsSeePhotos:(NSInteger)seePhotos uploadPhotos:(NSInteger)uploadPhotos invite:(NSInteger)invite;
@end

@interface PEPSHelpTableViewController : UITableViewController<SectionHeaderViewDelegate>
@property (nonatomic ,assign) NSInteger type; //type 0: Help table - 1: Album settings 
@property (nonatomic ,strong) NSString *albumIdToEdit;
@property (nonatomic ,assign) NSInteger isEdit; //0: create , 1: edit
@property (nonatomic ,assign) NSInteger seeAlbumSelectedRow;
@property (nonatomic ,assign) NSInteger uploadPhotosSelectedRow;
@property (nonatomic ,assign) NSInteger inviteOthersSelectedRow;

@property (nonatomic, weak) id<settingsControllerDelegate> delegate;

@end
