//
//  PEPSPublicAlbumsViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 27/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSPublicAlbumsViewController.h"
#import "UIImageView+WebCache.h"
#import "PEPSViewAlbumViewController.h"
#import "AsyncImageView.h"

@interface PEPSPublicAlbumsViewController ()

@end

@implementation PEPSPublicAlbumsViewController{

    IBOutlet UICollectionView *albumsCollectionView;
    NSArray *returnedDataArray;

}



- (void)viewDidLoad {
    [super viewDidLoad];
       [self.navigationController setNavigationBarHidden:NO];
    [self.navigationController.navigationBar setBarTintColor:[UIColor colorWithRed:(48/255.0) green:(48/255.0) blue:(48/255.0) alpha:1]];
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    self.navigationItem.title = @ "PUBLIC ALBUMS" ;
    [self getPublicAlbums];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Collection View Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return returnedDataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
  
    
    NSString *albumName = [returnedDataArray objectAtIndex:indexPath.row][@"albumName"];

    NSString *coverPhotoURL = [returnedDataArray objectAtIndex:indexPath.row][@"coverPhoto"];
    
   // NSLog(@"COVER URL : %@",coverPhotoURL);
    
    NSString *invitesCount = [returnedDataArray objectAtIndex:indexPath.row][@"invitiesCount"];

    NSString *likesCount = [returnedDataArray objectAtIndex:indexPath.row][@"likesCount"];
    NSString *photosCount = [returnedDataArray objectAtIndex:indexPath.row][@"photosCount"];
    

    AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:1];
    
    
    
    AsyncImageView *bkImageView = (AsyncImageView *)[cell viewWithTag:6];
    UILabel *albumNameLabel  = (UILabel *)[cell viewWithTag:2];
    UILabel *invitesLabel = (UILabel *)[cell viewWithTag:3];
    UILabel *likesLabel = (UILabel *)[cell viewWithTag:4];
    UILabel *photosLabel = (UILabel *)[cell viewWithTag:5];
 
    NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",coverPhotoURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
    [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
    [bkImageView sd_setImageWithURL:theURL];
    
    /*
    imageView.image = [UIImage imageNamed:@"placeholderPahaar"];
    imageView.showActivityIndicator= YES;
    imageView.imageURL =[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]] ;
    bkImageView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]] ;
*/
    
   // [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]] placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
  //  [bkImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",coverPhotoURL]] placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];

 
    albumNameLabel.text = albumName;
    invitesLabel.text =  [ NSString stringWithFormat:@"%@ Invited" , invitesCount];

    likesLabel.text = likesCount;
    photosLabel.text = [ NSString stringWithFormat:@"%@ Photos" , photosCount];

    
    return cell;
}

- (BOOL)collectionView:(UICollectionView *)collectionView canMoveItemAtIndexPath:(NSIndexPath *)indexPath toIndexPath:(NSIndexPath *)toIndexPath
{
    
    
    
    return YES;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  //  NSLog(@"ndexPath.row : %ld" , (long)indexPath.row);
    NSString *albumIdToSend = [returnedDataArray objectAtIndex:indexPath.row][@"albumId"];
    NSString *albumName = [returnedDataArray objectAtIndex:indexPath.row][@"albumName"];
    NSString *coverPhotoURL = [returnedDataArray objectAtIndex:indexPath.row][@"coverPhoto"];
    NSString *userName = [returnedDataArray objectAtIndex:indexPath.row][@"from"];
    NSString *invitesCount = [returnedDataArray objectAtIndex:indexPath.row][@"invitiesCount"];
    NSString *likesCount = [returnedDataArray objectAtIndex:indexPath.row][@"likesCount"];
    NSString *photosCount = [returnedDataArray objectAtIndex:indexPath.row][@"photosCount"];
    NSString *albumDescription =[returnedDataArray objectAtIndex:indexPath.row][@"description"];
   // PEPSCarousalViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"carousalScene"];
    PEPSViewAlbumViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewAlbumScene"];
    vc.albumID = albumIdToSend;
    vc.albumName = albumName;
    vc.coverPhotoURL = coverPhotoURL;
    vc.userName = userName;
    vc.likesCount = likesCount;
    vc.photosCount = photosCount;
    vc.invitesCount = invitesCount;
    vc.albumDescription = albumDescription;
    vc.type=1;
    vc.isPublic=1;
   // vc.albumDescription = ;
   
    
    [collectionView deselectItemAtIndexPath:indexPath animated:YES];
    [self.navigationController pushViewController:vc animated:YES];
    
}

#pragma mark - CONNECTION



-(void)getPublicAlbums{
    
    [[webServices sharedServices] viewPublicAlbumsWithHandler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}


-(void)connectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    returnedDataArray = returnedDict[@"Data"];
   // NSLog(@"returnedDataArray : %@" , returnedDataArray);
    
    
   // NSInteger code = [returnedDict[@"code"] integerValue];
    
   // NSLog(@"returnedDict for PUBLIC : %@" , returnedDict);
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
 /*
    switch (1) {
        case 0:
            switch (code) {
                case 200:
                {
                    
                    
                    tabBarViewController *tabBarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarScene"];
                    //  tabBarViewController.userID = _userID;
                    [self presentViewController:tabBarViewController animated:YES completion:nil];
                    break;
                }
                    
                    break;
                    
                default:
                    break;
            }
    }
  */
    [albumsCollectionView reloadData];
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}



@end
