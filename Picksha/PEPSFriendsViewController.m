//
//  PEPSFriendsViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 16/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSFriendsViewController.h"
#import "CSAnimation.h"
#import "UIImageView+WebCache.h"
#import "PEPSProfileViewController.h"
#import "PEPSSearchViewController.h"
#import "AsyncImageView.h"
#import "PEPSViewAlbumViewController.h"

@interface PEPSFriendsViewController ()

@end

@implementation PEPSFriendsViewController{

    IBOutlet UIButton *showHideButton;
    IBOutlet UICollectionView *requestsCollectionView;

    IBOutlet UICollectionView *friendsCollectionView;
    IBOutlet NSLayoutConstraint *collectionViewHeightConstraint;
    IBOutlet UILabel *friendRequestsLabel;
    
    NSArray *friendRequestDataArray;
    NSArray *friendArray;
    NSArray *usersArray;
    BOOL toggleIsOn;
    NSString * myUserName;

  //  IBOutlet CSAnimationView *bkView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(handleFriendRequestChange:)
                                                 name:@"friendRequestCountChanged"
                                               object:nil];
    
    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = YES;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    
    showHideButton.hidden=YES;
    collectionViewHeightConstraint.constant = 0;
    [requestsCollectionView updateConstraints];
    [UIView animateWithDuration:0.0 animations:^(void){
        [requestsCollectionView layoutIfNeeded];
    }];
 
    myUserName =[[webServices sharedServices] myUser][@"username"];
    [self getFriendList];
    [self getFriendRequests];

}
- (void)handleFriendRequestChange:(NSNotification *)note {
    //NSLog(@"Getting FriendRequests in listener!");
    [self getFriendRequests];
    [self getFriendList];
    /*NSDictionary *theData = [note userInfo];
     if (theData != nil) {
     NSNumber *n = [theData objectForKey:@"isReachable"];
     BOOL isReachable = [n boolValue];
     NSLog(@"reachable: %d", isReachable);
     }
     */
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        [self getFriendList];

   
}
-(void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];


        
        // reseting tabbar badge value...
        

    

    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Collection View Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == friendsCollectionView) {
        return friendArray.count;
    }
    else
    return friendRequestDataArray.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    if (collectionView == requestsCollectionView) {
        friendRequestCollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
        cell.dataDictionary = friendRequestDataArray[indexPath.row];
        cell.delegate = self;//Dont delete this line Please
        [cell setupCell];
        return cell;
    } else  {
        UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
        
       // if (detailsOfAllTheFriendsArray.count) {
        
        AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:22];
        UILabel *name = (UILabel *)[cell viewWithTag:2];
        UILabel *albums = (UILabel *)[cell viewWithTag:3];
        UILabel *friends = (UILabel *)[cell viewWithTag:4];
        NSString *imageURL = friendArray[indexPath.row][@"imageUrl"];//[detailsOfAllTheFriendsArray objectAtIndex:indexPath.row][@"image"];
                 
        imageView.layer.cornerRadius = imageView.frame.size.height/2;
        imageView.layer.borderColor = [UIColor blackColor].CGColor;
        imageView.layer.borderWidth = 1.5;
        imageView.clipsToBounds = YES;
        
        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
        [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
        
        /*
        
        imageView.image=[UIImage imageNamed:@"personPlaceholder"];
        imageView.showActivityIndicator = YES;
         
        
        imageView.imageURL =[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] ;
         
         */
        
      //  [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
  
        name.text = friendArray[indexPath.row][@"name"];//[detailsOfAllTheFriendsArray objectAtIndex:indexPath.row][@"username"];
        albums.text = [friendArray objectAtIndex:indexPath.row][@"gallery_count"];
        friends.text = [friendArray objectAtIndex:indexPath.row][@"friend_count"];
        
        UIButton *tickButton = (UIButton *)[cell viewWithTag:1];
        UIButton *crossButton = (UIButton *)[cell viewWithTag:1];
        
        
        tickButton.hidden = YES;
        crossButton.hidden = YES;
        tickButton.enabled = NO;
        crossButton.enabled= NO;
          //  return cell;
       // }
        
        return cell;
        
    }
}

 
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    
    
    if (collectionView == requestsCollectionView) {
        friendRequestCollectionViewCell *friendRequestCell = (friendRequestCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [friendRequestCell showControls];
      
        //NSLog(@"should have changed tick button");

        
    }
    
    else{
       PEPSProfileViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileScene"];
        vc.type=1;
        vc.otherUserUsername=[friendArray objectAtIndex:indexPath.row][@"name"];
        vc.otherUserUserId=[friendArray objectAtIndex:indexPath.row][@"id"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}
-(void)collectionView:(UICollectionView *)collectionView didDeselectItemAtIndexPath:(NSIndexPath *)indexPath{
    if (collectionView == requestsCollectionView) {
        friendRequestCollectionViewCell *friendRequestCell = (friendRequestCollectionViewCell *)[collectionView cellForItemAtIndexPath:indexPath];
        [friendRequestCell hideControls];
        
        
    }
}


#pragma mark - CONNECTION


- (IBAction)showHideButtonOnTap:(id)sender {
    
    
    if(toggleIsOn) {
        collectionViewHeightConstraint.constant = 0;
        [requestsCollectionView updateConstraints];
        [UIView animateWithDuration:0.0 animations:^(void){
            [requestsCollectionView layoutIfNeeded];
        }];
        [showHideButton setSelected:YES];
    }
    else{

        collectionViewHeightConstraint.constant = 127.0;
        [requestsCollectionView updateConstraints];
        [UIView animateWithDuration:0.0 animations:^(void){
            [requestsCollectionView layoutIfNeeded];
        }];
        [showHideButton setSelected:NO];
        

    }
    toggleIsOn = !toggleIsOn;
    //[toggleButton setImage:[UIImage imageNamed:toggleIsOn ? @"heart-filled.png": @"Heart-WHITE_picsha.png" ] forState:UIControlStateNormal];
    
    }

-(void)getFriendRequests{
    
    [[webServices sharedServices] getFriendRequestsWithHandler:^(NSData *data){
        [self getFriendRequestsConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)getFriendRequestsConnectionGotData:(NSData *)data{
   
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //NSLog(@" returnedDict  for FriendRequests : %@", returnedDict );
    friendRequestDataArray = returnedDict[@"Data"];
    

    if (friendRequestDataArray.count == 0) {
        
        friendRequestsLabel.text = @"NO FRIEND REQUESTS";
        showHideButton.hidden = YES;
        showHideButton.enabled = NO;
        
        collectionViewHeightConstraint.constant = 0;
        [requestsCollectionView updateConstraints];
        [UIView animateWithDuration:2.0 animations:^(void){
            [requestsCollectionView layoutIfNeeded];
        }];
        [showHideButton setSelected:YES];
    }
    else{
        friendRequestsLabel.text = @"FRIEND REQUESTS";
        
        showHideButton.hidden = NO;
        showHideButton.enabled = YES;
        
        collectionViewHeightConstraint.constant = 127.0;
        [requestsCollectionView updateConstraints];
        [UIView animateWithDuration:2.0 animations:^(void){
            [requestsCollectionView layoutIfNeeded];
        }];
        [showHideButton setSelected:NO];
    }
   
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [requestsCollectionView reloadData];
}

-(void)getFriendList{
    [[webServices sharedServices] getFriendListWithHandler:^(NSData *data){
        [self getFriendListConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)getFriendListConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //NSLog(@"getFriendListConnectionGotData returnedDict for %@" , [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);

    if (returnedDict != nil) {
        friendArray = returnedDict[@"Data"];
    }
    [friendsCollectionView reloadData];
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];*/
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    alert.tag = 100;
    [alert show];
}

- (void)alertView:(UIAlertView *)alertView didDismissWithButtonIndex:(NSInteger)buttonIndex
{
    //NSLog(@"OK");
    [self getFriendList];
    [self getFriendRequests];
}
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
    
    if (buttonIndex == [alertView cancelButtonIndex]) {
       // NSLog(@"The cancel button was clicked from alertView");
        [self getFriendList];
        [self getFriendRequests];
    }
}


#pragma mark - Search Display Table View
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return usersArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    UITableViewCell *cell = [[UITableViewCell alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 66)];
   UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(15, 0, self.view.frame.size.width, 44)];

   // AsyncImageView *imageView = [[AsyncImageView alloc] initWithFrame:CGRectMake(0, 0, 50, 50)];
  //  NSString *imageUrl = [usersArray objectAtIndex:indexPath.row][@"imageUrl"];
   // NSLog(@"");
    
   // imageView.image =[UIImage imageNamed:@"personPlaceholder"];
  //  imageView.showActivityIndicator = YES;
   // imageView.imageURL =[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageUrl]] ;
    
   // [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageUrl]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];

     NSLog(@"[usersArray objectAtIndex:indexPath.row] : %@" , [usersArray objectAtIndex:indexPath.row]);
    
    if ([usersArray[indexPath.row][@"Cat Name"] isEqualToString:@"gallery"]) {
        NSString *labelTextToDisplay = [NSString stringWithFormat:@"%@ - %@ Photos",[usersArray objectAtIndex:indexPath.row][@"Name"],[usersArray objectAtIndex:indexPath.row][@"photosCount"]];
        label.text = labelTextToDisplay;
    }
    else{
    label.text = [usersArray objectAtIndex:indexPath.row][@"Name"];
    }
    //NSLog(@"label.text : %@" , label.text);
    [cell addSubview:label];
    return cell;
}
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
   // NSLog(@"selected : %ld" , (long)indexPath.row);
    
    if ([usersArray[indexPath.row][@"Cat Name"] isEqualToString:@"gallery"]) {
        PEPSViewAlbumViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"viewAlbumScene"];
        NSLog(@"FROM : %@" ,usersArray[indexPath.row][@"userName"] );
        if ([usersArray[indexPath.row][@"userName"] isEqualToString: myUserName]) {
            
            NSLog(@"Sending type 0 to View albums");
            vc.type = 0;
        }
        else {
            NSLog(@"Sending type 1 to View albums");
            vc.type = 1;
        }
        vc.albumID = usersArray[indexPath.row][@"galleryId"];
        vc.albumDescription = usersArray[indexPath.row][@"albumDescription"];
        vc.albumName = usersArray[indexPath.row][@"Name"];
        vc.coverPhotoURL = usersArray[indexPath.row][@"imageUrl"];
        vc.userName = usersArray[indexPath.row][@"userName"];
        vc.likesCount = usersArray[indexPath.row][@"likesCount"];
        vc.photosCount = usersArray[indexPath.row][@"photosCount"];
        vc.invitesCount = usersArray[indexPath.row][@"invitesCount"];
        
      //  NSLog(@"userId from profile to albums view: %@ " , userId );
      //  vc.userId = userId;
        [self.navigationController pushViewController:vc animated:YES];
    }

    else{
        PEPSProfileViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"profileScene"];
        vc.type=1;
        vc.otherUserUsername=[usersArray objectAtIndex:indexPath.row][@"Name"];
        vc.otherUserUserId=[usersArray objectAtIndex:indexPath.row][@"UserId"];
        [self.navigationController pushViewController:vc animated:YES];
    }
  
}

-(void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText{

    NSLog(@"changed TEXT : %@" , searchText);
    [[webServices sharedServices] getSearchWithString:searchText handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
   // [MBProgressHUD showHUDAddedTo:self.view animated:YES];

}
- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    // Do the search and show the results in tableview
    // Deactivate the UISearchBar
    
    // You'll probably want to do this on another thread
    // SomeService is just a dummy class representing some
    // api that you are using to do the search
    
    //NSLog(@"searchBar.text : %@", searchBar.text);
    
    [[webServices sharedServices] getSearchWithString:searchBar.text handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];

    
    //  NSArray *results = [SomeService doSearch:searchBar.text];
    
    //[self searchBar:searchBar activate:NO];
    
   
   
    
}
-(BOOL)searchDisplayController:(UISearchDisplayController *)controller shouldReloadTableForSearchString:(NSString *)searchString {

    // Return YES to cause the search result table view to be reloaded.
    return YES;
}

-(void)connectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    //NSLog(@"returnedDictionary for search results: %@" , returnedDictionary);
    
    usersArray = returnedDictionary[@"Users"];
    //NSLog(@"usersArray : %@" , usersArray);
    //  self.searchResult = returnedDictionary[@"Users"];
    
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [self.searchDisplayController.searchResultsTableView reloadData];
    
}
#pragma mark - friend Request Delegate

-(void)friendRequestAccepted{
    //NSLog(@"friendRequestAccepted");
    [self getFriendList];
    [self getFriendRequests];
}
-(void)friendRequestCancelled{
    
    //NSLog(@"friendRequestCencelled");
    [self getFriendList];
    [self getFriendRequests];
}

@end
