//
//  ConnectionManager.h
//  bringThings
//
//  Created by iOS Department-Pantera on 25/02/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//


#import <Foundation/Foundation.h>

@interface connectionManager : NSObject <NSURLConnectionDataDelegate>

-(void)startConnectionWithRequest:(NSURLRequest*)request handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

@end
