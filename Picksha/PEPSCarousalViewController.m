//
//  PEPSCarousalViewController.m
//  StoryboardExample
//
//  Created by Nick Lockwood on 08/06/2012.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "PEPSCarousalViewController.h"

@interface PEPSCarousalViewController ()

@property (nonatomic, strong) NSMutableArray *items;

@end


@implementation PEPSCarousalViewController{

    IBOutlet UIView *viewForImage;
    NSArray *returnedDataArray;

}

@synthesize carousel;
@synthesize items;

- (void)awakeFromNib
{
    //set up data
    //your carousel should always be driven by an array of
    //data of some kind - don't store data in your item views
    //or the recycling mechanism will destroy your data once
    //your item views move off-screen
    self.items = [NSMutableArray array];
    for (int i = 0; i < 1000; i++)
    {
        [items addObject:@(i)];
    }
}

- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    carousel.delegate = nil;
    carousel.dataSource = nil;
}

#pragma mark -
#pragma mark View lifecycle

- (void)viewDidLoad
{
    [super viewDidLoad];
    NSLog(@"A_ID : %@" , _albumID);
    [self getGalleryDetailswithAlbumID:_albumID];
    //configure carousel
    carousel.type = iCarouselTypeCoverFlow2;
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    
    //free up memory by releasing subviews
    self.carousel = nil;
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

#pragma mark -
#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(iCarousel *)carousel
{
    //return the total number of items in the carousel
    return [returnedDataArray count];
}

- (UIView *)carousel:(iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        CGRect screenRect = [[UIScreen mainScreen] bounds];
        CGFloat screenWidth = screenRect.size.width;
        CGFloat screenHeight = screenRect.size.height;
        
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, screenHeight/2, screenWidth-40.0)];
        ((UIImageView *)view).image = [UIImage imageNamed:@"car.jpg"];
        view.contentMode = UIViewContentModeScaleAspectFill;
        view.clipsToBounds = YES;
      
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50];
        label.tag = 1;
        [view addSubview:label];
        [viewForImage addSubview:view];
    }
    else
    {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = [items[index] stringValue];
    
    return view;
}


#pragma mark - CONNECTION

-(void)getGalleryDetailswithAlbumID:(NSString *)albumID{
    
    [[webServices sharedServices]getGalleryDetailWithAlbumId:albumID handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}


-(void)connectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    returnedDataArray = returnedDict[@"Data"];
    NSLog(@"returnedDataArray : %@" , returnedDataArray);
    
    
    // NSInteger code = [returnedDict[@"code"] integerValue];
    
    NSLog(@"returnedDict for PUBLIC : %@" , returnedDict);
    
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    // [hud setLabelText:responseDictionary[@"message"]];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
  /*
    switch (_type) {
        case 0:
            switch (code) {
                case 200:
                {
                    
                    
                    tabBarViewController *tabBarViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"tabBarScene"];
                    //  tabBarViewController.userID = _userID;
                    [self presentViewController:tabBarViewController animated:YES completion:nil];
                    break;
                }
                    
                    break;
                    
                default:
                    break;
            }
    }
 */
    [self.carousel reloadData];
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}




@end
