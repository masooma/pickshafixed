//
//  PEPSLoginSignupViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 21/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface PEPSLoginSignupViewController : UIViewController<FBLoginViewDelegate>
@property (nonatomic ,assign) NSInteger type;// type 0: LOGIN 1: SIGNUP


@end
