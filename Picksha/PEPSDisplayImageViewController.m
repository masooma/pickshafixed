//
//  PEPSDisplayImageViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 27/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSDisplayImageViewController.h"
#import "UIImageView+WebCache.h"

@interface PEPSDisplayImageViewController ()

@end

@implementation PEPSDisplayImageViewController{

    IBOutlet UIImageView *imageView;

    IBOutlet UIButton *cancelButton;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"type : %ld"  , (long)_type);
    
    if (_type==1) {
        [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",_imageURLToDisplay]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    }
    else if (_type==2) {
        NSLog(@"image : %@" , _imageToDisplay);
        imageView.image = _imageToDisplay;
    }
    else if (_type==3) {
        [imageView sd_setImageWithURL:_imageURL];
    }
    else{
        imageView.image =[UIImage imageNamed:@"chi-luong1"];
        cancelButton.enabled=NO;
        cancelButton.hidden=YES;
    }


    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)cancelButtonOnTap:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}
- (IBAction)saveButtonOnTap:(id)sender {
     UIImageWriteToSavedPhotosAlbum(imageView.image, nil, nil, nil);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Picture saved to Photos" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
