//
//  PEPSSettingsProfileTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 20/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSSettingsProfileTableViewController.h"
#import "PEPSLanguagesTableViewController.h"
#import "settingsFormViewController.h"
#import "PSTAlertController.h"
#import "UIImageView+WebCache.h"
#import "MZFormSheetController.h"

@interface PEPSSettingsProfileTableViewController ()

@end

@implementation PEPSSettingsProfileTableViewController{

    UIGestureRecognizer *tapRecognizer;
    IBOutlet UITextField *albumTitleField;
    IBOutlet UITextField *realNameField;
    IBOutlet UITextField *emailField;
    IBOutlet UIImageView *dpImageView;
    UIImagePickerController *ipc;
    UIPopoverController *popover;
    UIImage *imageToBeSentToServer;
    
    NSString *usernameToSend;
    NSString *realNameToSend;
    NSString *emailToSend;
    
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    NSString *myEmail;
    NSString *myRealName;
}





- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"Profile Settings" ;
   // NSLog(@"my user is *****************************************\n*****************\n*******: %@",[[webServices sharedServices] myUser]);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    myRealName=[[webServices sharedServices] myUser][@"realname"];
    myEmail=[[webServices sharedServices] myUser][@"email"];
    //NSLog(@"myUserName : %@ " , myUserName);
    //NSLog(@"myUserId : %@ " , myId);
    //NSLog(@"myImageURL : %@ " , myImageURL);
    //NSLog(@"myEmail : %@ " , myEmail);
    //NSLog(@"myRealName : %@ " , myRealName);
    
    //Rounding Image
    dpImageView.layer.cornerRadius = dpImageView.frame.size.height/2;
    dpImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    dpImageView.layer.borderWidth = 3.0;
    dpImageView.clipsToBounds = YES;
    
    //NSLog(@"myImageURL applying to dp : %@" , myImageURL);
    
    [dpImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[[webServices sharedServices] myUser][@"image"]]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    
    //For Hiding Keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
    [albumTitleField resignFirstResponder];
    [realNameField resignFirstResponder];
    [emailField resignFirstResponder];
}
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    
     //NSLog(@"myImageURL applying to dp : %@" , myImageURL);
    [dpImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",[[webServices sharedServices] myUser][@"image"]]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];

}

#pragma mark - Table view data source
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
#warning Potentially incomplete method implementation.
    // Return the number of sections.
    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
#warning Incomplete method implementation.
    // Return the number of rows in the section.
    return 0;
}
*/

 
 - (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

     
     settingsFormViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"formScene"];
     if (indexPath.section == 1) {
         vc.type = 4;
     }

     else if (indexPath.section == 3 && indexPath.row == 0) {
         vc.type = 1;
     }
     else if (indexPath.section == 3 && indexPath.row == 1) {
         vc.type = 2;
         NSLog(@"sending type 2 !");
     }
     
     MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
     
     if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
         formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-300, self.view.frame.size.height/4);
     } else {
         formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-30, self.view.frame.size.height/2);
     };
     formSheet.shadowRadius = 5.0;
     formSheet.shadowOpacity = 0.3;
     formSheet.shouldDismissOnBackgroundViewTap = YES;
     formSheet.shouldCenterVertically = YES;
     formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
     
     [[MZFormSheetController sharedBackgroundWindow] setBackgroundBlurEffect:YES];
     [[MZFormSheetController sharedBackgroundWindow] setBlurRadius:2.0];
     [[MZFormSheetController sharedBackgroundWindow] setBackgroundColor:[UIColor clearColor]];
     [formSheet setMovementWhenKeyboardAppears:MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard];
     
     
     formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromBottom;
     
     switch (indexPath.section) {
             
        case 0:
         {
             PSTAlertController *gotoPageController = [PSTAlertController alertWithTitle:nil message:nil];
             
             [gotoPageController addCancelActionWithHandler:NULL];
             
             [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Capture Photo" handler:^(PSTAlertAction *action) {
                // NSLog(@"capture photo here ! ");
                 
                 ipc = [[UIImagePickerController alloc] init];
                 ipc.delegate = self;
                 if([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera])
                 {
                     ipc.sourceType = UIImagePickerControllerSourceTypeCamera;
                     [self presentViewController:ipc animated:YES completion:NULL];
                 }
                 else
                 {
                     UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"No Camera Available." delegate:self cancelButtonTitle:@"Ok" otherButtonTitles:nil, nil];
                     [alert show];
                     alert = nil;
                 }

                 
             }]];
             
             [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Upload Photo" handler:^(PSTAlertAction *action) {
                 //NSLog(@"upload photo here ! ");
                 
                 if ([UIImagePickerController isSourceTypeAvailable: UIImagePickerControllerSourceTypePhotoLibrary])
                 {
                     UIImagePickerController *controller = [[UIImagePickerController alloc] init];
                     controller.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
                     controller.allowsEditing = NO;
                     controller.mediaTypes = [UIImagePickerController availableMediaTypesForSourceType:
                                              UIImagePickerControllerSourceTypePhotoLibrary];
                     controller.delegate = self;
                     [self.navigationController presentViewController: controller animated: YES completion: nil];
                 }

                 
             }]];
             
            [gotoPageController showWithSender:nil controller:self animated:YES completion:NULL];

         }
             break;
             
         case 1:{
             

             [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
                 
             }];
             
             break;
         
         }
        case 2:
             if (indexPath.row == 0) {
                 PEPSLanguagesTableViewController *vc2 = [self.storyboard instantiateViewControllerWithIdentifier:@"languageSettingsScene"];
                 [self.navigationController pushViewController:vc2 animated:YES];
             }
             break;
             
        case 3:
             if (indexPath.row == 0) {

                                 [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
                     
                 }];
                 

             }
             else if (indexPath.row == 1) {
                
                 [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
                     
                 }];
                 

             }
             break;

             
 
         default:
            break;
     }
 
 }
-(void)saveBasicInfo{
 
    //NSLog(@"usernameToSend : %@ ",usernameToSend);
   // NSLog(@"realNameToSend : %@ " , realNameToSend);
   // NSLog(@"emailToSend : %@ " , emailToSend);
    
}
#pragma mark - ImagePickerController Delegate

-(void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info
{
    if(UI_USER_INTERFACE_IDIOM()==UIUserInterfaceIdiomPhone) {
        [picker dismissViewControllerAnimated:YES completion:nil];
    } else {
        [popover dismissPopoverAnimated:YES];
    }
    
    
    imageToBeSentToServer = [info objectForKey:UIImagePickerControllerOriginalImage];
    
    float width,height;
    width = imageToBeSentToServer.size.width;
    height = imageToBeSentToServer.size.height;
    if (width>1024.0f && height>1024.0f) {
        if (width<height) {
            height = height/width*1024.0f;
            width=1024.0f;
        }else{
            width = width/height*1024.0f;
            height = 1024.0f;
        }
    }
    CGSize newSize = CGSizeMake(width, height);
    UIGraphicsBeginImageContext(newSize);
    
    [imageToBeSentToServer drawInRect:CGRectMake(0,0,newSize.width,newSize.height)];
    imageToBeSentToServer = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    [picker dismissViewControllerAnimated:YES completion:nil];
    [[webServices sharedServices]uploadProfilePictureithImage:imageToBeSentToServer withImageName:@""handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
}

-(void)imagePickerControllerDidCancel:(UIImagePickerController *)picker
{
    [picker dismissViewControllerAnimated:YES completion:nil];
}

-(void)connectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
   // NSLog(@"returnedDictionary for profile pic upload : %@" , returnedDictionary);
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    
    if ([returnedDictionary[@"code"] isEqualToString:@"200"]) {
        
        dpImageView.image = imageToBeSentToServer;
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Profile picture uploaded successfully."
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [[webServices sharedServices] updateMyUser];
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@""
                                                        message:@"Profile picture could not be uploaded.\nPlease try again later."
                                                       delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
    }
    
    
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}



@end
