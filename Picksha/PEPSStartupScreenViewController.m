//
//  PEPSStartupScreenViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 21/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSStartupScreenViewController.h"
#include "PEPSLoginSignupViewController.h"

@interface PEPSStartupScreenViewController ()

@end

@implementation PEPSStartupScreenViewController{

}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self.navigationController setNavigationBarHidden:YES];
    NSLog(@"IMAGE OUTPUT : %@",[UIImage imageNamed:@"up.png"]);
  
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}
- (IBAction)loginOnTap:(id)sender {
    PEPSLoginSignupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupScene"];
    vc.type=1;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)signupOnTap:(id)sender {
    PEPSLoginSignupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupScene"];
    vc.type=2;
    [self.navigationController pushViewController:vc animated:YES];
}
- (IBAction)publicAlbumsOnTap:(id)sender {
}



@end
