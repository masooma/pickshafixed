//
//  commentBoxViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 20/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "commentBoxViewController.h"
#import "UIImageView+WebCache.h"

@interface commentBoxViewController ()

@end

@implementation commentBoxViewController{

    IBOutlet UIView *bottomCommentView;
    IBOutlet UILabel *peopleLikeThisPhotoLabel;
    IBOutlet UILabel *photoLikesLabel;
    IBOutlet UITableView *commentsTableView;
    
    IBOutlet NSLayoutConstraint *postCommentButton;
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    
    NSArray *imageDetailsArray;

    IBOutlet UITextField *commentTextfield;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    if (_isOutsidePublic ==1) {
         NSLog(@" OUTSIDE PUBLIC 1 : %ld" , (long)_isOutsidePublic);
        bottomCommentView.hidden = YES;
        bottomCommentView.userInteractionEnabled = NO;
    }
   
    
    commentTextfield.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Write a comment..." attributes:@{NSForegroundColorAttributeName: [UIColor whiteColor]}];
    
    //NSLog(@"my user is: %@",[[webServices sharedServices] myUser]);
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    //NSLog(@"myUserName : %@ " , myUserName);
    //NSLog(@"myUserId : %@ " , myId);
    //NSLog(@"myImageURL : %@" , myImageURL);
    
    //NSLog(@"imageId : %@" , _imageId);
   // photoLikesLabel.text = _imageLikeCount
    [self getImageDetails];

}

-(void)getImageDetails{
    [[webServices sharedServices]getImageDetailWithUserId:myId objectId:_imageId handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)postCommentButtonOnTap:(id)sender {
    
     [commentTextfield resignFirstResponder];
    int commentNumber = imageDetailsArray.count+1;
    NSString* commentNumberString = [NSString stringWithFormat:@"%i", commentNumber];

    if (![commentTextfield.text  isEqual: @""]) {
        NSCharacterSet *set = [NSCharacterSet whitespaceCharacterSet];
        if (![[commentTextfield.text stringByTrimmingCharactersInSet: set] length] == 0)
        {

        [[webServices sharedServices] saveCommentForObjectId:_imageId text:commentTextfield.text commentNumber:commentNumberString handler:^(NSData *data){
            [self saveCommentConnectionGotData:data];
        } errorHandler:^(NSError *error) {}];
        
    }
    }
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    // Return the number of rows in the section.
    return imageDetailsArray.count;
    
}




- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    commentTableViewCell *cell= (commentTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"messageCell" forIndexPath:indexPath];

    [cell setDataDictionaryforCell:imageDetailsArray[indexPath.row]];
    //cell.dataDictionaryforCell = ;
    cell.delegate = self;
    [cell setupCell];
    return cell;
}
/*
-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if([indexPath row] == ((NSIndexPath*)[[tableView indexPathsForVisibleRows] lastObject]).row){
        NSUInteger finalRow = MAX(0, [commentsTableView numberOfRowsInSection:0] - 1);
        NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
        
        if ([commentsTableView numberOfRowsInSection:0] !=0) {
            [commentsTableView scrollToRowAtIndexPath:finalIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:NO];
        }
    }
}
 */

- (NSDate *)convert:(NSString *)strdate
{
    NSDateFormatter * formatter = [[NSDateFormatter alloc] init];
    [formatter setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [formatter setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    NSDate * date = [formatter dateFromString:strdate];
    
    return date;
    
}

- (NSString *)timeToShowForDate:(NSDate *)date{
    NSDate *currentDate = [NSDate date];
    NSString *dateFormat;
    
    //NSTimeInterval timePassed = [currentDate timeIntervalSinceDate:date];
    //NSInteger hoursPassed = timePassed/3600;
    NSCalendar *gregorianCalendar = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [gregorianCalendar components:NSDayCalendarUnit
                                                        fromDate:date
                                                          toDate:currentDate
                                                         options:0];
    NSInteger daysPassed = [components day];
    if (daysPassed<1) {
        dateFormat = @"HH:mm a";
    }else if(daysPassed<2){
        return @"Yesterday";
    }else if (daysPassed<5){
        dateFormat = @"EEE";
    }else{
        dateFormat = @"MMM dd";
    }
    NSDateFormatter * df = [[NSDateFormatter alloc] init];
    [df setTimeZone:[NSTimeZone timeZoneForSecondsFromGMT:0]];
    [df setLocale:[NSLocale localeWithLocaleIdentifier:@"en_US_POSIX"]];
    [df setDateFormat:dateFormat];
    NSString * depResult = [df stringFromDate:date];
    
    return depResult;
}

-(void)sendCommentsAlerts{

    [[webServices sharedServices] commentAlertWithImageId:_imageId handler:^(NSData *data){  } errorHandler:^(NSError *error) {}];
    [[webServices sharedServices] commentOnCommentedAlertWithImageId:_imageId handler:^(NSData *data){
    } errorHandler:^(NSError *error) {}];

}

#pragma -mark Connection

-(void)connectionGotData:(NSData *)data{
     NSLog(@"connection got data for comment: %@",[[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]);
    NSDictionary *returnedDict= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
   // conversations = returnedDict[@"Data"];
    photoLikesLabel.text = returnedDict[@"imageLikeCount"];
   // NSLog(@"returnedDict for image Details: : %@" , returnedDict);
    
    NSString *photosCountText = returnedDict[@"imageLikeCount"];


    
 
    if ([photosCountText isKindOfClass:[NSNull class]]) {
        NSLog(@"***** NULL ****");
        photosCountText = @"0";
    }

    
    if ([photosCountText isEqualToString:@"0"]) {
        [photoLikesLabel performSelectorOnMainThread:@selector(setText:) withObject:@" " waitUntilDone:NO];
        [peopleLikeThisPhotoLabel performSelectorOnMainThread:@selector(setText:) withObject:@"No one likes this photo" waitUntilDone:NO];
    
    }
    else if ([photosCountText isEqualToString:@"1"]) {
        [photoLikesLabel performSelectorOnMainThread:@selector(setText:) withObject:@"1" waitUntilDone:NO];
        [peopleLikeThisPhotoLabel performSelectorOnMainThread:@selector(setText:) withObject:@"person likes this photo" waitUntilDone:NO];
        
    }
    
    else
    {
    
    [photoLikesLabel performSelectorOnMainThread:@selector(setText:) withObject:photosCountText waitUntilDone:NO];
    [peopleLikeThisPhotoLabel performSelectorOnMainThread:@selector(setText:) withObject:@"people like this photo" waitUntilDone:NO];
    }
    
    imageDetailsArray = returnedDict[@"Data"];

    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [commentsTableView reloadData];
}
-(void)saveCommentConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict= [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    // conversations = returnedDict[@"Data"];
   // NSLog(@"returnedDict for image Details: : %@" , returnedDict);
    if ([returnedDict[@"code"] integerValue ]== 200) {
        commentTextfield.text=@"";
        [self getImageDetails];
        [self sendCommentsAlerts];
        [commentsTableView reloadData];
     
        NSUInteger finalRow = MAX(0, [commentsTableView numberOfRowsInSection:0] - 1);
        NSIndexPath *finalIndexPath = [NSIndexPath indexPathForItem:finalRow inSection:0];
    
        if ([commentsTableView numberOfRowsInSection:0] !=0) {
              [commentsTableView scrollToRowAtIndexPath:finalIndexPath atScrollPosition:UITableViewScrollPositionBottom animated:YES];
        }
      
      
     
    }
    

    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];

}


-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}
-(BOOL)prefersStatusBarHidden{
    return YES;
}

@end
