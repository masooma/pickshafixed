//
//  friendRequestCollectionViewCell.m
//  Picksha
//
//  Created by iOS Department-Pantera on 07/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "friendRequestCollectionViewCell.h"
#import "UIImageView+WebCache.h"
#import "webServices.h"
#import "MBProgressHUD.h"

@implementation friendRequestCollectionViewCell{
    
    IBOutlet UIImageView *imageView;
    IBOutlet UILabel *name;
    NSString *to;
    NSString *from;
    NSString *idOfUser;
}


-(void)setupCell{
    //UIImageView *imageView = (UIImageView *)[cell viewWithTag:32];
    
    [self hideControls];
    
    
    //   [imageView setImageWithURL:theItem.thumbURL placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    imageView.layer.cornerRadius = imageView.frame.size.height/2;
    imageView.layer.borderColor = [UIColor blackColor].CGColor;
    imageView.layer.borderWidth = 1.5;
    imageView.clipsToBounds = YES;
    
    name.text=_dataDictionary[@"name"];
    NSString *imageURL =_dataDictionary[@"profileImageUrl"];
    [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];
    [_tickButton addTarget:self action:@selector(tickButtonOnTap:) forControlEvents:UIControlEventTouchUpInside];
}

- (IBAction)tickButtonOnTap:(id)sender {
    NSLog(@"tick button tapped !!");
    to=_dataDictionary[@"to"];
    from=_dataDictionary[@"from"];
    idOfUser =_dataDictionary[@"id"];
    

    [[webServices sharedServices]acceptFriendRequestWithId:idOfUser from:from handler:^(NSData *data){
        [self acceptConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];

       [MBProgressHUD showHUDAddedTo:self.contentView animated:YES];
}

- (IBAction)crossButtonOnTap:(id)sender {
    to=_dataDictionary[@"to"];
    from=_dataDictionary[@"from"];
    idOfUser =_dataDictionary[@"id"];
    [[webServices sharedServices]cancelFriendRequestWithId:idOfUser handler:^(NSData *data){
        [self cancelConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.contentView animated:YES];
}
-(void)showControls{
    NSLog(@"toggle ON");
    if (_tickButton.alpha==1) {
        [self hideControls];
        return;
    }
    [UIView animateWithDuration:0.5 animations:^(void){
        
        _tickButton.alpha=1;
        _crossButton.alpha = 1;
        _tickButton.enabled = 1;
        _crossButton.enabled = 1;
        
    }completion:^(BOOL abc){
        
    }];

}
-(void)hideControls{
    NSLog(@"toggle OFF");
    [UIView animateWithDuration:0.5 animations:^(void){
        
        _tickButton.alpha=0;
        _crossButton.alpha = 0;
        _tickButton.enabled = 0;
        _crossButton.enabled = 0;
        
    }completion:^(BOOL abc){
        
    }];
    
}
#pragma mark - connection

-(void)acceptConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@" returnedDict  for acceptConnectionGotData : %@", [[NSString alloc] initWithData:data encoding:NSUTF8StringEncoding]/*returnedDict*/ );
    if ([returnedDict[@"code"] isEqualToString:@"200"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Friend Request accepted" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
        if (self.delegate) {
            NSLog(@"delegate method for accpet connection");
            [self.delegate friendRequestAccepted];
        }
        
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:returnedDict[@"message"] delegate:self cancelButtonTitle:@" OK" otherButtonTitles:nil, nil];
        [alert show];
    }
       [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}
-(void)cancelConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSLog(@" returnedDict  for FriendRequests : %@", returnedDict );
    if ([returnedDict[@"code"] integerValue]==200) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Friend Request declined" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       if (self.delegate) {
            [self.delegate friendRequestCancelled];
        }
       
    }
    else{
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Sorry! The request could not be declined" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
}
-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.contentView animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}


@end
