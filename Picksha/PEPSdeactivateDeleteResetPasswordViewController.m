
//  PEPSdeactivateDeleteResetPasswordViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 20/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSdeactivateDeleteResetPasswordViewController.h"


@interface PEPSdeactivateDeleteResetPasswordViewController ()

@end

@implementation PEPSdeactivateDeleteResetPasswordViewController
{

    IBOutlet UIButton *theButton;
    UIGestureRecognizer *tapRecognizer;
    
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;
    NSString *myEmail;
    NSString *myRealName;

    NSString *usernameToSend;
    NSString *realNameToSend;
    NSString *emailToSend;

}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    myRealName=[[webServices sharedServices] myUser][@"realname"];
    myEmail=[[webServices sharedServices] myUser][@"email"];
    //NSLog(@"myUserName : %@ " , myUserName);
    //NSLog(@"myUserId : %@ " , myId);
    //NSLog(@"myImageURL : %@ " , myImageURL);
    //NSLog(@"myEmail : %@ " , myEmail);
    //NSLog(@"myRealName : %@ " , myRealName);
    
    switch (_type) {
        case 1:
            [theButton setTitle:@"Deactivate Account" forState:UIControlStateNormal];
            break;
        case 2:
            [theButton setTitle:@"Delete Account" forState:UIControlStateNormal];
          
            break;
        case 3:
            [theButton setTitle:@"Change Password" forState:UIControlStateNormal];
                        break;
        case 4:
            [theButton setTitle:@"Save Changes" forState:UIControlStateNormal];
            break;
            
        default:
            break;
    }
    //For Hiding Keyboard
    NSNotificationCenter *nc = [NSNotificationCenter defaultCenter];
    [nc addObserver:self selector:@selector(keyboardWillShow:) name:
     UIKeyboardWillShowNotification object:nil];
    [nc addObserver:self selector:@selector(keyboardWillHide:) name:
     UIKeyboardWillHideNotification object:nil];
    tapRecognizer = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(didTapAnywhere:)];
}

-(void)didTapAnywhere: (UITapGestureRecognizer*) recognizer {
   /* [albumTitleField resignFirstResponder];
    [realNameField resignFirstResponder];
    [emailField resignFirstResponder];
    */
}
-(void) keyboardWillShow:(NSNotification *) note {
    [self.view addGestureRecognizer:tapRecognizer];
}

-(void) keyboardWillHide:(NSNotification *) note
{
    [self.view removeGestureRecognizer:tapRecognizer];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
  
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if ((_type == 3) || (_type == 4)) {
        return 3;
    }
    else
        return 2;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier" forIndexPath:indexPath];
    UITextField *thetextField = (UITextField *)[cell viewWithTag:1];
    

    if (_type == 3) {
        if (indexPath.row == 0) {
            thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Current Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        }
        else if (indexPath.row == 1) {
            thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"New Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        }
        else if (indexPath.row == 2) {
            thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
        }
    }
    else if (_type == 4) {
        if (indexPath.row == 0) {
            thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Username" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
            thetextField.text = myUserName;
        }
        else if (indexPath.row == 1) {
            thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Real Name" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
            thetextField.text = myRealName;
        }
        else if (indexPath.row == 2) {
            thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Email" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
            thetextField.text = myEmail;
        }
    }
    
    else
    
    {
            if (indexPath.row == 0) {
                thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Enter Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
            }
            else if (indexPath.row == 1) {
                thetextField.attributedPlaceholder = [[NSAttributedString alloc] initWithString:@"Confirm Password" attributes:@{NSForegroundColorAttributeName: [UIColor grayColor]}];
            }
    }

    return  cell;
}
- (IBAction)deactivateDeleteAccountOnTap:(id)sender {
    switch (_type) {
        case 1:
            NSLog(@"Deactivating Account ! ");
            
            break;
        case 2:
            NSLog(@"Deleting Account ! ");
            
            break;
        case 3:
            NSLog(@"Changing Pwd ! ");
            
            break;
        case 4:
            NSLog(@"saving info details ! ");
            
            break;
            
            
        default:
            break;
    }
}


@end
