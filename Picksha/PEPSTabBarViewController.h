//
//  PEPSTabBarViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 21/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEPSTabBarViewController : UITabBarController

@end
