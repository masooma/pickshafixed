//
//  commentBoxViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 20/05/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "commentTableViewCell.h"

@interface commentBoxViewController : UIViewController<UITableViewDataSource , UITableViewDelegate , commentTableViewCellDelegate >
@property(nonatomic , strong) NSString *imageId;
@property(nonatomic , assign)NSInteger isOutsidePublic;
//@property(nonatomic , strong) NSString *imageLikeCount;

@end
