//
//  PEPSMainSettingsViewTableController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 17/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSMainSettingsTableViewController.h"
#import "PEPSSettingsProfileTableViewController.h"
#import "PEPSAlertsTableViewController.h"
#import "PEPSdeactivateDeleteResetPasswordViewController.h"
#import "PEPSLoginSignupViewController.h"
#import "settingsFormViewController.h"
#import "MZFormSheetController.h"


@interface PEPSMainSettingsTableViewController ()

@end

@implementation PEPSMainSettingsTableViewController{
    BOOL isFb;

}

- (void)viewDidLoad {
    [super viewDidLoad];
    isFb =[[[webServices sharedServices] myUser][@"fb"] boolValue];
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                                                                      }];
    self.navigationItem.title = @"Settings" ;
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Table View
/*
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 2;
}
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return 1;
}
 */
/*
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{

      UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"cellIdentifier" forIndexPath:indexPath];
 
    UILabel *theLabel = (UILabel *)[cell viewWithTag:1];
    if (indexPath.section==0) {
        switch (indexPath.row) {
            case 0:
                theLabel.text =@"Profile";
                break;
            case 1:
                theLabel.text =@"Alerts";
                break;
            case 2:
                theLabel.text =@"Password";
                break;
            case 3:
                theLabel.text =@"About Us";
                break;

            default:
                break;
        }
    }

    return cell;
}
*/


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
  if (isFb) {
      if (section ==0) {
          return 2;
      }
      else
          return 1;
    }
    
    else
        if (section ==0) {
            return 3;
        }
        else
            return 1;
    
}


- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
  //  [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
   //  PEPSMainSettingsTableViewController *vc;
    switch (indexPath.section) {
        case 0:
            if (indexPath.row == 0) {
                  PEPSMainSettingsTableViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"settingsProfileScene"];
                 [self.navigationController pushViewController:vc animated:YES];
            }
            else if (indexPath.row == 1) {
                PEPSAlertsTableViewController  *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"alertsSettingsScene"];
                [self.navigationController pushViewController:vc animated:YES];
            }
            else if (indexPath.row == 2) {
                
                
                settingsFormViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"formScene"];
                
                    vc.type = 3;
                
                MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
                
                if([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad) {
                    formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-300, self.view.frame.size.height/4);
                } else {
                    formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-30, self.view.frame.size.height/2);
                }
                
                formSheet.shadowRadius = 5.0;
                formSheet.shadowOpacity = 0.3;
                formSheet.shouldDismissOnBackgroundViewTap = YES;
                formSheet.shouldCenterVertically = YES;
                formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
                
                [[MZFormSheetController sharedBackgroundWindow] setBackgroundBlurEffect:YES];
                [[MZFormSheetController sharedBackgroundWindow] setBlurRadius:2.0];
                [[MZFormSheetController sharedBackgroundWindow] setBackgroundColor:[UIColor clearColor]];
                [formSheet setMovementWhenKeyboardAppears:MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard];
                
                
                formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromBottom;
                [self mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController)
                {
                    
                }];

            }
            break;
        case 2:
        {
            [[webServices sharedServices] logoutUserWithHandler:^(NSData *data){
                [self logoutConnectionGotData:data];
                [FBSession.activeSession closeAndClearTokenInformation];
                
                
                
            }errorHandler:^(NSError *error){
                //[self connectionFailedWithError:error];
            }];
            
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
            break;
        default:
            break;
    }
  
    
    
    
}

-(void)logoutConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
   // NSLog(@"logoutConnectionGotData: returnedDictionary : %@" , returnedDictionary);
    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    if ([returnedDictionary[@"code"] isEqualToString:@"200"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Logout Successful!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
       /* PEPSLoginSignupViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"loginSignupScene"];
        
        [self presentViewController:vc animated:YES completion:nil];*/
        [self.tabBarController dismissViewControllerAnimated:YES completion:nil]; 
    }
    else if ([returnedDictionary[@"code"] isEqualToString:@"300"]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:@"Could not Logout.\nPlease try again later!" delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
        [alert show];
    }
    
}

-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
@end
