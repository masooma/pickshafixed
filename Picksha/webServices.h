//
//  WebServicesViewController.h
//  bringThings
//
//  Created by iOS Department-Pantera on 25/02/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//



#import <Foundation/Foundation.h>

@interface webServices : NSObject
extern NSString * const baseURL;

+(webServices *)sharedServices;
@property (nonatomic, strong) NSDictionary *myUser;
-(void)updateMyUser;

#pragma - mark Login Register Methods
-(void)attemptSignUpUserWithEmail:(NSString *)email password:(NSString *)password username:(NSString *)username handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)attemptLoginWithUsername:(NSString *)username password:(NSString *)password handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)attemptFacebookLoginWithfacebookId:(NSString *)facebookId email:(NSString *)email handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)logoutUserWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)checkIfUserLoggedInWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

#pragma - mark Create/Edit/Delete Album
-(void)uploadImagesTemporarilywithImage:(UIImage *)image withImageName:(NSString *)imageName aboutPhoto:(NSString *)aboutPhoto cover:(NSString *)cover handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)createGalleryWithAlbumname:(NSString *)albumName description:(NSString *)description seeAlbum:(NSString *)seeAlbum uploadPhotos:(NSString *)uploadPhotos inviteOthers:(NSString *)inviteOthers handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)uploadGalleryWithAlbumname:(NSString *)albumName description:(NSString *)description seeAlbum:(NSString *)seeAlbum uploadPhotos:(NSString *)uploadPhotos inviteOthers:(NSString *)inviteOthers handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deleteGalleryWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)updateGalleryWithAlbumId:(NSString *)albumId name:(NSString *)albumName description:(NSString *)description seeAlbum:(NSString *)seeAlbum uploadPhotos:(NSString *)uploadPhotos inviteOthers:(NSString *)inviteOthers handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getAlbumSettingsWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)inviteFriendToAlbumId:(NSString *)albumId senderId:(NSString *)senderId receiverId:(NSString *)receiverId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)inviteFriendWithEmail:(NSString *)email galleryId:(NSString *)galleryId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getInvitedFriendsForAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)likeObjectType:(NSString *)objectType userId:(NSString *)userId objectId:(NSString *)objectId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)unLikeObjectType:(NSString *)objectType userId:(NSString *)userId objectId:(NSString *)objectId likeId:(NSString *)likeId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)editGallerywithImage:(UIImage *)image userUploadingUsername:(NSString *)username withGalleryId:(NSString *)gallery_id cover:(NSString *)cover handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deleteTemporaryPicturesUploadedByUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getImageDetailWithUserId:(NSString *)userId objectId:(NSString *)objectId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)saveCommentForObjectId:(NSString *)objectId text:(NSString *)text commentNumber:(NSString *)commentNumber handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deletePhotoByUsername:(NSString *)username galleryId:(NSString *)galleryId objectId:(NSString *)objectId deleteAll:(NSString *)deleteAll handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)makeCoverPhotoWithImageId:(NSString *)imageId galleryId:(NSString *)galleryId  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

#pragma - mark View Albums
-(void)viewPublicAlbumsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)viewOutsidePublicAlbumsWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getGalleryDetailWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)viewFriendsAlbumsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)viewMyAlbumsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getAlbumsForUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

#pragma - mark ProfileInfo
-(void)getAdminProfileImageAndUsernameWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getProfileInfoWithUsername:(NSString *)username handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getProfileInfoWithUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

#pragma - mark Notifications and Messages
-(void)getNotificationsFriendRequestsAndMessagesCountWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getNotificationsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getMessagesListWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)sendMessageTo:(NSString *)to from:(NSString *)from textMessage:(NSString *)textMessage handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getMessagesForSpecificUserWithId:(NSString *)specificUserId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deleteConversationWithUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)sendMessagewithImage:(UIImage *)image toUserId:(NSString *)to  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

#pragma - mark Friends
-(void)getFriendRequestsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getFriendListWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)acceptFriendRequestWithId:(NSString *)requestId from:(NSString *)from  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)cancelFriendRequestWithId:(NSString *)requestId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)sendFriendRequestTo:(NSString *)to from:(NSString *)from handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)isFriendWithUserWithId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deleteFriendWithUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

#pragma - mark SEARCH

-(void)getSearchWithString:(NSString *)string handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;


#pragma - mark Account Settings
-(void)uploadProfilePictureithImage:(UIImage *)image withImageName:(NSString *)imageName  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)updateUsername:(NSString *)username realName:(NSString *)realName  email:(NSString *)email handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)forgotPasswordEnterEmail:(NSString *)email handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)changePasswordWithNewPassword:(NSString *)newPassword oldPassword:(NSString *)oldPassword handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deactivateAccountWithPassword:(NSString *)password handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)deleteAccountWithPassword:(NSString *)password handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)getAlertsSettingsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)postAlertsSettingsIfGetNewFriendRequest:(NSString *)friendRequest confirmRequest:(NSString *)confirmRequest confirmInvitation:(NSString *)confirmInvitation getInvitation:(NSString *)getInvitation commentInMyGallery:(NSString *)commentInMyGallery commentWhereICommented:(NSString *)commentWhereICommented gotMessage:(NSString *)gotMessage handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)commentOnCommentedAlertWithImageId:(NSString *)imageId   handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)commentAlertWithImageId:(NSString *)imageId   handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;
-(void)messageAlertWithToUserId:(NSString *)userId messageText:(NSString *)messageText handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler;

@end
