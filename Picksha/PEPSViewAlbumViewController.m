//
//  PEPSViewAlbumViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 28/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSViewAlbumViewController.h"
#import "UIImageView+WebCache.h"
#import "PEPSCreateEditTableViewController.h"
#import "PEPSInviteFriendsViewController.h"
#import "MZFormSheetController.h"
#import "commentBoxViewController.h"
#import "PSTAlertController.h"
#import "AsyncImageView.h"
#import "PSTAlertController.h"
#import "emailInviteViewController.h"


@interface PEPSViewAlbumViewController ()

@end

@implementation PEPSViewAlbumViewController{
    NSString *imageIdToSend;
    
    NSArray *returnedDataArray;
    IBOutlet UICollectionView *photosCollectionView;
    IBOutlet UIImageView *profileImageView;
    IBOutlet UILabel *userNameLabel;
    IBOutlet UILabel *albumDescriptionLabel;
    IBOutlet UILabel *numberOfPhotosLabel;
    IBOutlet UILabel *numberOfLikesLabel;
    IBOutlet UIImageView *coverPhotoImageView;
    IBOutlet UIButton *toggleButton;
    BOOL toggleIsOn;
    NSString *userIdToSendToLikeOrUnlike;
    
    NSDictionary *galleryDetailsDict;
    NSDictionary *likeDict;
    
    NSString *likeId;
    NSString *isLike;
    
    NSString *canAddPhotos;
    NSString *canDeletePhotos;
    NSString *canInviteFriends;
    
    NSString * myId;
    NSString * myUserName;
    NSString *myImageURL;

    IBOutlet UIButton *settingsButton;
    
    UIRefreshControl *refreshControl;
    



}


- (void)viewDidLoad {
    [super viewDidLoad];
    NSLog(@"VIEW DID LOAD album view!");
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(refreshView:) name:@"refreshAlbum" object:nil];

    _coverPhotoURL = nil;
    myUserName =[[webServices sharedServices] myUser][@"username"];
    myId =[[webServices sharedServices] myUser][@"id"];
    myImageURL =[[webServices sharedServices] myUser][@"image"];
    
    if ([self.navigationController respondsToSelector:@selector(interactivePopGestureRecognizer)]) {
        self.navigationController.interactivePopGestureRecognizer.enabled = NO;
    }

    [self.navigationController.navigationBar setBackgroundImage:[UIImage new]
                                                  forBarMetrics:UIBarMetricsDefault];
    self.navigationController.navigationBar.shadowImage = [UIImage new];
    self.navigationController.navigationBar.translucent = NO;
    self.navigationController.view.backgroundColor = [UIColor clearColor];
    self.navigationController.navigationBar.backgroundColor = [UIColor clearColor];
    UIColor *aColor =[UIColor colorWithRed:45.0f/255.0f green:45.0f/255.0f blue:45.0f/255.0f alpha:1.0f];
    self.navigationController.navigationBar.barTintColor = aColor;
    [self.navigationController.navigationBar setTitleTextAttributes:@{
                                                                      NSForegroundColorAttributeName: [UIColor whiteColor],
                                                                      NSFontAttributeName: [UIFont fontWithName:@"HelveticaNeue-Light" size:24.0],
                      
                                                                      }];
    self.navigationItem.title = _albumName ;
    albumDescriptionLabel.text = _albumDescription;
    userNameLabel.text = _userName;
    numberOfLikesLabel.text = _likesCount;
    
    profileImageView.layer.cornerRadius = profileImageView.frame.size.height/2;
    profileImageView.layer.borderColor = [UIColor whiteColor].CGColor;
    profileImageView.layer.borderWidth = 2.0;
    profileImageView.clipsToBounds = YES;
    
    [self setUpRefreshControl];

    [[webServices sharedServices]getProfileInfoWithUsername:_userName handler:^(NSData *data){
        [self getProfileInfoConnectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    /*
    if (_isPublic == 0) {
        
            [self getGalleryDetailswithAlbumID:_albumID];
    }
     */
    //else if (_isPublic == 1) {
    if (_isPublic == 1) {
        
        settingsButton.enabled = NO;
        settingsButton.hidden = YES;
        toggleButton.enabled=NO;
        [[webServices sharedServices] viewOutsidePublicAlbumsWithAlbumId:_albumID handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
    if (_isPublic == 0) {
        
        [self getGalleryDetailswithAlbumID:_albumID];
    }
}

-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
        NSLog(@"VIEW DID APPEAR album view!");
    self.navigationController.navigationBarHidden = NO;
   /* if (_isPublic == 0) {
        
        [self getGalleryDetailswithAlbumID:_albumID];
    }
    */
    

    if (_isPublic == 1) {
        
        settingsButton.enabled = NO;
        settingsButton.hidden = YES;
    }

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)refreshView:(NSNotification*)notification
{
    [self getGalleryDetailswithAlbumID:_albumID];

}

-(void)setUpRefreshControl{
    
    refreshControl = [[UIRefreshControl alloc] init];
    refreshControl.tintColor = [UIColor grayColor];
    [refreshControl addTarget:self action:@selector(refreshAlbums:) forControlEvents:UIControlEventValueChanged];
    [photosCollectionView addSubview:refreshControl];
    photosCollectionView.alwaysBounceVertical = YES;
    
    [self showPullToRefreshMessage:NO];
}
- (IBAction)refreshAlbums:(id)sender {
    
    NSLog(@"REFFRESH ALBUMS !");
    
    [self showPullToRefreshMessage:NO];
    if (_isPublic == 0) {
        
        [self getGalleryDetailswithAlbumID:_albumID];
    }
    else if (_isPublic == 1) {
        
        settingsButton.enabled = NO;
        settingsButton.hidden = YES;
        toggleButton.enabled=NO;
        [[webServices sharedServices] viewOutsidePublicAlbumsWithAlbumId:_albumID handler:^(NSData *data){
            [self connectionGotData:data];
        }errorHandler:^(NSError *error){
            [self connectionFailedWithError:error];
        }];
        [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
}
-(void)showPullToRefreshMessage:(BOOL)show{
    if (show) {
        refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Pull Down to refresh"];
    }else{
        
        refreshControl.attributedTitle = [[NSAttributedString alloc] initWithString:@"Updating albums"];
    }
    
}
- (IBAction)changeCoverOnLongTap:(UILongPressGestureRecognizer *)sender {
    
    if (_type == 1) {
        return;
    }
    
    //NSLog(@"longPresss");
   // if ( sender.state == UIGestureRecognizerStateEnded ) {
    if ( sender.state == UIGestureRecognizerStateRecognized ) {
        
        //NSLog(@"longPresss ENDED");
        PSTAlertController *gotoPageController = [PSTAlertController alertWithTitle:nil message:nil];
        [gotoPageController addCancelActionWithHandler:NULL];

        
        [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Change Cover Photo?" handler:^(PSTAlertAction *action) {
            
            PEPSInviteFriendsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"inviteFriendsScene"];
            
            vc.albumId = _albumID;
            vc.type =2;
            
            
            [self.navigationController pushViewController:vc animated:YES];
        }]];
        [gotoPageController showWithSender:nil controller:self animated:YES completion:NULL];

    }
}

- (IBAction)toggle:(id)sender {
    
    if ([isLike integerValue]==0) {


   if(toggleIsOn) {
        //NSLog(@"toggle is off now - UNLIKED ");
             // NSLog(@"myId to send: %@" ,myId);
       [[webServices sharedServices] unLikeObjectType:@"gallery" userId:myId objectId:_albumID likeId:likeId handler:^(NSData *data){
           NSLog(@"for UNLIKE ID : %@" , likeId);
           [self unlikeConnectionGotData:data];
       }errorHandler:^(NSError *error){
           [self connectionFailedWithError:error];
       }];
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    }
   else{
       //NSLog(@"toggle is on now - LIKED");
 
       [[webServices sharedServices] likeObjectType:@"gallery" userId: myId objectId:_albumID handler:^(NSData *data){
           [self likeConnectionGotData:data];
       }errorHandler:^(NSError *error){
           [self connectionFailedWithError:error];
       }];
       [MBProgressHUD showHUDAddedTo:self.view animated:YES];
       
   }
    toggleIsOn = !toggleIsOn;
    //[toggleButton setImage:[UIImage imageNamed:toggleIsOn ? @"heart-filled.png": @"Heart-WHITE_picsha.png" ] forState:UIControlStateNormal];
        
        
    }
    
    
    
    
    else if ([isLike integerValue]==1) {
        
        
        if(!toggleIsOn) {
            //NSLog(@"toggle is On now - UNLIKED ");
            //NSLog(@"myId to send: %@" ,myId);
            [[webServices sharedServices] unLikeObjectType:@"gallery" userId:myId objectId:_albumID likeId:likeId handler:^(NSData *data){
                [self unlikeConnectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
        }
        else{
            //NSLog(@"toggle is on now - LIKED");
            
            [[webServices sharedServices] likeObjectType:@"gallery" userId: myId objectId:_albumID handler:^(NSData *data){
                [self likeConnectionGotData:data];
            }errorHandler:^(NSError *error){
                [self connectionFailedWithError:error];
            }];
            [MBProgressHUD showHUDAddedTo:self.view animated:YES];
            
        }
        toggleIsOn = !toggleIsOn;
     //   [toggleButton setImage:[UIImage imageNamed:toggleIsOn ?  @"Heart-WHITE_picsha.png" : @"heart-filled.png" ] forState:UIControlStateNormal];
        
        
    }
    
    
    

}

- (void)alertView:(UIAlertView *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
    // the user clicked one of the OK/Cancel buttons
    
    switch (actionSheet.tag ) {
        case 1001:
            if (buttonIndex == 0)
            {
                //NSLog(@"NO");
                //NSLog(@"case 1001 for NOT deleting album");
                
            }
            else
            {
                //NSLog(@"YES");
                //NSLog(@"case 1001 for deleting album with album id : %@" , _albumID);
                
                [[webServices sharedServices] deleteGalleryWithAlbumId:_albumID handler:^(NSData *data){
                    [self deleteGalleryConnectionGotData:data];
                }errorHandler:^(NSError *error){
                    [self connectionFailedWithError:error];
                }];
                [MBProgressHUD showHUDAddedTo:self.view animated:YES];
                
            }
            break;
            
        case 1009:
            if (buttonIndex == 0)
            {
                //NSLog(@"NO");
                //NSLog(@"case 1001 for NOT deleting album");
                
            }
            else if (buttonIndex == 1)
            {
                //NSLog(@"YES");
                NSLog(@"case 1009 for PICKSHA");
                PEPSInviteFriendsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"inviteFriendsScene"];
                
                vc.albumId = _albumID;
                vc.type =0;
                
                
                [self.navigationController pushViewController:vc animated:YES];
                

            }
            else if (buttonIndex == 2)
            {
                //NSLog(@"YES");
                NSLog(@"case 1009 for Email");
                emailInviteViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"emailInviteScene"];
                
                vc.albumID = _albumID;
               // vc.type =1;
                
                
                [self.navigationController pushViewController:vc animated:YES];
                
            }
            else if (buttonIndex == 3)
            {
                //NSLog(@"YES");
                NSLog(@"case 1009 for FB");
                
            }
            break;
        default:
            break;
    }
}

#pragma mark - Collection View Methods


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return returnedDataArray.count;
}
- (IBAction)editGalleryOnTap:(id)sender {
    
    PSTAlertController *gotoPageController = [PSTAlertController alertWithTitle:nil message:nil];
    
    NSString *labelToShow;
    if (_type == 0) {
        labelToShow = @"Edit Album";
    }
    else{
        labelToShow = @"Add Photos";
    }
    
    
    [gotoPageController addCancelActionWithHandler:NULL];
    
    if ([canAddPhotos integerValue] )
    {
        [gotoPageController addAction:[PSTAlertAction actionWithTitle:labelToShow handler:^(PSTAlertAction *action) {
        PEPSCreateEditTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"createEditAlbumScene"];
        
        vc.type = 1;
        vc.albumIdToEdit = _albumID;
        
        
        if (_type == 0) {
            vc.is_me =0; // yup!that's me
            vc.otherUserName =_userName;
        }
        else
            vc.is_me=1; //that is someOtherUser
            vc.otherUserName =_userName;
        
        [self.navigationController pushViewController:vc animated:YES];
        
        }]];
  
    }
    
    
    if ([canInviteFriends integerValue])
    {
        [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Invite Friends" handler:^(PSTAlertAction *action) {
            
            UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Choose to invite Friends" delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"Picksha", @"Email", nil];
            deleteAlert.tag = 1009;
            [deleteAlert show];
      
    
        }]];
        
    }
    
    if (_type == 0)
    {
        [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Delete Album" handler:^(PSTAlertAction *action) {
            UIAlertView *deleteAlert = [[UIAlertView alloc] initWithTitle:@"" message:@"Are you sure you want to delete this album?" delegate:self cancelButtonTitle:@"NO" otherButtonTitles:@"YES", nil];
            deleteAlert.tag = 1001;
            [deleteAlert show];
            
        }]];
        
        }
    if([canDeletePhotos integerValue]){
        
        [gotoPageController addAction:[PSTAlertAction actionWithTitle:@"Delete Photos" handler:^(PSTAlertAction *action) {
            
            PEPSInviteFriendsViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"inviteFriendsScene"];
            
            vc.albumId = _albumID;
            vc.type =1;
            
            
            [self.navigationController pushViewController:vc animated:YES];
        }]];
        
    }

    [gotoPageController showWithSender:nil controller:self animated:YES completion:NULL];
}


-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{NSLog(@" CELL FOR ITEM AT INDEX");
    UICollectionViewCell *cell=[collectionView dequeueReusableCellWithReuseIdentifier:@"albumCell" forIndexPath:indexPath];
    
    
    NSString *imageURL = [returnedDataArray objectAtIndex:indexPath.row][@"imageUrl"];

    AsyncImageView *imageView = (AsyncImageView *)[cell viewWithTag:1];
   /* imageView.image = [UIImage imageNamed:@"placeholderPahaar"];
    imageView.showActivityIndicator = YES;
    imageView.imageURL = [NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] ;
    */
    
    NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20"]];
   [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
   
   // [imageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURL]] placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
  
    return cell;
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
    [self showPhotoBrowserFromIndex:indexPath.row];
}

-(UICollectionReusableView *)collectionView:(UICollectionView *)collectionView viewForSupplementaryElementOfKind:(NSString *)kind atIndexPath:(NSIndexPath *)indexPath{
    UICollectionReusableView *headerView = nil;
    if (kind == UICollectionElementKindSectionHeader) {
        headerView = [collectionView dequeueReusableSupplementaryViewOfKind:UICollectionElementKindSectionHeader withReuseIdentifier:@"headerView" forIndexPath:indexPath];
        UIImageView *imageView = (UIImageView *)[headerView viewWithTag:21];
        
        UILongPressGestureRecognizer *gestureRecognizer = [[UILongPressGestureRecognizer alloc]  initWithTarget:self action:@selector(changeCoverOnLongTap:)];
        gestureRecognizer.minimumPressDuration = 0.5;
        [headerView addGestureRecognizer:gestureRecognizer];
        
        NSURL *theURL = [NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",_coverPhotoURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];

        NSLog(@"setting URL COVER wity URL  : %@" , theURL);
        
        [imageView sd_setImageWithURL:theURL placeholderImage:[UIImage imageNamed:@"placeholderPahaar"]];
        
    }
    return headerView;

}
-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout referenceSizeForHeaderInSection:(NSInteger)section{
    CGSize size = CGSizeMake(self.view.frame.size.width, self.view.frame.size.height/2.2);
    return size;
}

#pragma mark - CONNECTION

-(void)getGalleryDetailswithAlbumID:(NSString *)albumID{
    
    [[webServices sharedServices]getGalleryDetailWithAlbumId:albumID handler:^(NSData *data){
        [self connectionGotData:data];
    }errorHandler:^(NSError *error){
        [self connectionFailedWithError:error];
    }];
    [MBProgressHUD showHUDAddedTo:self.view animated:YES];
    //[photosCollectionView reloadData];
    
}




-(void)getUserIdConnectionGotData:(NSData *)data{
    
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    userIdToSendToLikeOrUnlike= returnedDict[@"id"];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    
}

-(void)likeConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];

    NSLog(@"likeConnectionGotData returnedDict : %@" , returnedDict);
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    NSLog(@"setting returnedDict likeCount : %@ " , returnedDict[@"likeCount"]);
    [numberOfLikesLabel performSelectorOnMainThread:@selector(setText:) withObject:returnedDict[@"likeCount"] waitUntilDone:NO];
    likeId =returnedDict[@"likeId"];
    
    if ([returnedDict[@"code"] isEqualToString:@"200"]) {
        [toggleButton setSelected:YES];
    }

}
-(void)unlikeConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];

    NSLog(@"UNlikeConnectionGotData returnedDict : %@" , returnedDict);

    
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    
    [numberOfLikesLabel performSelectorOnMainThread:@selector(setText:) withObject:[NSString stringWithFormat:@"%@",returnedDict[@"likeCount"]] waitUntilDone:NO];

    if ([returnedDict[@"code"] isEqualToString:@"200"]) {

        [toggleButton setSelected:NO];
   
    }
    
}
-(void)deleteGalleryConnectionGotData:(NSData *)data{
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    NSDictionary *returnedDictionary = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];

    MBProgressHUD *hud = [MBProgressHUD HUDForView:self.view];
    [hud setMode:MBProgressHUDModeText];
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    [self.navigationController popToRootViewControllerAnimated:YES];
    
}
-(void)connectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    returnedDataArray = returnedDict[@"Data"];
    galleryDetailsDict = returnedDict[@"gallery_details"][0];
    
    NSLog(@"returnedDict for galleryDetails : %@" , returnedDict);

    
    likeDict = returnedDict[@"like"][0];
    likeId = likeDict[@"like_id"];
    _coverPhotoURL = galleryDetailsDict[@"coverUrl"];
    

    
    isLike = likeDict[@"like"] ;

    
    if ([isLike integerValue] ==1) {

        [toggleButton setSelected:YES];
    }
    else if([isLike integerValue] ==0){

        [toggleButton setSelected:NO];
    }
    NSLog(@"setting galleryDetailsDict imageCount   :%@" , galleryDetailsDict[@"imageCount"] );
    [numberOfPhotosLabel performSelectorOnMainThread:@selector(setText:) withObject:galleryDetailsDict[@"imageCount"] waitUntilDone:NO];
    canAddPhotos = galleryDetailsDict[@"gallery_access"];
    canDeletePhotos = galleryDetailsDict[@"deletePhotos"];
    canInviteFriends=galleryDetailsDict[@"invite_gallery"];
    
    if ((_type ==1) && ([canAddPhotos integerValue] == 0) && ([canInviteFriends integerValue]==0) && ([canDeletePhotos integerValue]==0)) {
        settingsButton.enabled = NO;
        settingsButton.hidden = YES;
        
    }
    
    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];
    
    [refreshControl endRefreshing];
    [self showPullToRefreshMessage:YES];
    [refreshControl endRefreshing];
    
    [self loadPhotos];
    [photosCollectionView reloadData];
}

-(void)getProfileInfoConnectionGotData:(NSData *)data{
    NSDictionary *returnedDict = [NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:nil];
    NSDictionary * returnedDataDictionary = returnedDict[@"Data"];

    
    NSString *imageURLForOtherUser = returnedDataDictionary[@"image"];

    [profileImageView sd_setImageWithURL:[NSURL URLWithString:[NSString stringWithFormat:@"http://%@",imageURLForOtherUser]] placeholderImage:[UIImage imageNamed:@"personPlaceholder"]];

    [self performSelector:@selector(hideAllHUDs) withObject:nil afterDelay:0.0];

}



-(void)hideAllHUDs{
    [MBProgressHUD hideAllHUDsForView:self.view animated:YES];
}
-(void)connectionFailedWithError:(NSError *)error{
    [self hideAllHUDs];
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:[[[error userInfo ] objectForKey:NSUnderlyingErrorKey] localizedDescription] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil, nil];
    [alert show];
}

#pragma mark - Photo Browser Methods
-(void)loadPhotos{
    self.photos = [NSMutableArray array];


    for (int i = 0; i<returnedDataArray.count; i++) {
        NSDictionary *theImageDict =[returnedDataArray objectAtIndex:i];
        NSString *imageURL=theImageDict[@"imageUrl"];
        
        MWPhoto *photo =[MWPhoto photoWithURL:[NSURL URLWithString:[[NSString stringWithFormat:@"http://%@",imageURL] stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]]];
        photo.imageId=[theImageDict[@"imageId"] integerValue];
        photo.isLiked =[theImageDict[@"like"] boolValue];
        photo.likeId=[theImageDict[@"like_id"] integerValue];
        [_photos addObject:photo];

    }
    

}
-(void)showPhotoBrowserFromIndex:(NSInteger )startingIndex{
    MWPhotoBrowser *browser = [[MWPhotoBrowser alloc] initWithDelegate:self];
    
    // Set options
    browser.displayActionButton = YES; // Show action button to allow sharing, copying, etc (defaults to YES)
    browser.displayNavArrows = YES; // Whether to display left and right nav arrows on toolbar (defaults to NO)
    browser.displaySelectionButtons = NO; // Whether selection buttons are shown on each image (defaults to NO)
    browser.zoomPhotosToFill = YES; // Images that almost fill the screen will be initially zoomed to fill (defaults to YES)
    browser.alwaysShowControls = NO; // Allows to control whether the bars and controls are always visible or whether they fade away to show the photo full (defaults to NO)
    browser.enableGrid = NO; // Whether to allow the viewing of all the photo thumbnails on a grid (defaults to YES)
    browser.startOnGrid = NO; // Whether to start on the grid of thumbnails instead of the first photo (defaults to NO)
    //browser.wantsFullScreenLayout = YES; // iOS 5 & 6 only: Decide if you want the photo browser full screen, i.e. whether the status bar is affected (defaults to YES)
    browser.isPublic = self.isPublic;
    // Optionally set the current visible photo before displaying
   // [browser setCurrentPhotoIndex:1];
    
    // Present
    [self.navigationController pushViewController:browser animated:YES];
    
    // Manipulate
    [browser showNextPhotoAnimated:YES];
    [browser showPreviousPhotoAnimated:YES];
    [browser setCurrentPhotoIndex:startingIndex];

}
- (NSUInteger)numberOfPhotosInPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    return self.photos.count;
}



- (id <MWPhoto>)photoBrowser:(MWPhotoBrowser *)photoBrowser photoAtIndex:(NSUInteger)index {
    
    if (index < self.photos.count){
        
        return [self.photos objectAtIndex:index];
    }
    return nil;
}

-(void)likePhotoButtonPressedForPhotoBrowser:(MWPhotoBrowser *)photoBrowser {

}
-(void)commentButtonPressedForPhotoBrowser:(MWPhotoBrowser *)photoBrowser {
    //NSLog(@"show a comment box inside the view :%@",photoBrowser);
    
    imageIdToSend =  [returnedDataArray objectAtIndex:photoBrowser.currentIndex][@"imageId"];
    
    commentBoxViewController *vc=[self.storyboard instantiateViewControllerWithIdentifier:@"commentBoxScene"];
    vc.imageId = imageIdToSend;
    vc.isOutsidePublic = self.isPublic;
  

    MZFormSheetController *formSheet = [[MZFormSheetController alloc] initWithViewController:vc];
    
    formSheet.presentedFormSheetSize = CGSizeMake(self.view.frame.size.width-30,self.view.frame.size.height-150);
    formSheet.shadowRadius = 0.0;
    formSheet.cornerRadius = 0.0;
    formSheet.shadowOpacity = 0.3;
    formSheet.shouldDismissOnBackgroundViewTap = YES;
    formSheet.shouldCenterVertically = YES;
    formSheet.movementWhenKeyboardAppears = MZFormSheetWhenKeyboardAppearsCenterVertically;
    
    [[MZFormSheetController sharedBackgroundWindow] setBackgroundBlurEffect:YES];
    [[MZFormSheetController sharedBackgroundWindow] setBlurRadius:5.0];
    [[MZFormSheetController sharedBackgroundWindow] setBackgroundColor:[UIColor clearColor]];
    [formSheet setMovementWhenKeyboardAppears:MZFormSheetWhenKeyboardAppearsMoveAboveKeyboard];
    
    
    formSheet.transitionStyle = MZFormSheetTransitionStyleSlideFromBottom;
    
    [photoBrowser mz_presentFormSheetController:formSheet animated:YES completionHandler:^(MZFormSheetController *formSheetController) {
        
    }];
}
@end
