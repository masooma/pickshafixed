//
//  ViewController.h
//  iOS7SearchBarTableViewTutorial
//
//  Created by Arthur Knopper on 24-02-14.
//  Copyright (c) 2014 Arthur Knopper. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PEPSSearchViewController : UIViewController<UISearchDisplayDelegate, UITableViewDelegate , UITableViewDataSource>

@end
