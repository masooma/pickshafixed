//
//  SOConversationViewController.h
//  snapOpen
//
//  Created by Pantera Engineering on 06/03/2015.
//  Copyright (c) 2015 Pantera Engineering. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JSQMessages.h"
//#import "user.h"

@interface SOConversationViewController : JSQMessagesViewController<UIActionSheetDelegate , UIImagePickerControllerDelegate>


@property (strong, nonatomic) JSQMessagesBubbleImage *outgoingBubbleImageData;

@property (strong, nonatomic) JSQMessagesBubbleImage *incomingBubbleImageData;
@property (nonatomic, strong) NSString *myUserId;
@property (nonatomic, strong) NSString *messageFromUserId;
@property (nonatomic, strong) NSString *messageFromUserName;
@property (nonatomic, strong) NSString *messageFromUserProfilePicture;
//@property (nonatomic, strong) user *theUser;
-(void)messageReceived:(NSDictionary *)theMessage;
-(void)closeChat;
@end
