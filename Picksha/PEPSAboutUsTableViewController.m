//
//  PEPSAboutUsTableViewController.m
//  Picksha
//
//  Created by iOS Department-Pantera on 02/06/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import "PEPSAboutUsTableViewController.h"
#import "PEPSHelpTableViewController.h"

@interface PEPSAboutUsTableViewController ()

@end

@implementation PEPSAboutUsTableViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationItem.title = @"About Us" ;
    
    // Uncomment the following line to preserve selection between presentations.
    // self.clearsSelectionOnViewWillAppear = NO;
    
    // Uncomment the following line to display an Edit button in the navigation bar for this view controller.
    // self.navigationItem.rightBarButtonItem = self.editButtonItem;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source
/*

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {

    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    return 3;
}
 */



-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    

    if (indexPath.section == 0 && indexPath.row==1) {
        PEPSHelpTableViewController *vc = [self.storyboard instantiateViewControllerWithIdentifier:@"helpScene"];
        [self.navigationController pushViewController:vc animated:YES];
    }
    
}

@end
