//
//  WebServicesViewController.m
//  bringThings
//
//  Created by iOS Department-Pantera on 25/02/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//
#import "webServices.h"
#import "connectionManager.h"


@implementation webServices

+ (webServices *)sharedServices {
    static webServices *sharedServices = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        sharedServices = [[self alloc] init];
    });
    return sharedServices;
}
-(void)updateMyUser{
    [self getProfileInfoWithUserId:_myUser[@"id"] handler:^(NSData *data){
        NSDictionary *response = [NSJSONSerialization JSONObjectWithData:data options:NSUTF8StringEncoding error:nil];
        if (response[@"Data"]!=nil) {
            _myUser = response[@"Data"];
            NSLog(@"UPDATED MU USER : %@" , _myUser);
        }
    }errorHandler:^(NSError *error){}];
}
NSString * const baseURL = @"http://www.panteraenergy.pk/picksha";

#pragma mark - Helper Methods

-(NSURL *)urlWithString:(NSString *)urlString{
    return   [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,urlString]];
}

-(void)startConnectionWithUrlString:(NSString *)urlString handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSURL *theURL = [self urlWithString:[urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]];
    NSURLRequest *request = [NSURLRequest requestWithURL:theURL];
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:request handler:handler errorHandler:errorHandler];
}
-(void)startPostConnectionWithUrlString:(NSString *)urlString data:(NSData *) data handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,[urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]]];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[data length]];
    NSMutableURLRequest *theRequest = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
    // NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [theRequest setHTTPMethod:@"POST"];
   [theRequest setValue:msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //[theRequest setValue:@"application/form-data" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPBody:data];
    theRequest.HTTPShouldHandleCookies = YES;
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:theRequest handler:handler errorHandler:errorHandler];
}
/* WHEN JSON WAS SENT!
-(void)startPostConnectionWithUrlString:(NSString *)urlString jsonMessage:(NSString *) jsonMessage handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,urlString]];
    NSString *msgLength = [NSString stringWithFormat:@"%lu", (unsigned long)[jsonMessage length]];
    NSMutableURLRequest *theRequest = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
       // NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [theRequest setHTTPMethod:@"POST"];
    [theRequest setValue:msgLength forHTTPHeaderField:@"Content-Length"];
    [theRequest setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    //[theRequest setValue:@"application/form-data" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPBody:[jsonMessage dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES]];
    theRequest.HTTPShouldHandleCookies = YES;
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:theRequest handler:handler errorHandler:errorHandler];
}
*/
-(void)startDeleteConnectionWithUrlString:(NSString *)urlString handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
   // NSURL *theURL = [self urlWithString:urlString];
     NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,[urlString stringByReplacingOccurrencesOfString:@" " withString:@"%20" ]]];
    NSMutableURLRequest *theRequest = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
    NSString *post = @"";
    NSData *postData = [post dataUsingEncoding:NSUTF8StringEncoding allowLossyConversion:YES];
    [theRequest setHTTPMethod:@"DELETE"];
    [theRequest setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
    [theRequest setHTTPBody:postData];
    theRequest.HTTPShouldHandleCookies = YES;
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:theRequest handler:handler errorHandler:errorHandler];
}

#pragma mark - Login / Logout / Register Methods

-(void)attemptSignUpUserWithEmail:(NSString *)email password:(NSString *)password username:(NSString *)username handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/registration-with-email.php";
    NSString *rawStr = [NSString stringWithFormat:@"username=%@&email=%@&password=%@",username ,email, password];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr being sent for SIGNUP : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}

-(void)attemptLoginWithUsername:(NSString *)username password:(NSString *)password handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/sign.php";
   // NSString *JsonMsg = [NSString stringWithFormat:@"json={\"email\" : \"%@\",\"password\": \"%@\"}", username , password];
    NSString *rawStr = [NSString stringWithFormat:@"email=%@&password=%@",username , password];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr being sent for LOGIN : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}
-(void)attemptFacebookLoginWithfacebookId:(NSString *)facebookId email:(NSString *)email handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/fb.php";
    NSString *rawStr = [NSString stringWithFormat:@"email=%@&facebook_id=%@",email , facebookId];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr being sent for FB LOGIN : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}
-(void)logoutUserWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/logout.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)checkIfUserLoggedInWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/check-login.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}

#pragma - mark  Gallery Methods

-(void)uploadImagesTemporarilywithImage:(UIImage *)image withImageName:(NSString *)imageName aboutPhoto:(NSString *)aboutPhoto cover:(NSString *)cover handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/temp-album.php";
    

    
    
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,urlString]];
    NSLog(@" uploadImagesTemporarilywithImage !!!!!! URL : %@" , theURL);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"YOUR_BOUNDARY_STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
     //Sending Image :
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n", imageName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];

     //Sending String Message :
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"about_photo\"\r\n\r\n%@", aboutPhoto] dataUsingEncoding:NSUTF8StringEncoding]];
    

    //Sending String Message :
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cover\"\r\n\r\n%@", aboutPhoto] dataUsingEncoding:NSUTF8StringEncoding]];

    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:request handler:handler errorHandler:errorHandler];
}



-(void)createGalleryWithAlbumname:(NSString *)albumName description:(NSString *)description seeAlbum:(NSString *)seeAlbum uploadPhotos:(NSString *)uploadPhotos inviteOthers:(NSString *)inviteOthers handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/create_gallery.php";
    NSLog(@"%@, %@ , %@ , %@ , %@" , albumName , description , seeAlbum , uploadPhotos , inviteOthers);
    
  /*
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?name=%@&description=%@&see_gallery=%@&gallery_access=%@&invite_gallery=%@",urlString,albumName ,  description , seeAlbum , uploadPhotos , inviteOthers];
    NSLog(@" URL being sent for createGalleryWithAlbumname:: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
   */
   
   
    NSString *rawStr = [NSString stringWithFormat:@"name=%@&description=%@&see_gallery=%@&gallery_access=%@&invite_gallery=%@",albumName ,  description , seeAlbum , uploadPhotos , inviteOthers];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for createGalleryWithAlbumname : %@" , rawStr );
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
    
}
-(void)uploadGalleryWithAlbumname:(NSString *)albumName description:(NSString *)description seeAlbum:(NSString *)seeAlbum uploadPhotos:(NSString *)uploadPhotos inviteOthers:(NSString *)inviteOthers handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/upload-album.php";
    NSString *rawStr = [NSString stringWithFormat:@"name=%@&description=%@&see_gallery=%@&gallery_access=%@&invite_gallery=%@",albumName ,  description , seeAlbum , uploadPhotos , inviteOthers];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"JSON msg being sent for uploadGalleryWithAlbumname : %@" , data);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}

-(void)deleteGalleryWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/delete-album.php"];
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , albumId];
    
    /*
     NSString *rawStr = [NSString stringWithFormat:@"id=%@",albumId];
     NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
     */
    NSLog(@" URL being sent for gallery DEtails : %@" , urlStringtoSend);
    
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)updateGalleryWithAlbumId:(NSString *)albumId name:(NSString *)albumName description:(NSString *)description seeAlbum:(NSString *)seeAlbum uploadPhotos:(NSString *)uploadPhotos inviteOthers:(NSString *)inviteOthers handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/update-album.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , albumId];
    NSString *rawStr = [NSString stringWithFormat:@"name=%@&description=%@&see_gallery=%@&gallery_access=%@&invite_gallery=%@", albumName ,  description , seeAlbum , uploadPhotos , inviteOthers];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"JSON msg being sent for updateGalleryWithAlbumId : %@" , data);
    [self startPostConnectionWithUrlString:urlStringtoSend data:data handler:handler errorHandler:errorHandler];
}

-(void)viewPublicAlbumsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/index.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)viewOutsidePublicAlbumsWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/public-albums.php"];
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , albumId];

    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)getAlbumSettingsWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/album-settings.php";
    
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?albumId=%@",urlString , albumId];

    NSLog(@" URL being sent for getGallerySETTINGSWithAlbumId: : %@" , urlStringtoSend);
    
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)getGalleryDetailWithAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/gallery-detail.php";

    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , albumId];
 
   /*
    NSString *rawStr = [NSString stringWithFormat:@"id=%@",albumId];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    */
    NSLog(@" URL being sent for gallery DEtails : %@" , urlStringtoSend);
    
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)viewFriendsAlbumsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/friends-gallerys.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)viewMyAlbumsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/my-gallery.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)getAlbumsForUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/user-albums.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?user_id=%@",urlString , userId];
    NSLog(@" URL being sent for getAlbumsForUserId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)inviteFriendToAlbumId:(NSString *)albumId senderId:(NSString *)senderId receiverId:(NSString *)receiverId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{

    NSString *urlString = @"/api/invite-friend-to-gallery.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?gallery_id=%@&sender_id=%@&receiver_id=%@",urlString , albumId, senderId , receiverId];
    NSLog(@" URL being sent for getAlbumsForUserId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)inviteFriendWithEmail:(NSString *)email galleryId:(NSString *)galleryId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/invite-friends-by-email.php";
   
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?emails=%@&gallery_id=%@",urlString , email , galleryId];
    NSLog(@" URL being sent for inviteFriendWithEmail : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}


-(void)getInvitedFriendsForAlbumId:(NSString *)albumId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/invited-friends.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?gallery_id=%@",urlString , albumId];
    NSLog(@" URL being sent for getInvitedFriendsForAlbumId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)likeObjectType:(NSString *)objectType userId:(NSString *)userId objectId:(NSString *)objectId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/like.php";
    
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?object_type=%@&user_id=%@&object_id=%@",urlString ,  objectType , userId , objectId];
    NSLog(@" URL being sent for likeObjectType : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];

}
-(void)unLikeObjectType:(NSString *)objectType userId:(NSString *)userId objectId:(NSString *)objectId likeId:(NSString *)likeId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/unlike.php";

    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?object_type=%@&user_id=%@&object_id=%@&like=%@",urlString , objectType , userId , objectId , likeId];
    NSLog(@" URL being sent for likeObjectType : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)editGallerywithImage:(UIImage *)image userUploadingUsername:(NSString *)username withGalleryId:(NSString *)gallery_id cover:(NSString *)cover handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/edit-album.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?username=%@&gallery_id=%@&cover=%@",urlString , username , gallery_id , cover];

    NSLog(@"urlStringtoSend being sent for editGallerywithImage : %@" , urlStringtoSend);
    
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,urlStringtoSend]];
    
    NSLog(@"theURL for : editGallerywithImage  :%@ " , theURL);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"YOUR_BOUNDARY_STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    //Sending Image :
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n", @""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    NSLog(@"image: %@",image);
   
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:request handler:handler errorHandler:errorHandler];
}

-(void)deleteTemporaryPicturesUploadedByUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/delete-temp-data.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , userId];
    NSLog(@" URL being sent for deleteTemporaryPicturesUploadedByUs : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)getImageDetailWithUserId:(NSString *)userId objectId:(NSString *)objectId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/image-detail.php";
    
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?user_id=%@&object_id=%@",urlString ,userId,objectId];
    NSLog(@" URL being sent for getImageDetailWithUserId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
   /*
    NSString *rawStr = [NSString stringWithFormat:@"user_id=%@&object_id=%@",userId,objectId];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr being sent for getImageDetail : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
    */
}

-(void)saveCommentForObjectId:(NSString *)objectId text:(NSString *)text commentNumber:(NSString *)commentNumber handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/save-comment.php";
    NSString *rawStr = [NSString stringWithFormat:@"object_id=%@&comment_text=%@&comment_no=%@",objectId , text , commentNumber];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr being sent for saveCommentForObjectId : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}
-(void)deletePhotoByUsername:(NSString *)username galleryId:(NSString *)galleryId objectId:(NSString *)objectId deleteAll:(NSString *)deleteAll handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/delete-photo.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@&gallery_id=%@&username=%@&delete_all=%@",urlString ,objectId , galleryId , username , deleteAll];
    NSLog(@" URL being sent for deleteTemporaryPicturesUploadedByUs : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)makeCoverPhotoWithImageId:(NSString *)imageId galleryId:(NSString *)galleryId  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/select-cover.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?imageId=%@&galleryId=%@", urlString,imageId ,galleryId];
    NSLog(@" URL being sent for makeCoverPhotoWithImageId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}




#pragma - mark  ProfileInfo
-(void)getAdminProfileImageAndUsernameWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/profileimage.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)getProfileInfoWithUsername:(NSString *)username handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/profilepage.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?username=%@",urlString , username];
    NSLog(@" URL being sent for getProfileInfoWithUsername : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)getProfileInfoWithUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/profilepagewithid.php";

    
    NSString *rawStr = [NSString stringWithFormat:@"id=%@",userId];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for getProfileInfoWithUserId : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}


#pragma - mark  Notifications and Messages

-(void)getNotificationsFriendRequestsAndMessagesCountWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/header.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}

-(void)getNotificationsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/notifications.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)getMessagesListWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/message-list.php";
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)sendMessageTo:(NSString *)to from:(NSString *)from textMessage:(NSString *)textMessage handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/send-message.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?to=%@&from=%@",urlString , to , from];
    NSLog(@"sendMessageTo urlStringtoSend  :%@", urlStringtoSend);

    NSString *rawStr = [NSString stringWithFormat:@"message_text=%@",textMessage];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for sendMessageTo : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlStringtoSend data:data handler:handler errorHandler:errorHandler];
}
-(void)getMessagesForSpecificUserWithId:(NSString *)specificUserId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/specific-message.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?user_from=%@",urlString , specificUserId];
    NSLog(@" URL being sent for getMessagesForSpecificUserWithId: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)deleteConversationWithUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/delete-conversation.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?user_id=%@",urlString , userId];
    NSLog(@" URL being sent for deleteConversation: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)sendMessagewithImage:(UIImage *)image toUserId:(NSString *)to  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/submitNewMsg.php";
    
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@?to=%@",baseURL,urlString , to]];
    NSLog(@"endMessagewithImage withImage !!!!!! URL : %@" , theURL);
    
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"YOUR_BOUNDARY_STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    //Sending Image :
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"msg_file\"; filename=\"%@.jpg\"\r\n", @""] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    
    //Sending String Message (writeMessageText):
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"write_msg\"\r\n\r\n%@", @"image"] dataUsingEncoding:NSUTF8StringEncoding]];
    
    /*
     //Sending String Message :
     [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
     [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"cover\"\r\n\r\n%@", aboutPhoto] dataUsingEncoding:NSUTF8StringEncoding]];
     */
    
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:request handler:handler errorHandler:errorHandler];
}


#pragma - mark  Friends

-(void)getFriendRequestsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/friends-request.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)getFriendListWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/friends.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)acceptFriendRequestWithId:(NSString *)requestId from:(NSString *)from  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/accept-request.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?request_id=%@&user_from=%@",urlString , requestId ,from ];
    NSLog(@"URL being sent for acceptFriendRequestWithId: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}

-(void)cancelFriendRequestWithId:(NSString *)requestId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/cancel-request.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , requestId];
    NSLog(@" URL being sent for cancelFriendRequestWithId: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)sendFriendRequestTo:(NSString *)to from:(NSString *)from handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/send_request.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?to=%@&from=%@",urlString , to , from];
    NSLog(@" URL being sent for cancelFriendRequestWithId: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)isFriendWithUserWithId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/is-friend.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?to=%@",urlString , userId];
    NSLog(@" URL being sent for isFriendWithUserWithId: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)deleteFriendWithUserId:(NSString *)userId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/delete-friend.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?del_id=%@",urlString , userId];
    NSLog(@" URL being sent for deleteFriendWithUserId: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
#pragma - mark  SEARCH

-(void)getSearchWithString:(NSString *)string handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/picksha-search.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?search_txt=%@",urlString , string];
    NSLog(@" URL being sent for getSearchWithString: %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}



#pragma - mark  Settings

-(void)uploadProfilePictureithImage:(UIImage *)image withImageName:(NSString *)imageName  handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/upload-profilepic.php";
    // NSString *JsonMsg = [NSString stringWithFormat:@"json={\"email\" : \"%@\",\"password\": \"%@\"}", username , password];

    NSString *rawStr = [NSString stringWithFormat:@"image=%@",image ];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"JSON msg being sent for LOGIN : %@" , data);
    
    
    
    NSURL *theURL = [NSURL URLWithString:[NSString stringWithFormat:@"%@%@",baseURL,urlString]];
    NSMutableURLRequest *request = [[NSMutableURLRequest alloc] initWithURL:theURL cachePolicy:NSURLRequestUseProtocolCachePolicy timeoutInterval:200.0];
    [request setHTTPMethod:@"POST"];
    
    NSString *boundary = @"YOUR_BOUNDARY_STRING";
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", boundary];
    [request addValue:contentType forHTTPHeaderField:@"Content-Type"];
    
    NSMutableData *body = [NSMutableData data];
    NSData *imageData = UIImageJPEGRepresentation(image, 1.0);
    
    //Sending Image :
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"image\"; filename=\"%@.jpg\"\r\n", imageName] dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[@"Content-Type: application/octet-stream\r\n\r\n" dataUsingEncoding:NSUTF8StringEncoding]];
    [body appendData:[NSData dataWithData:imageData]];
    [body appendData:[[NSString stringWithFormat:@"\r\n--%@\r\n", boundary] dataUsingEncoding:NSUTF8StringEncoding]];
    
    [request setHTTPBody:body];
    
    connectionManager *connMang = [[connectionManager alloc] init];
    [connMang startConnectionWithRequest:request handler:handler errorHandler:errorHandler];
}

-(void)updateUsername:(NSString *)username realName:(NSString *)realName  email:(NSString *)email handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/account_update.php";
    NSString *rawStr = [NSString stringWithFormat:@"username=%@&email=%@&realname=%@", username, email , realName];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for forgotPasswordEnterEmail : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}

-(void)forgotPasswordEnterEmail:(NSString *)email handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/forgot-password.php";
    
    NSString *rawStr = [NSString stringWithFormat:@"email=%@",email];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for forgotPasswordEnterEmail : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}

-(void)changePasswordWithNewPassword:(NSString *)newPassword oldPassword:(NSString *)oldPassword handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/password.php";
    
    NSString *rawStr = [NSString stringWithFormat:@"password=%@&oldpassword=%@",newPassword , oldPassword];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for changePasswordWithNewPassword: %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}

-(void)deactivateAccountWithPassword:(NSString *)password handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/deactivate_account.php";

    NSString *rawStr = [NSString stringWithFormat:@"oldpassword=%@",password];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for deactivateAccountWithPassword : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}
-(void)deleteAccountWithPassword:(NSString *)password handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/delete_account.php";
    
    NSString *rawStr = [NSString stringWithFormat:@"oldpassword=%@",password];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for deleteAccountWithPassword : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}
-(void)getAlertsSettingsWithHandler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = [NSString stringWithFormat:@"/api/email_alerts_get.php"];
    [self startConnectionWithUrlString:urlString handler:handler errorHandler:errorHandler];
}
-(void)postAlertsSettingsIfGetNewFriendRequest:(NSString *)friendRequest confirmRequest:(NSString *)confirmRequest confirmInvitation:(NSString *)confirmInvitation getInvitation:(NSString *)getInvitation commentInMyGallery:(NSString *)commentInMyGallery commentWhereICommented:(NSString *)commentWhereICommented gotMessage:(NSString *)gotMessage handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/email_alerts_post.php";
    
    NSString *rawStr = [NSString stringWithFormat:@"i_got_a_new_friend_request=%@&a_friend_confirms_my_friend_request=%@&a_friends_confirms_my_invition_to_a_gallery=%@&i_get_a_invition_to_a_gallery=%@&about_new_comments_in_my_gallerys=%@&about_new_comments_where_i_commented=%@&i_got_a_new_personal_message=%@",friendRequest, confirmRequest , confirmInvitation , getInvitation , commentInMyGallery , commentWhereICommented , gotMessage];
    NSData *data = [rawStr dataUsingEncoding:NSUTF8StringEncoding];
    NSLog(@"rawStr msg being sent for postAlertsSettingsIfGetNewFriendRequest : %@" , rawStr);
    
    [self startPostConnectionWithUrlString:urlString data:data handler:handler errorHandler:errorHandler];
}

-(void)commentOnCommentedAlertWithImageId:(NSString *)imageId   handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/cmntOnCmnted-Alert.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?object_id=%@", urlString,imageId ];
    NSLog(@" URL being sent for commentOnCommentedWithImageId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)commentAlertWithImageId:(NSString *)imageId handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/sendComment-Alert.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?object_id=%@", urlString,imageId ];
    NSLog(@" URL being sent for commentAlertWithImageId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
-(void)messageAlertWithToUserId:(NSString *)userId messageText:(NSString *)messageText handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    
    NSString *urlString = @"/api/sendPersonel-msgAlert.php";
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?to=%@&message_text=%@", urlString,userId ,messageText];
    NSLog(@" URL being sent for commentOnCommentedWithImageId : %@" , urlStringtoSend);
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}





/*
-(void)getImageOfAlbumwithImageName:(NSString *)imageName userName:(NSString *)imageName handler:(void (^)(NSData*))handler errorHandler:(void (^)(NSError*))errorHandler{
    NSString *urlString = @"/api/uploads";
    
    NSString *urlStringtoSend = [NSString stringWithFormat:@"%@?id=%@",urlString , albumId];
 
    NSLog(@" URL being sent for gallery DEtails : %@" , urlStringtoSend);
    
    [self startConnectionWithUrlString:urlStringtoSend handler:handler errorHandler:errorHandler];
}
*/
@end
