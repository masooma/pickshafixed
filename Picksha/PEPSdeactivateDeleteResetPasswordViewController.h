//
//  DeactivateDeleteResetPasswordViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 20/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.

#import <UIKit/UIKit.h>

@interface PEPSdeactivateDeleteResetPasswordViewController : UIViewController <UITableViewDelegate , UITableViewDataSource>
@property (nonatomic ,assign) NSInteger type;// type 1: Deactivate 2: Delete 3: Change Password
@end