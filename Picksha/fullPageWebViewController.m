//
//  fullPageWebViewController.m
//  SignalBox
//
//  Created by iOS Department-Pantera on 29/12/14.
//  Copyright (c) 2014 Pantera Private Limited. All rights reserved.
//

#import "fullPageWebViewController.h"

@interface fullPageWebViewController ()

@end

@implementation fullPageWebViewController
{

    IBOutlet UIWebView *theWebView;
}
- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad {
    
    [super viewDidLoad];
    theWebView.delegate = self;
    if (!self.urlString) {
        
        self.urlString = @"http://www.google.com";
    }
}


-(void)viewDidAppear:(BOOL)animated{
    [super viewDidAppear:animated];
    [self loadWebPageWithUrlString:_urlString];
    [self.navigationItem setTitle:_titleString];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)viewWillDisappear:(BOOL)animated
{
    if ( [theWebView isLoading] ) {
        [theWebView stopLoading];
    }
    theWebView.delegate = nil;    // disconnect the delegate as the webview is hidden
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}
-(void)loadWebPageWithUrlString:(NSString *)urlString{
    [theWebView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:urlString]]];
}
#pragma mark - Web View Delegate
- (void)webViewDidStartLoad:(UIWebView *)webView
{
    // starting the load, show the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = YES;
}

- (void)webViewDidFinishLoad:(UIWebView *)webView
{
    // finished loading, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error
{
    // load error, hide the activity indicator in the status bar
    [UIApplication sharedApplication].networkActivityIndicatorVisible = NO;
    
    // report the error inside the webview
    NSString* errorString = [NSString stringWithFormat:
                             @"<html><center><font size=+5 color='red'>An error occurred:<br>%@</font></center></html>",
                             error.localizedDescription];
    [webView loadHTMLString:errorString baseURL:nil];
}


@end
