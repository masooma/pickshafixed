//
//  fullPageWebViewController.h
//  SignalBox
//
//  Created by iOS Department-Pantera on 29/12/14.
//  Copyright (c) 2014 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface fullPageWebViewController : UIViewController <UIWebViewDelegate>

@property (nonatomic,strong) NSString *urlString;
@property (nonatomic, strong) NSString *titleString;
@end
