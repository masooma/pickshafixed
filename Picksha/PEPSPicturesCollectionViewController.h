//
//  PEPSPicturesCollectionViewController.h
//  Picksha
//
//  Created by iOS Department-Pantera on 15/04/15.
//  Copyright (c) 2015 Pantera Private Limited. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "URBMediaFocusViewController.h"

@interface PEPSPicturesCollectionViewController : UICollectionViewController<URBMediaFocusViewControllerDelegate>

@end
